@extends('user.misc.front')

@section('content')
<!--Contact-us-->
<section class="contact_us section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Get in touch using the form below</h3>
                <div class="contact_form gray-bg">
                    <form action="#" method="get">
                        {{ Form::open(['route' => 'contact-us']) }}
                        <div class="form-group">
                            <label class="control-label">Name <span>*</span></label>
                            {{ Form::text('nama', null, ['class' => 'form-control white_bg']) }}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email<span>*</span></label>
                            {{ Form::email('email', null, ['class' => 'form-control white_bg']) }}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Telepon/Hp</label>
                            {{ Form::number('no_telp', null, ['class' => 'form-control white_bg']) }}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Pesan <span>*</span></label>
                            {{ Form::textarea('pesan', null, ['class' => 'form-control white_bg', 'required']) }}
                        </div>
                        <div class="form-group">
                            <button class="btn" type="submit">Send Message <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <h3>Contact Info</h3>
                <div class="contact_detail">
                    <ul>
                        @foreach($data as $val)
                            @if(array_key_exists($val->type, $icon))
                            <li>
                                <div class="icon_wrap"><i class="{{ $icon[$val->type]['icon'] }}" aria-hidden="true"></i></div>
                                <div class="contact_info_m">{{ $val->link }}</div>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="map_wrap">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3316.9793539212224!2d115.20849544626792!3d-8.636691436139087!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6f96db3e30ec6ffd!2sSekolah+Menengah+Kejuruan+Negeri+1+Denpasar!5e0!3m2!1sen!2sid!4v1547171886748" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Contact-us-->
@endsection