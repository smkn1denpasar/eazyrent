<div class="col-md-3 col-sm-3">
    <div class="profile_nav">
        <ul>
            <li @if(Request::segment(1) == 'my-rental' && Request::segment(2) == 'profile') class="active"@endif>
                <a href="{{ route('rental.profile') }}">Rental Settings</a>
            </li>
            <li @if(Request::segment(1) == 'my-rental' && Request::segment(2) == 'kendaraan') class="active"@endif>
                <a href="{{ route('rental.car') }}">Manage Kendaraan</a>
            </li>
            <li @if(Request::segment(1) == 'my-rental' && Request::segment(2) == 'status-penyewaan') class="active"@endif>
                <a href="{{ route('rental.status-penyewaan') }}">Manage Status Penyewaan</a>
            </li>
            <li @if(Request::segment(1) == 'my-rental' && Request::segment(2) == 'history-penyewaan') class="active"@endif>
                <a href="{{ route('rental.history') }}">History Penyewaan</a>
            </li>
        </ul>
    </div>
</div>