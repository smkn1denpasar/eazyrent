<div class="col-md-3 col-sm-3">
    <div class="profile_nav">
        <ul>
            <li @if(Request::segment(1) == "profile") class="active"@endif>
                <a href="{{ route('profile') }}">Pengaturan Profile</a>
            </li>                        
            <li @if(Request::segment(1) == "riwayat-transaksi") class="active"@endif>
                <a href="{{ route('riwayat-transaksi') }}">Riwayat Transaksi</a>
            </li>
            <li>
                <a href="{{ route('logout') }}">Log Out</a>
            </li>
        </ul>
    </div>
</div>