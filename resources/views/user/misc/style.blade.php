<!--Bootstrap -->
<link rel="stylesheet" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
<!--Custome Style -->
<link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">
<!--OWL Carousel slider-->
<link rel="stylesheet" href="{{ asset('assets/front/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/front/css/owl.transitions.css') }}">
<!--slick-slider -->
<link href="{{ asset('assets/front/css/slick.css') }}" rel="stylesheet">
<!--bootstrap-slider -->
<link href="{{ asset('assets/front/css/bootstrap-slider.min.css') }}" rel="stylesheet">
<!--FontAwesome Font Style -->
<link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/front/fontawesome/css/all.css') }}">
<!--  Select2 CDN -->
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/select2/dist/css/select2.min.css') }}">


<link rel="stylesheet" href="{{ asset('assets/front/css/custom.css') }}">