<!--Header-->
<header>
    <div class="default-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="logo"> <a href="{{ route('index') }}"><img src="{{ asset('assets/front/images/logo/logo1.png') }}" alt="image" class="logo-image" /></a> </div>
                </div>
                <div class="col-sm-9 col-md-10 mt-30">
                    <div class="header_info">
                        <div class="header_widgets">
                            <div class="circle_icon"> <i class="fa fa-envelope" aria-hidden="true"></i> </div>
                            <p class="uppercase_text">EMAIL KAMI : </p>
                            <a href="mailto:info@example.com">info@eazyrent.com</a>
                        </div>
                        <div class="header_widgets">
                            <div class="circle_icon"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
                            <p class="uppercase_text">HUBUNGI KAMI: </p>
                            <a href="tel:61-1234-5678-09">+61-1234-5678-9</a>
                        </div>
                        @if(Auth::guest())
                        <div class="login_btn custom"> <a href="#loginform" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">Masuk / Daftar</a> </div>
                        @else
                        <div class="login_btn custom">
                            <a href="{{ route('logout') }}" class="btn btn-xs uppercase">Logout</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Navigation -->
    <nav id="navigation_bar" class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed"
                    type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="header_wrap">
                <div class="user_login">
                    <ul>
                        <li class="dropdown"> <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                    class="fa fa-user-circle" aria-hidden="true"></i> Pramayasa <i class="fa fa-angle-down"
                                    aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="profile-settings.html">Profil Saya</a></li>
                                <!-- <li><a href="my-vehicles.html">My Vehicles</a></li>
                  <li><a href="post-vehicle.html">Post a Vehicle</a></li> -->
                                <li><a href="#">Sign Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                {{-- <div class="header_search">
                    <div id="search_toggle"><i class="fa fa-search" aria-hidden="true"></i></div>
                    <form action="#" method="get" id="header-search-form">
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div> --}}
            </div>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav">
                    <li class=""><a href="{{ route('index') }}">BERANDA</a>
                    </li>
                    <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inventory</a>
              <ul class="dropdown-menu">
                <li><a href="listing-grid.html">Grid Style</a></li>
                <li><a href="listing-classic.html">Classic Style</a></li>
                <li><a href="listing-detail.html">Detail Page Style 1</a></li>
                <li><a href="listing-detail-2.html">Detail Page Style 2</a></li>
              </ul>
            </li> -->
                    <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dealers</a>
              <ul class="dropdown-menu">
                <li><a href="dealers-list.html">List View</a></li>
                <li><a href="dealers-profile.html">Detail Page</a></li>
              </ul>
            </li> -->
                    <li class="dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bantuan</a>
                        <ul class="dropdown-menu">
                            <li><a href="services.html">Servis Kami</a></li>
                            <li><a href="contact-us.html">Kontak Kami</a></li>
                            <li><a href="faq.html">FAQ</a></li>
                            <!-- <li><a href="404.html">404 Error</a></li>
                <li><a href="coming-soon.html">Coming Soon</a></li> -->
                        </ul>
                    </li>
                    <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">News</a>
              <ul class="dropdown-menu">
                <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                <li><a href="blog-detail.html">Blog Detail</a></li>
              </ul>
            </li> -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navigation end -->

</header>
<!-- /Header -->
