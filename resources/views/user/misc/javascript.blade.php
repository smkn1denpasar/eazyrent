<!-- Scripts --> 
<script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('assets/front/js/interface.js') }}"></script> 
<!--Switcher-->
<script src="{{ asset('assets/front/switcher/js/switcher.js') }}"></script>
<!--bootstrap-slider-JS--> 
<script src="{{ asset('assets/front/js/bootstrap-slider.min.js') }}"></script> 
<!--Slider-JS--> 
<script src="{{ asset('assets/front/js/slick.min.js') }}"></script> 
<script src="{{ asset('assets/front/js/owl.carousel.min.js') }}"></script>
<!-- Select2 CDN -->
<script src="{{ asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- Custom -->
<script src="{{ asset('assets/front/js/custom.js') }}"></script>

@yield('custom-js')