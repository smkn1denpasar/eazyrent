<!--Brands-->
<section class="brand-section gray-bg">
    <div class="container">
        <div class="brand-hadding">
            <h5>Popular Brands</h5>
        </div>
        <div class="brand-logo-list">
            <div id="popular_brands">
                <div><a href="#"><img src="{{ asset('assets/front/images/ford.png') }}" class="img-responsive custom"
                            alt="image"></a></div>
                <div><a href="#"><img src="{{ asset('assets/front/images/toyota.png') }}" class="img-responsive custom"
                            alt="image"></a></div>
                <div><a href="#"><img src="{{ asset('assets/front/images/audi.png') }}" class="img-responsive custom"
                            alt="image"></a></div>
                <div><a href="#"><img src="{{ asset('assets/front/images/porsche.png') }}" class="img-responsive custom"
                            alt="image"></a></div>
                <div><a href="#"><img src="{{ asset('assets/front/images/honda.png') }}" class="img-responsive custom"
                            alt="image"></a></div>
            </div>
        </div>
    </div>
</section>
<!-- /Brands-->

<!--Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h6>Top Categores</h6>
                    <ul>
                        <li><a href="#">Crossovers</a></li>
                        <li><a href="#">Hybrids</a></li>
                        <li><a href="#">Hybrid Cars</a></li>
                        <li><a href="#">Hybrid SUVs</a></li>
                        <li><a href="#">Concept Vehicles</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h6>About Us</h6>
                    <ul>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Hybrid Cars</a></li>
                        <li><a href="#">Cookies</a></li>
                        <li><a href="#">Trademarks</a></li>
                        <li><a href="#">Terms of use</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h6>Useful Links</h6>
                    <ul>
                        <li><a href="#">Our Partners</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Sitemap</a></li>
                        <li><a href="#">Investors</a></li>
                        <li><a href="#">Request a Quote</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h6>Subscribe Newsletter</h6>
                    <div class="newsletter-form">
                        <form action="#">
                            <div class="form-group">
                                <input type="email" class="form-control newsletter-input" required placeholder="Enter Email Address" />
                            </div>
                            <button type="submit" class="btn btn-block">Subscribe <span class="angle_arrow"><i class="fa fa-angle-right"
                                        aria-hidden="true"></i></span></button>
                        </form>
                        <p class="subscribed-text">*We send great deals and latest auto news to our subscribed users
                            very week.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6 text-right">
                    <div class="footer_widget">
                        <p>Sosial Media Akun:</p>
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <p class="copy-right">Copyright &copy; 2018 EazyRent. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Footer-->
