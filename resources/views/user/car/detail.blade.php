@extends('user.misc.front')

@section('custom-style')
    <style> 
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }

    .grid_listing .product-listing-img img {
        height: 185px;
    }
    </style>
@endsection

@section('content')
<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="listing_images">
                    <div class="listing_images_slider">
                        @foreach($data->Foto as $foto)
                        <div><img src="{{ asset('assets/front/images/kp/' . $foto->nama) }}" alt="image"></div>
                        @endforeach
                    </div>
                    <div class="listing_images_slider_nav">
                        @foreach($data->Foto as $foto)
                        <div><img src="{{ asset('assets/front/images/kp/' . $foto->nama) }}" alt="image"></div>
                        @endforeach
                    </div>
                </div>
                <div class="listing_more_info" style="padding-top:0;">
                    <div class="listing_detail_wrap">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs gray-bg" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#vehicle-overview " aria-controls="vehicle-overview" role="tab" data-toggle="tab">Tinjauan Kendaraan </a>
                            </li>
                            <li role="presentation">
                                <a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">Spesifikasi Teknis</a>
                            </li>
                            <li role="presentation">
                                <a href="#accessories" aria-controls="accessories" role="tab" data-toggle="tab">Aksesoris</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- vehicle-overview -->
                            <div role="tabpanel" class="tab-pane active" id="vehicle-overview">
                                <p>{!! nl2br($data->deskripsi) !!}</p>
                            </div>

                            <!-- Technical-Specification -->
                            <div role="tabpanel" class="tab-pane" id="specification">
                                <div class="table-responsive">
                                    <!--Basic-Info-Table-->
                                    <table>
                                        <thead>
                                            <tr>
                                                <th colspan="2">BASIC INFO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tahun Model</td>
                                                <td>{{ $data->tahun }}</td>
                                            </tr>
                                            <tr>
                                                <td>Total KM</td>
                                                <td>{{ $data->realTotalKM }}</td>
                                            </tr>
                                            <tr>
                                                <td>Bahan Bakar</td>
                                                <td>{{ $data->realBahanBakar }}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <!--Technical-Specification-Table-->
                                    <table>
                                        <thead>
                                            <tr>
                                                <th colspan="2">Technical Specification</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Kapasitas Tangki Bahan Bakar</td>
                                                <td>{{ number_format($data->max_fuel, 0, ",", ".") }} liter</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Tempat Duduk</td>
                                                <td>{{ $data->seat }} Kursi</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Transmisi</td>
                                                <td>{{ $data->humanTransmisi }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Accessories -->
                            <div role="tabpanel" class="tab-pane" id="accessories">
                                <!--Accessories-->
                                <table>
                                    <thead>
                                        <tr>
                                            <th colspan="2">Accessories</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Air Conditioner</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>AntiLock Braking System</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Steering</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Windows</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>CD Player</td>
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Leather Seats</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Central Locking</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Door Locks</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Brake Assist</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Driver Airbag</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Passenger Airbag</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Crash Sensor</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Engine Check Warning</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Automatic Headlamps</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Side-Bar-->
            <aside class="col-md-4">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fal fa-store-alt" aria-hidden="true"></i> Info Rental </h5>
                    </div>
                    <div class="dealer_detail">
                        <div class="rental_logo">
                            @if($data->Rental->logo)
                            <img src="{{ asset('assets/front/images/') }}" alt="">
                            @else
                            <img src="{{ asset('assets/front/images/rental.png') }}" alt="image">
                            @endif
                        </div>
                        <p class="rental_name">
                            <i class="fal fa-store-alt"></i> <span><a href="{{ route('rental', $data->Rental->username) }}"> {{ $data->Rental->nama }} </a></span>
                        </p>
                        @if($data->Rental->email)
                        <p><span>Email:</span> {{ $data->Rental->email }}</p>
                        @endif
                        @if($data->Rental->no_telp)
                        <p><span>Phone:</span> {{ $data->Rental->no_telp }}</p>
                        @endif
                        @if($data->Rental->alamat)
                        <p><span>Alamat:</span> {!! nl2br($data->Rental->alamat) !!}</p>
                        @endif
                        <a href="{{ route('rental', $data->Rental->username) }}" class="btn btn-xs">Lihat Profil</a>
                    </div>
                </div>
                <div class="sidebar_widger">
                    {{ Form::open(['route' => ['car.create_cart', $data->kode]]) }}
                    <button class="btn btn-block">Sewa Kendaraan</button>
                    {{ Form::close() }}
                </div>
                {{-- <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5>
                            <i class="fal fa-file-invoice" aria-hidden="true"></i> Formulir Penyewaan
                        </h5>
                    </div>
                    <form action="#">
                        <div class="form-group">
                            <label class="form-label">Tanggal Pengambilan</label>
                            <input type="date" class="form-control">                            
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tanggal Pengembalian</label>
                            <input type="date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Waktu Pengambilan</label>
                            <input type="time" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Waktu Pengembalian</label>
                            <input type="time" class="form-control">
                        </div>                        
                        <div class="divider"></div>
                        <div class="price_table">
                            <div class="price_row">
                                Tarif Harian Regular (per hari)
                                <span class="price" id="price">Rp. 200.000 <span class="rent_day" id="rent_day"> x 2 Hari</span></span>
                            </div>
                            <div class="price_row">
                              <br>
                              <span class="subtotal">Rp. 400.000</span>
                            </div>
                        </div>        
                        <div class="divider"></div>  
                        <div class="total_table">
                            <div class="total_row">
                                <span class="total_text">Total</span>
                            </div>  
                            <div class="total_row">
                                <span class="total_price">Rp. 400.000</span>
                            </div>
                        </div>                                      
                        <div class="form-group">                            
                            <input type="submit" value="Lanjutkan ke Penyewaan" class="btn btn-block">
                        </div>
                    </form>
                </div> --}}
            </aside>
            <!--/Side-Bar-->

        </div>

        @if($similar->count() > 4)
        <div class="space-20"></div>
        <div class="divider"></div>
        <!--Similar-Cars-->
        <div class="similar_cars">
            <h3>Mobil Serupa</h3>
            <div class="row">
                @foreach ($similar as $smlr)
                <div class="col-md-3 grid_listing custom">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil18.jpg') }}"
                                    class="img-responsive" alt="image" /> </a>
                            <div class="promo_tag deal">
                                <label class="promo_label deal">Great Deal</label>
                                <label class="promo_label popular">Most Popular</label>
                                <label class="promo_label new">New</label>
                            </div>
                            <div class="rental_info">
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                            <div class="box-price">
                                <span class="old_price">Rp. 200.000</span>
                                <span class="price">Rp 150.000</span>
                            </div>
                            <ul class="features_list">
                                <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                            </ul>
                        </div>
                        <div class="read_btn">
                            <a href="" class="btn btn-block">Info Lanjut</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!--/Similar-Cars-->
        @endif
    </div>
</section>
<!--/Listing-detail-->
@endsection
