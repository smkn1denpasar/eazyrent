@extends('user.misc.front')

@section('content')
<!--Listing-->
<section class="listing-page">
    <div class="container">
        <div class="row">
            <!--Side-Bar-->
            <aside class="col-md-3">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fa fa-filter" aria-hidden="true"></i> Temukan Mobil Idaman </h5>
                    </div>
                    <div class="sidebar_filter">
                        {{ Form::open(['route' => 'car', 'method' => 'get']) }}
                            <div class="form-group">
                                {{ Form::select('lokasi', $wlyh, $request->get('lokasi'), ['class' => 'form-control select2', 'placeholder' => 'Pilih Lokasi']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::select('merk', $merk, $request->get('merk'), ['class' => 'form-control select2', 'placeholder' => 'Pilih Merk']) }}
                            </div>
                            <div class="form-group">
                                <div class="select">
                                    {{ Form::select('transmisi', $tipe, $request->get('transmisi'), ['class' => 'form-control white_bg', 'placeholder' => 'Pilih Tipe Mobil']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label price-tag">Jangkauan Harga (Rp)</label>
                                {{ Form::text('price_range', $request->get('price_range'), ['class' => 'span2', 'id' => 'price_range', 'data-slider-min' => '100000', 'data-slider-max' => '2000000', 'data-slider-step' => '500000', 'data-slider-value' => $request->get('price_range') ? '[' . $request->get('price_range') . ']' : '[100000, 500000]']) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-block"><i class="fa fa-search" aria-hidden="true"></i> Cari Mobil</button>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="sidebar_widget sell_car_quote">
                    <div class="white-text div_zindex text-center">
                        <h3>Buka Rental</h3>
                        <p>Rentalkan Mobil Anda Disini Sekarang Juga!</p>
                        <a href="#" class="btn">Buka Rental <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                    </div>
                    <div class="dark-overlay"></div>
                </div>
            </aside>
            <!--/Side-Bar-->
            @if($car->total())
            <div class="col-md-9">
                <div class="result-sorting-wrapper">
                    <div class="sorting-count">
						<p>{{ $car->firstitem() . " - " . $car->lastitem() }} <span>dari {{ $car->total() }} Item</span></p>
                    </div>
                    <div class="result-sorting-by">
                        <p>Urutkan dari:</p>
                        <form action="#" method="post">
                            <div class="form-group select sorting-select">
                                <select class="form-control ">
                                    <option>Harga ( rendah - tinggi )</option>
                                    <option>Rp. 50.000 - Rp. 100.000</option>
                                    <option>Rp. 100.000 - Rp. 150.000</option>
                                    <option>Rp. 150.000 - Rp. 200.000</option>
                                    <option>Rp. 200.000 lebih</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
				@foreach($car as $data)
                <div class="product-listing-m gray-bg">
                    <div class="product-listing-img">
                        <a href="{{ route('car.detail', $data->kode) }}">
                            <img src="{{ asset('assets/front/images/kp/' . $data->Foto->first()->nama) }}" class="img-responsive" alt="Image" />
                        </a>
                        <div class="promo_tag deal">
                            @if($data->diskon)
                            <label class="promo_label deal">Diskon</label>
                            @endif
                        </div>
                        <div class="rental_info">
                            <a href="{{ route('rental', $data->rental->username) }}" class="store_info"> <i class="fa fa-store-alt"></i> {{ $data->rental->nama }} </a>
                        </div>
                    </div>
                    <div class="product-listing-content">
                        <h5><a href="{{ route('car.detail', $data->kode) }}">{{ $data->fullNameKendaraan }}</a></h5>
                        @if($data->diskon)
                        <span class="old_price">{{ $data->hargaLama }}</span>
                        @endif
                        <p class="price">{{ $data->humanTarif }}</p>
                        <ul>
                            <li><i class="fa fa-road" aria-hidden="true"></i>{{ $data->realTotalKM }}</li>
                            <li><i class="fa fa-power-off" aria-hidden="true"></i>{{ $data->humanTransmisi }}</li>
                            <li><i class="fa fa-user-plus" aria-hidden="true"></i>{{ $data->seat }} Kursi</li>
                            <li><i class="fa fa-calendar-alt" aria-hidden="true"></i>Model {{ $data->tahun }}</li>
                            <li><i class="fa fa-cogs" aria-hidden="true"></i>{{ $data->realBahanBakar }}</li>
                            <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>{{ $data->Rental->Lokasi->nama }}</li>
                        </ul>
                        <a href="{{ route('car.detail', $data->kode) }}" class="btn">View Details <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                    </div>
				</div>
				@endforeach
                <div class="pagination">
					{{ $car->links() }}
                </div>
            </div>
            @else
            <div class="col-md-9">
                <div class="car-not-found text-center">
                    <img src="{{ asset('assets/front/images/tidak_ada_kendaraan.png') }}" alt="" style="width:400px;">
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
<!-- /Listing-->
@endsection

@section('custom-js')
    <script>
        $(".select2-container--default").addClass('white_input');
    </script>
@endsection