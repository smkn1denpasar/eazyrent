@extends('user.misc.front')

@section('content')
<!--About-us-->
<section class="about_us section-padding">
    <div class="container">
        <div class="section-header text-center">
            <h2><span>Verify Your Email Address</span></h2>
            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
            @endif
            <p>Before proceeding, please check your email for a verification link.</p>
            <p>If you did not receive the email, <a href="{{ route('verification.resend') }}">click here to request another.</a></p>
        </div>
    </div>
</section>
<!-- /About-us-->
@endsection