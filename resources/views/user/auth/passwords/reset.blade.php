@extends('user.misc.front')

@section('content')
<section class="section-padding gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-12">
                <div class="user-account-box">
                    <div class="user-account-header">
                        <div class="text-center">
                            <h2 class="user-account-title">Pemulihan Kata Sandi</h2>
                        </div>
                    </div>
                    <div class="user-account-body">
                        {{ Form::open(['route' => 'password.update']) }}
                        <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                                {{ Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Masukkan Email Anda', 'required']) }}
                                @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Masukkan Password Baru Anda', 'required']) }}
                                @if($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Konfirmasi Password Baru Anda', 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Ubah Kata Sandi', ['class' => 'btn btn-block']) }}
                            </div>
                            <p class="user-account-text"><a class="link" href="{{ route('login') }}"><i class="fa fa-angle-double-left"></i> Kembali Masuk</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection