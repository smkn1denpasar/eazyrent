@extends('user.misc.front')

@section('content')
    
<section class="section-padding gray-bg">
    <div class="container">
        <div class="row">                
            <div class="col-md-6 col-md-offset-3 col-12">
                <div class="user-account-box">
                    <div class="user-account-header">
                        <div class="text-center">
                            <h2 class="user-account-title">Masuk</h2>
                        </div>
                    </div>
                    <div class="user-account-body">
                        {{ Form::open(['route' => 'login', 'method' => 'post']) }}
                        <form action="#" method="get">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Alamat Email Anda', 'required']) }}
                                @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Kata Sandi Anda', 'required']) }}
                                @if($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="user-account-help">
                                <div class="form-group checkbox">
                                    {{ Form::checkbox('remember', null, false, ['id' => 'remember']) }}
                                    <label for="remember">Ingatkan Saya</label>
                                </div>
                                <a href="{{ route('password.request') }}">Lupa Password?</a>
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Login', ['class' => 'btn btn-block']) }}
                            </div>
                            <p class="user-account-text">Belum Mempunyai Akun EazyRent? <a class="link" href="{{ route('register') }}">Daftar Sekarang</a></p>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection