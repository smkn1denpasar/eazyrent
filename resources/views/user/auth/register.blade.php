@extends('user.misc.front')

@section('content')
    
    <section class="section-padding gray-bg">
        <div class="container">
            <div class="row">                
                <div class="col-md-6 col-md-offset-3 col-12">
                    <div class="user-account-box">
                        <div class="user-account-header">
                            <div class="text-center">
                                <h2 class="user-account-title">Daftar Akun</h2>
                            </div>
                        </div>
                        <div class="user-account-body">
                            {{ Form::open(['route' => 'register', 'method' => 'post']) }}
                                <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                                    {{ Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap', 'required']) }}
                                    @if($errors->has('nama'))
                                    <span class="help-block">{{ $errors->first('nama') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                                    {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required']) }}
                                    @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                                    {{ Form::number('no_telp', null, ['class' => 'form-control', 'placeholder' => 'No Telepon/Hp', 'required']) }}
                                    @if($errors->has('no_telp'))
                                    <span class="help-block">{{ $errors->first('no_telp') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Kata Sandi', 'required']) }}
                                    @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                    <div class="form-group">
                                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Konfirmasi Kata Sandi', 'required']) }}
                                </div>
                                <div class="form-group checkbox{{ $errors->has('terms_agree') ? ' has-error' : null }}">
                                    {{ Form::checkbox('terms_agree', null, false, ['id' => 'terms_agree', 'required']) }}
                                    <label for="terms_agree">Saya Menyetujui <a href="#">Syarat dan Ketentuan</a></label>
                                    @if($errors->has('terms_agree'))
                                    <span class="help-block">{{ $errors->first('terms_agree') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Daftar', ['class' => 'btn btn-block']) }}
                                </div>
                                <p class="user-account-text">Sudah Mempunyai Akun EazyRent? <a class="link" href="{{ route('login') }}">Masuk Sekarang</a></p>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section>

@endsection