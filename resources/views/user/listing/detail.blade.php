@extends('misc.front.front')

@section('custom-style')
    <style> 
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }
    </style>
@endsection

@section('content')

<!-- Listing-detail-header -->
<section class="listing_detail_header">
    <div class="container">
        <div class="listing_detail_head white-text div_zindex row">
            <div class="col-md-9">
                <h2>BMW 535i, Navi, Leather, ABS</h2>
                <div class="car-location"><span><i class="fa fa-map-marker-alt" aria-hidden="true"></i> Jalan Kebo Iwa
                        Utara III A, Denpasar Barat</span>
                </div>             
            </div>          
        </div>
    </div>
    <div class="dark-overlay"></div>
</section>
<!-- /Listing-detail-header -->

<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="listing_images">
                    <div class="listing_images_slider">
                        <div><img src="{{ asset('assets/front/images/listing_img3.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img4.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img5.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img2.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img1.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img3.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img5.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img2.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img1.jpg') }}" alt="image"></div>
                    </div>
                    <div class="listing_images_slider_nav">
                        <div><img src="{{ asset('assets/front/images/listing_img3.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img4.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img5.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img2.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img1.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img3.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img5.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img2.jpg') }}" alt="image"></div>
                        <div><img src="{{ asset('assets/front/images/listing_img1.jpg') }}" alt="image"></div>
                    </div>
                </div>
                <div class="main_features">
                    <ul>
                        <li> <i class="fa fa-tachometer" aria-hidden="true"></i>
                            <h5>13,000</h5>
                            <p>Total Kilometres</p>
                        </li>
                        <li> <i class="fa fa-calendar" aria-hidden="true"></i>
                            <h5>2010</h5>
                            <p>Reg.Year</p>
                        </li>
                        <li> <i class="fa fa-cogs" aria-hidden="true"></i>
                            <h5>Diesel</h5>
                            <p>Fuel Type</p>
                        </li>
                        <li> <i class="fa fa-power-off" aria-hidden="true"></i>
                            <h5>Automatic</h5>
                            <p>Transmission</p>
                        </li>
                        <li> <i class="fa fa-superpowers" aria-hidden="true"></i>
                            <h5>153KW</h5>
                            <p>Engine</p>
                        </li>
                        <li> <i class="fa fa-user-plus" aria-hidden="true"></i>
                            <h5>5</h5>
                            <p>Seats</p>
                        </li>
                    </ul>
                </div>
                <div class="listing_more_info">
                    <div class="listing_detail_wrap">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs gray-bg" role="tablist">
                            <li role="presentation" class="active"><a href="#vehicle-overview " aria-controls="vehicle-overview"
                                    role="tab" data-toggle="tab">Tinjauan Kendaraan </a></li>
                            <li role="presentation"><a href="#specification" aria-controls="specification" role="tab"
                                    data-toggle="tab">Spesifikasi Teknis</a></li>
                            <li role="presentation"><a href="#accessories" aria-controls="accessories" role="tab"
                                    data-toggle="tab">Aksesoris</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- vehicle-overview -->
                            <div role="tabpanel" class="tab-pane active" id="vehicle-overview">
                                <h4>What is Lorem Ipsum?</h4>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                    have suffered alteration in some form, by injected humour, or randomised words
                                    which don't look even slightly believable. If you are going to use a passage of
                                    Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the
                                    middle of text. All the Lorem Ipsum generators on the Internet tend to repeat
                                    predefined chunks as necessary, making this the first true generator on the
                                    Internet. It uses a dictionary of over 200 Latin words, combined with a handful of
                                    model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to make a type specimen
                                    book. It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged.</p>                                
                            </div>

                            <!-- Technical-Specification -->
                            <div role="tabpanel" class="tab-pane" id="specification">
                                <div class="table-responsive">
                                    <!--Basic-Info-Table-->
                                    <table>
                                        <thead>
                                            <tr>
                                                <th colspan="2">BASIC INFO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Model Year</td>
                                                <td>2010</td>
                                            </tr>
                                            <tr>
                                                <td>No. of Owners</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td>KMs Driven</td>
                                                <td>30,000</td>
                                            </tr>
                                            <tr>
                                                <td>Fuel Type</td>
                                                <td>Diesel</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <!--Technical-Specification-Table-->
                                    <table>
                                        <thead>
                                            <tr>
                                                <th colspan="2">Technical Specification</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Engine Type</td>
                                                <td>TDCI Diesel Engine</td>
                                            </tr>
                                            <tr>
                                                <td>Engine Description</td>
                                                <td>1.5KW</td>
                                            </tr>
                                            <tr>
                                                <td>No. of Cylinders</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td>Mileage-City</td>
                                                <td>22.4kmpl</td>
                                            </tr>
                                            <tr>
                                                <td>Mileage-Highway</td>
                                                <td>25.83kmpl</td>
                                            </tr>
                                            <tr>
                                                <td>Fuel Tank Capacity</td>
                                                <td>40 (Liters)</td>
                                            </tr>
                                            <tr>
                                                <td>Seating Capacity</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <td>Transmission Type</td>
                                                <td>Manual</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Accessories -->
                            <div role="tabpanel" class="tab-pane" id="accessories">
                                <!--Accessories-->
                                <table>
                                    <thead>
                                        <tr>
                                            <th colspan="2">Accessories</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Air Conditioner</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>AntiLock Braking System</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Steering</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Windows</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>CD Player</td>
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Leather Seats</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Central Locking</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Power Door Locks</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Brake Assist</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Driver Airbag</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Passenger Airbag</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Crash Sensor</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Engine Check Warning</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                        <tr>
                                            <td>Automatic Headlamps</td>
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--Vehicle-Video-->
                    <div class="video_wrap">
                        <h6>Tonton Video </h6>
                        <div class="video-box">
                            <iframe class="mfp-iframe" src="https://www.youtube.com/embed/rqSoXtKMU3Q" allowfullscreen></iframe>
                        </div>
                    </div>

                    <!--Comment-Form-->
                    <div class="comment_form">
                        <h6>Leave a Comment</h6>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <textarea rows="5" class="form-control" placeholder="Comments"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn" value="Submit Comment">
                            </div>
                        </form>
                    </div>
                    <!--/Comment-Form-->

                </div>
            </div>

            <!--Side-Bar-->
            <aside class="col-md-4">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fas fa-address-card" aria-hidden="true"></i> Info Rental </h5>
                    </div>
                    <div class="dealer_detail">
                        <div class="rental_logo">
                            <img src="{{ asset('assets/front/images/rental2.jpg') }}" alt="image">
                        </div>
                        <p class="rental_name"><i class="fas fa-store-alt"></i> <span><a href=""> Rheznendra Rental </a></span>
                        </p>
                        <p><span>Email:</span> contact@example.com</p>
                        <p><span>Phone:</span> +61-1234-5678-09</p>
                        <p><span>Alamat:</span> Jalan Kebo Iwa Utara, Denpasar</p>
                        <a href="#" class="btn btn-xs">Lihat Profil</a>
                    </div>
                </div>
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fal fa-file-invoice" aria-hidden="true"></i> Formulir Penyewaan</h5>
                    </div>
                    <form action="#">
                        <div class="form-group">
                            <label class="form-label">Tanggal Pengambilan</label>
                            <input type="date" class="form-control">                            
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tanggal Pengembalian</label>
                            <input type="date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Waktu Pengambilan</label>
                            <input type="time" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Waktu Pengembalian</label>
                            <input type="time" class="form-control">
                        </div>                        
                        <div class="divider"></div>
                        <div class="price_table">
                            <div class="price_row">
                              Tarif Harian Regular (per hari)
                              <span class="price" id="price">Rp. 200.000 <span class="rent_day" id="rent_day"> x 2 Hari</span></span>
                            </div>
                            <div class="price_row">
                              <br>
                              <span class="subtotal">Rp. 400.000</span>
                            </div>
                        </div>        
                        <div class="divider"></div>  
                        <div class="total_table">
                          <div class="total_row">
                            <span class="total_text">Total</span>
                          </div>  
                          <div class="total_row">
                            <span class="total_price">Rp. 400.000</span>
                          </div>
                        </div>                                      
                        <div class="form-group">                            
                            <input type="submit" value="Lanjutkan ke Penyewaan" class="btn btn-block">
                        </div>
                    </form>
                </div>
            </aside>
            <!--/Side-Bar-->

        </div>
        <div class="space-20"></div>
        <div class="divider"></div>

        <!--Similar-Cars-->
        <div class="similar_cars">
            <h3>Mobil Serupa</h3>
            <div class="row">
                <div class="col-md-3 grid_listing custom">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/featured-img-3.jpg') }}"
                                    class="img-responsive" alt="image" /> </a>
                            <div class="promo_tag deal">
                                <label class="promo_label deal">Great Deal</label>
                                <label class="promo_label popular">Most Popular</label>
                                <label class="promo_label new">New</label>
                            </div>
                            <div class="rental_info">
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                            <div class="box-price">
                                <span class="old_price">Rp. 200.000</span>
                                <span class="price">Rp 150.000</span>
                            </div>
                            <ul class="features_list">
                                <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                            </ul>
                        </div>
                        <div class="read_btn">
                            <a href="" class="btn btn-block">Info Lanjut</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 grid_listing custom">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/featured-img-2.jpg') }}"
                                    class="img-responsive" alt="image" /> </a>
                            <div class="promo_tag deal">
                                <label class="promo_label deal">Great Deal</label>
                                <label class="promo_label popular">Most Popular</label>
                                <label class="promo_label new">New</label>
                            </div>
                            <div class="rental_info">
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                            <div class="box-price">
                                <span class="old_price">Rp. 200.000</span>
                                <span class="price">Rp 150.000</span>
                            </div>
                            <ul class="features_list">
                                <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                            </ul>
                        </div>
                        <div class="read_btn">
                            <a href="" class="btn btn-block">Info Lanjut</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 grid_listing custom">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/featured-img-1.jpg') }}" class="img-responsive" alt="image" /> </a>
                            <div class="promo_tag deal">
                                <label class="promo_label deal">Great Deal</label>
                                <label class="promo_label popular">Most Popular</label>
                                <label class="promo_label new">New</label>
                            </div>
                            <div class="rental_info">
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                            <div class="box-price">
                                <span class="old_price">Rp. 200.000</span>
                                <span class="price">Rp 150.000</span>
                            </div>
                            <ul class="features_list">
                                <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                            </ul>
                        </div>
                        <div class="read_btn">
                            <a href="" class="btn btn-block">Info Lanjut</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 grid_listing custom">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/featured-img-3.jpg') }}"
                                    class="img-responsive" alt="image" /> </a>
                            <div class="promo_tag deal">
                                <label class="promo_label deal">Great Deal</label>
                                <label class="promo_label popular">Most Popular</label>
                                <label class="promo_label new">New</label>
                            </div>
                            <div class="rental_info">
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                            <div class="box-price">
                                <span class="old_price">Rp. 200.000</span>
                                <span class="price">Rp 150.000</span>
                            </div>
                            <ul class="features_list">
                                <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                            </ul>
                        </div>
                        <div class="read_btn">
                            <a href="" class="btn btn-block">Info Lanjut</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/Similar-Cars-->

    </div>
</section>
<!--/Listing-detail-->

@include('misc.front.footer')
@endsection
