@extends('misc.front.front')

@section('content')
    <!--Listing-->
<section class="listing-page">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-push-3">
          <div class="result-sorting-wrapper">
            <div class="sorting-count">
              <p>1 - 4 <span>dari 50 Item</span></p>
            </div>
            <div class="result-sorting-by">
              <p>Urutkan dari:</p>
              <form action="#" method="post">
                <div class="form-group select sorting-select">
                  <select class="form-control ">
                    <option>Harga ( rendah - tinggi )</option>
                    <option>Rp. 50.000 - Rp. 100.000</option>
                    <option>Rp. 100.000 - Rp. 150.000</option>
                    <option>Rp. 150.000 - Rp. 200.000</option>
                    <option>Rp. 200.000 lebih</option>
                  </select>
                </div>
              </form>
            </div>
          </div>
          <div class="product-listing-m gray-bg">
            <div class="product-listing-img"> 
              <a href="#"><img src="{{ asset('assets/front/images/featured-img-2.jpg') }}" class="img-responsive" alt="Image" /> </a>
              <div class="promo_tag deal">                
                <label class="promo_label deal">Great Deal</label>    
                <label class="promo_label popular">Most Popular</label>       
                <label class="promo_label new">New</label>         
              </div>  
              <div class="rental_info">
                  <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>           
              </div>                    
            </div>
            <div class="product-listing-content">
              <h5><a href="#">Mazda CX-5 SX, V6, ABS, Sunroof</a></h5>            
              <span class="old_price">Rp. 200.000</span>                          
              <p class="price">Rp. 150,000</p>
              <ul>
                <li><i class="fa fa-road" aria-hidden="true"></i>0,000 km</li>
                <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                <li><i class="fa fa-user" aria-hidden="true"></i>5 seats</li>
                <li><i class="fa fa-calendar" aria-hidden="true"></i>2005 model</li>
                <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Denpasar</li>
              </ul>
              <a href="#" class="btn">View Details <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>            
            </div>
          </div>
          <div class="product-listing-m gray-bg">
              <div class="product-listing-img"> 
                <a href="#"><img src="{{ asset('assets/front/images/featured-img-2.jpg') }}" class="img-responsive" alt="Image" /> </a>
                <div class="promo_tag deal">                                
                  <label class="promo_label popular">Most Popular</label>       
                  <label class="promo_label new">New</label>         
                </div>  
                <a href="#">
                <div class="rental_info">
                    <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>           
                </div>        
                </a>                
              </div>
              <div class="product-listing-content">
                <h5><a href="#">Mazda CX-5 SX, V6, ABS, Sunroof</a></h5>                     
                <p class="price">Rp. 150,000</p>
                <ul>
                  <li><i class="fa fa-road" aria-hidden="true"></i>0,000 km</li>
                  <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                  <li><i class="fa fa-user" aria-hidden="true"></i>5 seats</li>
                  <li><i class="fa fa-calendar" aria-hidden="true"></i>2005 model</li>
                  <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                  <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Denpasar</li>
                </ul>
                <a href="#" class="btn">View Details <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>            
              </div>
          </div>
          <div class="product-listing-m gray-bg">
              <div class="product-listing-img"> 
                <a href="#"><img src="{{ asset('assets/front/images/featured-img-2.jpg') }}" class="img-responsive" alt="Image" /> </a>
                <div class="promo_tag deal">                                
                  <label class="promo_label popular">Most Popular</label>       
                  <label class="promo_label new">New</label>         
                </div>  
                <div class="rental_info">
                    <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>           
                </div>                    
              </div>
              <div class="product-listing-content">
                <h5><a href="#">Mazda CX-5 SX, V6, ABS, Sunroof</a></h5>                        
                <p class="price">Rp. 150,000</p>
                <ul>
                  <li><i class="fa fa-road" aria-hidden="true"></i>0,000 km</li>
                  <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                  <li><i class="fa fa-user" aria-hidden="true"></i>5 seats</li>
                  <li><i class="fa fa-calendar" aria-hidden="true"></i>2005 model</li>
                  <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                  <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Denpasar</li>
                </ul>
                <a href="#" class="btn">View Details <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>            
              </div>
          </div>
          <div class="product-listing-m gray-bg">
              <div class="product-listing-img"> 
                <a href="#"><img src="{{ asset('assets/front/images/featured-img-2.jpg') }}" class="img-responsive" alt="Image" /> </a>
                <div class="promo_tag deal">                
                  <label class="promo_label deal">Great Deal</label>    
                  <label class="promo_label popular">Most Popular</label>       
                  <label class="promo_label new">New</label>         
                </div>  
                <div class="rental_info">
                    <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>           
                </div>                    
              </div>
              <div class="product-listing-content">
                <h5><a href="#">Mazda CX-5 SX, V6, ABS, Sunroof</a></h5>            
                <span class="old_price">Rp. 200.000</span>                          
                <p class="price">Rp. 150,000</p>
                <ul>
                  <li><i class="fa fa-road" aria-hidden="true"></i>0,000 km</li>
                  <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                  <li><i class="fa fa-user" aria-hidden="true"></i>5 seats</li>
                  <li><i class="fa fa-calendar" aria-hidden="true"></i>2005 model</li>
                  <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                  <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Denpasar</li>
                </ul>
                <a href="#" class="btn">View Details <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>            
              </div>
            </div>

          <div class="pagination">
            <ul>
              <li class="current">1</li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
            </ul>
          </div>
        </div>
        
        <!--Side-Bar-->
        <aside class="col-md-3 col-md-pull-9">
          <div class="sidebar_widget">
            <div class="widget_heading">
              <h5><i class="fa fa-filter" aria-hidden="true"></i> Temukan Mobil Idaman </h5>
            </div>
            <div class="sidebar_filter">
              <form action="#" method="get">
                <div class="form-group select">
                  <select class="form-control">
                    <option>Pilih Lokasi</option>
                    <option>Denpasar</option>
                    <option>Badung</option>
                    <option>Tabanan</option>
                    <option>Gianyar</option>
                    <option>Klungkung</option>
                  </select>
                </div>
                <div class="form-group select">
                  <select class="form-control">
                    <option>Pilih Merk</option>
                    <option>Audi</option>
                    <option>BMW</option>
                    <option>Nissan</option>
                    <option>Toyota</option>
                    <option>Volvo</option>
                    <option>Mazda</option>
                    <option>Mercedes-Benz</option>
                    <option>Lotus</option>
                  </select>
                </div>
                <div class="form-group select">
                  <select class="form-control">
                    <option>Pilih Model</option>
                    <option>Series 1</option>
                    <option>Series 2</option>
                    <option>Series 3</option>
                  </select>
                </div>
                <div class="form-group select">
                  <select class="form-control">
                    <option>Type Mobil </option>
                    <option>Matic</option>
                    <option>Manual</option>
                  </select>
                </div>              
                <div class="form-group">
                    <label class="form-label price-tag">Jangkauan Harga (Rp)</label>
                    <input id="price_range" type="text" class="span2" value="" data-slider-min="50000" data-slider-max="500000" data-slider-step="50000" data-slider-value="[100000,250000]"/>
                </div>              
                <div class="form-group">
                  <button type="submit" class="btn btn-block"><i class="fa fa-search" aria-hidden="true"></i> Cari Mobil</button>
                </div>
              </form>
            </div>
          </div>
  
          <div class="sidebar_widget sell_car_quote">
            <div class="white-text div_zindex text-center">
              <h3>Buka Rental</h3>
              <p>Rentalkan Mobil Anda Disini Sekarang Juga!</p>
              <a href="#" class="btn">Buka Rental <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a> </div>
            <div class="dark-overlay"></div>
          </div>        
        </aside>
        <!--/Side-Bar--> 
      </div>
    </div>
  </section>
  <!-- /Listing--> 

  @include('misc.front.footer')
@endsection