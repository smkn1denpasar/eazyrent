@extends('user.misc.front')

@section('content')
<!-- Banners -->
<section id="banner" class="banner-section">
    <div class="container">
        <div class="div_zindex">
            <div class="row">
                <div class="col-md-5 col-md-push-7">
                    <div class="banner_content">
                        <h1>Cari Mobil terbaik untuk Anda.</h1>
                        <p>Kami memiliki ratusan mobil yang siap Anda gunakan. </p>
                        <a href="listing-classic.html" class="btn">Cari Mobil <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Banners -->

<!-- Filter-Form -->
<section id="filter_form" class="gray-bg">
    <div class="container">
        <h3>Cari Mobil Dengan Mudah <span></span></h3>
        <div class="row">
            <form action="#" method="get">
                <div class="form-group col-md-4 col-sm-6">
					{{ Form::select('lokasi', $wlyh, null, ['class' => 'form-control select2 js-example-basic-single', 'placeholder' => 'Pilih Lokasi']) }}
                </div>
                <div class="form-group col-md-4 col-sm-6">
					{{ Form::select('merk', $merk, null, ['class' => 'form-control select2 js-example-basic-single', 'placeholder' => 'Pilih Merk']) }}
                </div>
                <div class="form-group black_input col-md-4 col-sm-6">
					<div class="select">
						{{ Form::select('tipe', $tipe, [], ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Tipe Mobil']) }}
					</div>
                </div>
                <div class="form-group col-md-9 col-sm-9">
					<label class="form-label price-tag">Jangkauan Harga (Rp)</label>
					{{ Form::text('price_range', null, ['class' => 'span2', 'id' => 'price_range', 'data-slider-min' => '100000', 'data-slider-max' => '2000000', 'data-slider-step' => '500000', 'data-slider-value' => '[100000, 500000]']) }}
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <button type="submit" class="btn btn-block"><i class="fa fa-search custom" aria-hidden="true"></i> Cari Mobil </button>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- /Filter-Form -->

<!-- About -->
<section class="about-us-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-offset-1">
                <div class="section-header text-left custom">
                    <h2 class="custom">Selamat Datang di <span class="color-blue">EazyRent</span></h2>
                    <p class="custom">
                        Ada banyak variasi bagian dari Lorem Ipsum yang tersedia, tetapi sebagian besar telah mengalami
                        perubahan dalam beberapa bentuk, dengan menyuntikkan humor, atau kata-kata acak yang tidak
                        terlihat bahkan sedikit dapat dipercaya. Jika Anda akan menggunakan bagian dari Lorem Ipsum,
                        Anda harus yakin tidak ada sesuatu yang memalukan yang tersembunyi di tengah teks.</p>
                    <a href="#" class="btn">Cari Mobil<i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 car-box">
                <img src="{{ asset('assets/front/images/looking-used-car.png') }}" alt="Image" class="image-car" />
            </div>
        </div>
    </div>
</section>
<!-- /About -->

<!-- Resent Cat-->
<section class="section-padding gray-bg">
    <div class="container">
        <div class="section-header text-center">
            <h2> <span class="color-blue fw-600">Spesial</span> <span>Untuk Anda</span></h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, numquam eius ex enim nisi quam.</p>
        </div>
        <div class="row">

            <!-- Recently Listed New Cars -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="resentnewcar">
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-1.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label deal">Great Deal!</label>
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">
                                    <span class="old_price">Rp. 500.000</span>
                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-2.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> AndraStore </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">

                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-2.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> AndraStore </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">

                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-1.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label deal">Great Deal!</label>
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">
                                    <span class="old_price">Rp. 500.000</span>
                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-2.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> AndraStore </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">

                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> <a href="#"><img src="{{ asset('assets/front/images/recent-car-1.jpg') }}"
                                        class="img-responsive" alt=""></a>
                                <div class="promo_tag deal">
                                    <label class="promo_label deal">Great Deal!</label>
                                    <label class="promo_label popular">Most Popular!</label>
                                </div>
                                <ul>
                                    <li><i class="fa fa-user" aria-hidden="true"></i>Maks 6 Orang</li>
                                    <li><i class="fa fa-map-marker-alt" aria-hidden="true"></i>Colorado, USA</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <a href="#">Ford Shelby GT350</a>
                                <a href="#" class="store_info"> <i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                            </div>
                            <div class="inventory_info_m">
                                <div class="box-price">
                                    <span class="old_price">Rp. 500.000</span>
                                    <span class="price">Rp. 450.000</span>
                                </div>
                            </div>
                            <div class="read_btn">
                                <a href="" class="btn btn-block">Info Lanjut</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Resent Cat -->

<!-- Fun Facts-->
<section class="fun-facts-section">
    <div class="container div_zindex">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="fun-facts-header">
                    <h2>Sewa Dengan Mudah</h2>
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 col-sm-3 mb-24 respon">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fal fa-search" aria-hidden="true"></i></h2>
                    </div>
                </div>
                <div class="fun-facts-text">
                    <p>Cari Mobil Yang Ingin Anda Sewa</p>
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 col-sm-3 mb-24 respon">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fal fa-file-invoice" aria-hidden="true"></i></h2>
                    </div>
                </div>
                <div class="fun-facts-text">
                    <p>Isi Formulir Penyewaan</p>
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 col-sm-3 mb-24 respon">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fal fa-money-bill-wave" aria-hidden="true"></i></h2>
                    </div>
                </div>
                <div class="fun-facts-text">
                    <p>Lakukan Pembayaran</p>
                </div>
            </div>
            <div class="col-lg-3 col-xs-12 col-sm-3 mb-24 respon">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fal fa-car" aria-hidden="true"></i></h2>
                    </div>
                </div>
                <div class="fun-facts-text">
                    <p>Mobil Siap Dipakai</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Fun Facts-->

<!--Trending Rental-->
<section class="section-padding gray-bg">
    <div class="container">
        <div class="section-header text-center">
            <h2> <span class="color-blue fw-600">Trending</span> <span>Rental</span></h2>
            <p class="custom limit-width"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptas numquam,
                reiciendis natus non laboriosam quasi, perspiciatis consequatur sapiente, aut quisquam iste sint enim
                ea esse? </p>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="trending_slider">
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">Ford Shelby GT350</a></h4>
                        </div>
                    </div>
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">Toyota Corolla</a></h4>
                        </div>
                    </div>
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">Volvo v40</a></h4>
                        </div>
                    </div>
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">Toyota Corolla</a></h4>
                        </div>
                    </div>
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">Mazda CX-5 SX, V6, ABS</a></h4>
                        </div>
                    </div>
                    <div class="trending-car-m">
                        <div class="trending-car-img"> <img src="{{ asset('assets/front/images/trending-car-img-1.jpg') }}"
                                alt="Image" class="img-responsive" /> </div>
                        <div class="trending-hover">
                            <h4><a href="#">BMW 535i</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Trending Rental-->

<!--Testimonial -->
<section class="section-padding testimonial-section parallex-bg">
    <div class="container div_zindex">
        <div class="section-header white-text text-center">
            <h2>Our Satisfied <span>Customers</span></h2>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                alteration in some form, by injected humour, or randomised words which don't look even slightly
                believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything
                embarrassing hidden in the middle of text. </p>
        </div>
        <div class="row">
            <div id="testimonial-slider">
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-1.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Donald Brooks</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-2.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Enzo Giovanotelli</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-1.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Donald Brooks</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-2.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Enzo Giovanotelli</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-1.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Donald Brooks</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
                <div class="testimonial-m">
                    <div class="testimonial-img"> <img src="{{ asset('assets/front/images/testimonial-img-2.jpg') }}"
                            alt="" /> </div>
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>Enzo Giovanotelli</h5>
                            <span class="client-designation">CEO of xzy company</span>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                            cupiditate non provident, similique sunt .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Testimonial-->

@include('user.misc.footer')

@if(Auth::guest())
@include('user.auth')
@endif

@endsection