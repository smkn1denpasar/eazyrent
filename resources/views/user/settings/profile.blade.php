@extends('user.misc.front')

@section('content')
<!--Profile-setting-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-ext')
            <div class="col-md-6 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Pengaturan Umum</h5>
                    {{ Form::open(['route' => 'profile']) }}
                        <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : null }}">
                            {{ Form::label('no_ktp', 'No KTP', ['class' => 'control-label']) }}
                            {{ Form::number('no_ktp', Auth::user()->no_ktp, ['class' => 'form-control white_bg', !Auth::user()->no_ktp ?: 'disabled']) }}
                            @if($errors->has('no_ktp'))
                                <span class="help-block">{{ $errors->first('no_ktp') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                            {{ Form::label('nama', 'Nama', ['class' => 'control-label']) }}
                            {{ Form::text('nama', Auth::user()->nama, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('nama'))
                                <span class="help-block">{{ $errors->first('nama') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                            {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                            {{ Form::email('email', Auth::user()->email, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                            <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                            {{ Form::label('no_telp', 'Nomor Telepon/Hp', ['class' => 'control-label']) }}
                            {{ Form::number('no_telp', Auth::user()->no_telp, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('no_telp'))
                                <span class="help-block">{{ $errors->first('no_telp') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                            {{ Form::label('alamat', 'Alamat', ['class' => 'control-label']) }}
                            {{ Form::textarea('alamat', Auth::user()->alamat, ['class' => 'form-control white_bg', 'rows' => 4]) }}
                            @if($errors->has('alamat'))
                                <span class="help-block">{{ $errors->first('alamat') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('jenis_kelamin') ? ' has-error' : null }}">
                            {{ Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'control-label']) }}
                            {{ Form::select('jenis_kelamin', $gender, \Auth::user()->jenis_kelamin, ['class' => 'form-control white_bg', 'placeholder' => 'Pilih Jenis Kelamin']) }}
                            @if($errors->has('jenis_kelamin'))
                                <span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('kode_lokasi') ? ' has-error' : null }}">
                            {{ Form::label('kode_lokasi', 'Wilayah', ['class' => 'control-label']) }}
                            {{ Form::select('kode_lokasi', $wlyh, \Auth::user()->kode_lokasi, ['class' => 'form-control select2 white_bg', 'placeholder' => 'Pilih Wilayah Anda']) }}
                            @if($errors->has('kode_lokasi'))
                                <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                            @endif
                        </div>
                        <div class="gray-bg field-title">
                            <h6>Ubah Kata Sandi</h6>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                            {{ Form::label('password', 'Password', ['class' => 'control-label']) }}
                            {{ Form::password('password', ['class' => 'form-control white_bg']) }}
                            @if($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'Konfirmasi Password', ['class' => 'control-label']) }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control white_bg']) }}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn">Simpan Perubahan <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Profile-setting-->
@endsection

@section('custom-js')
<script>
    $(".select2-container--default").addClass("white_input");
</script>
@endsection