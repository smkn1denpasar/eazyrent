@extends('user.misc.front')

@section('content')
<!-- Riwayat Transaksi -->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-ext')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Riwayat Transaksi</h5>
                    @if($data->total())
                    <div class="my_vehicles_list">
                        <ul class="vehicle_listing">
                            @foreach($data as $transaksi)
                            <li>
                                <div class="vehicle_img">
                                    <a href="{{ route('riwayat-transaksi.detail', $transaksi->kode) }}"><img src="{{ asset('assets/front/images/kp/' . $transaksi->Kendaraan->Foto->first()->nama) }}" alt="image"></a>
                                </div>
                                <div class="vehicle_title">
                                    <h6>{{ $transaksi->Kendaraan->fullNameKendaraan }}</h6>
                                    <div>
                                        <span class="rent-code">#{{ $transaksi->kode }}</span>
                                        <span class="seperate-symbol">|</span>
                                        <span class="rent-date">{{ $transaksi->tanggal_transaksi }}</span>
                                    </div>
                                    <span class="rent-store">
                                        <a href="{{ route('rental', $transaksi->Kendaraan->Rental->username) }}">
                                            <i class="fa fa-store-alt" style="margin-right:6px;margin-bottom:12px;"></i>{{ $transaksi->Kendaraan->Rental->nama }}
                                        </a>
                                    </span>  
                                </div>
                                <div class="vehicle_status">
                                    <a href="{{ route('riwayat-transaksi.detail', $transaksi->kode) }}" class="btn btn-xs btn-solid">Detail</a>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="pagination">
                            {{ $data->links() }}
                        </div>
                    </div>
                    @else
                        <div class="car-not-found text-center">
                            <img src="{{ asset('assets/front/images/order-history-not-found.png') }}" alt="">
                        </div>
                    @endif
                </div>
            </div>            
        </div>
    </div>
</section>
<!--/Riwayat Transaksi-->
@endsection
