@extends('user.misc.front')

@section('content')
<!--Page Header-->
<section class="page-header aboutus_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <h1>Lorem Ipsum</h1>
            </div>            
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header-->

<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="text-center">
                    <div class="success-payment">                        
                        <div class="checklist">
                            <span>
                                <i class="fa fa-check"></i>
                            </span>
                        </div>
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque earum minima enim officiis omnis, iste consequatur deleniti natus. Commodi voluptatum at quae cum quisquam labore impedit fuga eius obcaecati nihil!</p>
                        <a href="{{ route('riwayat-transaksi') }}" class="btn">Cek Invoice</a>
                    </div>                    
                </div>                
            </div>
        </div>
    </div>
</section>

@endsection
