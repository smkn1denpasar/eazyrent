@extends('user.misc.front')

@section('custom-style')
<style>
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }
    </style>
@endsection

@section('content')

<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
				<div class="sidebar_widget custom">
					<div class="widget_heading">
						<p class="title_heading"><i class="fa fa-file-invoice"></i>Formulir Penyewaan</p>
						<p class="subtitle">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae quae earum est laborum itaque ab nulla labore possimus id amet, ipsa error. Cupiditate reprehenderit voluptatem velit laudantium placeat, ad mollitia.</p>
					</div>
					<div class="payment_step">
						<p>Langkah 2 : <span class="italic">Masukkan Rincian Anda</span></p>
						<div class="form_payment">
							{{ Form::open(['route' => ['car.sewa.step_2', $data->kode]]) }}
								<div class="form-group{{ $errors->has('nama') ? ' has-error' : null  }}">
									{{ Form::label('nama', 'Nama *', ['class' => 'form-label']) }}
									{{ Form::text('nama', null, ['class' => 'form-control', 'required', 'placeholder' => 'Masukkan Nama Anda']) }}
									@if($errors->has('nama'))
									<span class="help-block">{{ $errors->first('nama') }}</span>
									@endif
								</div>
								<div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null  }}">
									{{ Form::label('no_telp', 'Nomor Telepon *', ['class' => 'form-label']) }}
									{{ Form::number('no_telp', null, ['class' => 'form-control', 'required', 'placeholder' => 'Masukkan Nomor Telepon Anda']) }}
									@if($errors->has('no_telp'))
									<span class="help-block">{{ $errors->first('no_telp') }}</span>
									@endif
								</div>
								<div class="form-group{{ $errors->has('email') ? ' has-error' : null  }}">
									{{ Form::label('email', 'Email *', ['class' => 'form-label']) }}
									{{ Form::email('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'Masukkan Email Anda']) }}
									@if($errors->has('no_telp'))
									<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>
								<div class="form-group{{ $errors->has('informasi') ? ' has-error' : null  }}">
									{{ Form::label('informasi', 'Informasi / Catatan', ['class' => 'form-label']) }}
									{{ Form::textarea('informasi', null, ['class' => 'form-control custom', 'rows' => 5]) }}
									@if($errors->has('informasi'))
									<span class="help-block">{{ $errors->first('informasi') }}</span>
									@endif
								</div>
								<div class="form-group">
									{{ Form::button('Lanjutkan', ['class' => 'btn btn-xs', 'type' => 'submit']) }}
								</div>
							{{ Form::close() }}
						</div>
					</div>
				</div>
            </div>
            <!--Side-Bar-->
            <aside class="col-md-4">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <img src="{{ asset('assets/front/images/kp/' . $data->Foto->first()->nama) }}" alt="Images" class="car_image">
                        <div class="form_review">
                            <span class="car_name">{{ $data->fullNameKendaraan }}</span>
                            <div class="pickup_table">
                                <div class="pickup_row">
                                    <span class="title">Pengambilan</span>
                                    <span class="date">{{ date('d M Y', strtotime($twp_1)) }}</span>
                                    <span class="date">{{ date('H:i', strtotime($twp_1)) }}</span>
                                </div>
                                <div class="pickup_row">
                                    <span class="title">Pengembalian</span>
                                    <span class="date">{{ date('d M Y', strtotime($twp_2)) }}</span>
                                    <span class="date">{{ date('H:i', strtotime($twp_1)) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
					<div class="price_table">
						<div class="price_row">
							Tarif Harian Regular (per hari)
							<span class="price" id="price">{{ $data->humanTarif }}<span class="rent_day" id="rent_day"> x {{ $day }} Hari</span></span>
						</div>
					</div>
					<div class="divider"></div>
					<div class="total_table">
						<div class="total_row">
							<span class="total_text">Total</span>
						</div>
						<div class="total_row">
							<span class="total_price">Rp. {{ number_format($data->total_harga*$day, 0, ",", ".") }}</span>
						</div>
					</div>
                </div>
            </aside>
            <!--/Side-Bar-->
        </div>
    </div>
</section>
<!--/Listing-detail-->
@endsection
