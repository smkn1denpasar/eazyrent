@extends('user.misc.front')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/front/bower_components/jquery-datetimepicker/build/jquery.datetimepicker.min.css') }}">
@endsection

@section('custom-style')
    <style> 
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }
    </style>
@endsection

@section('content')

<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="sidebar_widget custom">
                    <div class="widget_heading">
                        <p class="title_heading"><i class="fa fa-file-invoice"></i>Formulir Penyewaan</p>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi quae culpa nisi, illo similique dignissimos sint natus, vitae corporis esse, eos at reprehenderit. Recusandae saepe assumenda nihil ratione, deleniti commodi.</p>
                    </div>
                    <div class="payment_step">
                        <p>Langkah 1 : <span class="italic">Tentukan dan Waktu Penyewaan</span></p>                        
                        <div class="form_payment">
                            {{ Form::open(['route' => ['car.sewa.step_1', $data->kode]]) }} 
                                <div class="form-group{{ $errors->has('tanggal_waktu_pengambilan') ? ' has-error' : null  }}">
                                    {{ Form::label('tanggal_waktu_pengambilan', 'Tanggal & Waktu Pengambilan *', ['class' => 'form-label']) }}
                                    {{ Form::text('tanggal_waktu_pengambilan', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => 'Pilih Tanggal & Waktu Pengambilan']) }}
                                    @if($errors->has('tanggal_waktu_pengambilan'))
                                    <span class="help-block">{{ $errors->first('tanggal_waktu_pengambilan') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('tanggal_waktu_pengembalian') ? ' has-error' : null  }}">
                                    {{ Form::label('tanggal_waktu_pengembalian', 'Tanggal & Waktu Pengembalian *', ['class' => 'form-label']) }}
                                    {{ Form::text('tanggal_waktu_pengembalian', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'disabled', 'placeholder' => 'Pilih Tanggal & Waktu Pengembalian']) }}
                                    @if($errors->has('tanggal_waktu_pengembalian'))
                                    <span class="help-block">{{ $errors->first('tanggal_waktu_pengembalian') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {{ Form::button('Lanjutkan', ['class' => 'btn btn-xs', 'type' => 'submit']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            <!--Side-Bar-->
            <aside class="col-md-4">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <img src="{{ asset('assets/front/images/kp/' . $data->Foto->first()->nama) }}" alt="Images" class="car_image">
                        <div class="form_review">
                            <span class="car_name">{{ $data->fullNameKendaraan }}</span>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="promo_code">
                        <span class="head_title"><i class="fa fa-barcode-alt"></i> Kode Promo</span>
                        <form action="#">
                            <input type="text" placeholder="Masukkan Kode Promo">
                            <input type="button" value="Pakai">
                        </form>
                    </div>              
                    <div class="divider"></div>
                    <div class="price_table">
                        <div class="price_row">
                            Tarif Harian (per hari)
                            <span class="price" id="price">{{ $data->humanTarif }} <span class="rent_day" id="rent_day"></span></span>
                        </div>
                        <div class="price_row">
                            <br>
                            <span class="subtotal"></span>
                        </div>
                    </div>        
                    <div class="divider"></div>  
                    <div class="total_table">
                        <div class="total_row">
                            <span class="total_text">Total</span>
                        </div>  
                        <div class="total_row">
                            <span class="total_price" id="total_price">{{ $data->humanTarif }}</span>
                        </div>
                    </div>
                </div>
            </aside>
            <!--/Side-Bar-->

        </div>

    </div>
</section>
<!--/Listing-detail-->
@endsection

@section('custom-js')
<script src="{{ asset('assets/front/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/front/bower_components/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js') }}"></script>
    <script>
        var now = moment().hour()
        if(now > 17) {
            minDate = moment().add(2, 'days').format('YYYY/MM/D');
        } else {
            minDate = moment().add(1, 'days').format('YYYY/MM/D');
        }
        $("#tanggal_waktu_pengambilan").on("keyup", function() {
            $("#tanggal_waktu_pengembalian").datetimepicker("destroy").val(null).attr("disabled", "disabled");
        });
        $("#tanggal_waktu_pengambilan, #tanggal_waktu_pengembalian").on("keyup", function() {
            $("#rent_day").html(null);
            $("#total_price").html("{{ $data->humanTarif }}");
        });
        var a = $("#tanggal_waktu_pengambilan").val();
        var b = $("#tanggal_waktu_pengembalian").val();
        $("#tanggal_waktu_pengambilan").datetimepicker({
            format: 'Y-m-d H:i',
            maxTime: '17:00',
            minDate: minDate,
            onGenerate: function(ct)
            {
                if(a) tp(ct);
            },
            onSelectDate: function(date) {
                tp(date);
            }
        });
        function tp(date)
        {
            var selectedDate = new Date(date);
            var msecsInADay = 86400000;
            minDateP = new Date(selectedDate.getTime() + msecsInADay);
            $("#tanggal_waktu_pengembalian").datetimepicker({
                format: 'Y-m-d H:i',
                minDate: minDateP,
                maxTime: '17:00',
                onSelectDate: function(new_date) {
                    var kembali = new Date(new_date);
                    var day = new Date(kembali - selectedDate).getDate()-1;
                    detail(day);
                },
                onGenerate: function(current_time)
                {
                    var kembali = new Date(current_time);
                    var day = new Date(kembali - selectedDate).getDate()-1;
                    if(b) detail(day);
                }
            }).removeAttr("disabled");
        }
        function detail(day)
        {
            $("#rent_day").html("x " + day + " Hari");
            var price = {{ $data->total_harga }};
            $("#total_price").html(convertToRupiah(price*day));           
        }
    </script>
@endsection
