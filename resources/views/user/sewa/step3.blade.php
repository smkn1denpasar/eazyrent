@extends('user.misc.front')

@section('custom-style')
<style>
    .display-hidden {
        display: none;
    }
    .rental_info {
        padding: 10px!important;
    }
    .store_info{
        font-size: 14px!important;
        transition: all ease-in .2s;
        color: #eee!important;
    }
    .store_info:hover {
        color: #fff!important;
    }
</style>
@endsection

@section('content')

<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="sidebar_widget custom">
                    <div class="widget_heading">
                        <p class="title_heading"><i class="fa fa-file-invoice"></i>Formulir Penyewaan</p>
                        <p class="subtitle">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi eius perspiciatis consequatur quidem, aperiam delectus maxime nobis natus iure eveniet? Quos, asperiores libero? Sint iste ea eum perferendis dolore est.</p>
                    </div>
                    <div class="payment_step">
                        <p>Langkah 3 : <span class="italic">Pilih Metode Pembayaran dan Jenis Pengambilan</span></p>
                        {{ Form::open(['route' => ['car.sewa.step_3', $data->kode]]) }}
                        <div class="form-group{{ $errors->has('jenis_pembayaran') ? ' has-error' : null }}">
                            {{ Form::label('jenis_pembayaran', 'Jenis Pembayaran', ['class' => 'form-label']) }}
                            {{ Form::select('jenis_pembayaran', $jenis_pembayaran, null, ['class' => 'form-control', 'placeholder' => 'Pilih Jenis Pembayaran', 'required']) }}
                            @if($errors->has('jenis_pembayaran'))
                            <span class="help-block">{{ $errors->first('jenis_pembayaran') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('jenis_pengambilan') ? ' has-error' : null }}">
                            {{ Form::label('jenis_pengambilan', 'Jenis Pengambilan', ['class' => 'form-label']) }}
                            {{ Form::select('jenis_pengambilan', $jenis_pengambilan, null, ['class' => 'form-control', 'placeholder' => 'Pilih Jenis Pengambilan', 'required']) }}
                            @if($errors->has('jenis_pengambilan'))
                            <span class="help-block">{{ $errors->first('jenis_pengambilan') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ old('jenis_pengambilan') && old('jenis_pengambilan') == "ambil_sendiri" ? null : ' display-hidden' }}" id="pick_address">
                            {{ Form::label('alamat_pengambilan', 'Alamat Pengambilan', ['class' => 'form-label']) }}
                            {{ Form::textarea('alamat_pengambilan', $pick_address, ['class' => 'form-control', 'rows' => 5, 'disabled']) }}
                        </div>
                        <div class="form-group{{ $errors->has('alamat_diantar') ? ' has-error' : (old('jenis_pengambilan') == "diantar" ? null : ' display-hidden') }}" id="delivery_address">
                            {{ Form::label('alamat_diantar', 'Alamat Diantar *', ['class' => 'form-label']) }}
                            {{ Form::textarea('alamat_diantar', null, ['class' => 'form-control', 'rows' => 5]) }}
                            @if($errors->has('alamat_diantar'))
                            <span class="help-block">{{ $errors->first('alamat_diantar') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Checkout', ['class' => 'btn btn-xs', 'type' => 'submit', 'id' => 'checkout']) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>

            <!--Side-Bar-->
            <aside class="col-md-4">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <img src="{{ asset('assets/front/images/kp/' . $data->Foto->first()->nama) }}" alt="Images" class="car_image">
                        <div class="form_review">
                            <span class="car_name">{{ $data->fullNameKendaraan }}</span>
                            <div class="pickup_table">
                                <div class="pickup_row">
                                    <span class="title">Pengambilan</span>
                                    <span class="date">{{ date('d M Y', strtotime($twp_1)) }}</span>
                                    <span class="date">{{ date('H:i', strtotime($twp_1)) }}</span>
                                </div>
                                <div class="pickup_row">
                                    <span class="title">Pengembalian</span>
                                    <span class="date">{{ date('d M Y', strtotime($twp_2)) }}</span>
                                    <span class="date">{{ date('H:i', strtotime($twp_1)) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div id="price-list">
                        <div class="price_table">
                            <div class="price_row">
                                Tarif Harian Regular (per hari)
                                <span class="price" id="price">{{ $data->humanTarif }}<span class="rent_day" id="rent_day"> x {{ $day }} Hari</span></span>
                            </div>
                        </div>
                    </div>
					<div class="divider"></div>
					<div class="total_table">
						<div class="total_row">
							<span class="total_text">Total</span>
						</div>
						<div class="total_row">
							<span class="total_price">Rp. {{ number_format($data->total_harga*$day, 0, ",", ".") }}</span>
						</div>
					</div>
                </div>
            </aside>
            <!--/Side-Bar-->

        </div>

    </div>
</section>
<!--/Listing-detail-->
@endsection

@section('custom-js')
    <script src="{{ asset('assets/front/js/sweetalert.min.js') }}"></script>
	<script>
        $(document).ready(function() {
            var jp = $("jenis_pengambilan").val();
            if(jp != "diantar") $("#alamat_diantar").val(null);
        });
		$("#checkout").on("click", function(e) {
            e.preventDefault();
            var $form = $(this).closest("form");
            swal({
                title: "Lorem Ipsum",
                text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                icon: "info",
                buttons: true,
            })
            .then((willDelete) => {
                if(willDelete) {
				    $form.submit();
                }
            });
        });
		$("#jenis_pengambilan").on("change", function(e) {
			if(this.value === "diantar") {
				$("#delivery_address").slideDown().removeClass("display-hidden");
                $("#pick_address").slideUp().addClass("display-hidden");
				$("#alamat_diantar").attr('required', true);
                $("#price-list").append("<div class=\"price_table\" id=\"biaya_antar\"><div class=\"price_row\">Biaya Antar<span class=\"price\">" + convertToRupiah({{ $data->biaya_antar }}) + "</span></div></div>");
			} else if(this.value == "ambil_sendiri") {
				$("#delivery_address").slideUp().addClass("display-hidden");
                $("#pick_address").slideDown().removeClass("display-hidden");
				$("#alamat_diantar").removeAttr('required');
                $("#biaya_antar").remove();
			} else {
				$("#delivery_address").slideUp().addClass("display-hidden");
                $("#pick_address").slideUp().addClass("display-hidden");
				$("#alamat_diantar").removeAttr('required');
                $("#biaya_antar").remove();
            }
		});
	</script>
@endsection