<!--Login-Form -->
<div class="modal fade" id="loginform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Masuk</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="login_wrap">
                        <div class="col-md-12 col-sm-12">
                            {{ Form::open(['route' => 'login', 'method' => 'post']) }}
                            <form action="#" method="get">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                                    {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Alamat Email Anda', 'required']) }}
                                    @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Kata Sandi Anda', 'required']) }}
                                    @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group checkbox">
                                    {{ Form::checkbox('remember', null, false, ['id' => 'remember']) }}
                                    <label for="remember">Ingatkan Saya</label>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Login', ['class' => 'btn btn-block']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <p>Belum mempunyai akun? <a href="#signupform" data-toggle="modal" data-dismiss="modal">Daftar Sekarang</a></p>
                <p><a href="#forgotpassword" data-toggle="modal" data-dismiss="modal">Lupa Kata Sandi?</a></p>
            </div>
        </div>
    </div>
</div>
<!--/Login-Form -->

<!--Register-Form -->
<div class="modal fade" id="signupform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Daftar Akun</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="signup_wrap">
                        <div class="col-md-12 col-sm-12">
                            <form action="#" method="get">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Kata Sandi">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Konfirmasi Kata Sandi">
                                </div>
                                <div class="form-group checkbox">
                                    <input type="checkbox" id="terms_agree">
                                    <label for="terms_agree">Saya Menyetujui <a href="#">Syarat dan Ketentuan</a></label>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Daftar" class="btn btn-block">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <p>Sudah mempunyai akun? <a href="#loginform" data-toggle="modal" data-dismiss="modal">Masuk Disini</a></p>
            </div>
        </div>
    </div>
</div>
<!--/Register-Form -->

<!--Forgot-password-Form -->
<div class="modal fade" id="forgotpassword">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Pemulihan Kata Sandi</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="forgotpassword_wrap">
                        <div class="col-md-12">
                            <form action="#" method="get">
                                <div class="form-group">
                                    {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Alamat Email Anda']) }}
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Ubah Password Saya" class="btn btn-block">
                                </div>
                            </form>
                            <div class="text-center">
                                <p class="gray_text">Untuk alasan keamanan, kami tidak menyimpan kata sandi Anda.
                                    Kata sandi Anda akan diatur ulang dan akan dikirimkan kata sandi baru.</p>
                                <p><a href="#loginform" data-toggle="modal" data-dismiss="modal"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Kembali Masuk</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/Forgot-password-Form -->
@section('custom-js')
@if($errors->has('email') && $errors->has('password'))
<script>
    $(document).ready(function(){
        $("#myModal").modal();
    });
</script>
@endif
@endsection