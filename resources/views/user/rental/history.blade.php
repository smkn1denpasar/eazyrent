@extends('user.misc.front')

@section('content')
<!--my-vehicles-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">History Penyewaan</h5>     
                    <div class="my_vehicles_list">
                        <ul class="vehicle_listing">
                            <li>
                                <div class="vehicle_img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil17.jpg') }}"
                                            alt="image"></a>
                                </div>
                                <div class="vehicle_title">
                                    <h6><a href="#">Mazda CX-5 SX, V6, ABS, Sunroof </a></h6>
                                    <div>
                                        <span class="rent-code">SW/098282-2214</span>
                                        <span class="seperate-symbol">|</span>
                                        <span class="rent-date">08 Jan 2019</span>
                                    </div>
                                    <span class="rent-store"><a href="#"><i class="fa fa-store-alt" style="margin-right:6px;margin-bottom:12px;"></i>RheznendraStore</a>
                                    </span>  
                                </div>
                                <div class="vehicle_status">
                                    <a href="{{ route('riwayat-transaksi.detail', '1') }}" class="btn btn-xs btn-solid">Detail</a>
                                </div>
                            </li>
                        </ul>
                        <div class="pagination">
                            <ul>
                                <li class="current">1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                        </div>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</section>
<!--/my-vehicles-->
@endsection