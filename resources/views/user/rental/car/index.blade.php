@extends('user.misc.front')

@section('content')
<!--my-vehicles-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Manage Kendaraan <span>({{ $kendaraan->total() }} kendaraan)</span></h5>
                    <a href="{{ route('rental.car.add') }}" class="btn btn-xs outline"><i class="fal fa-plus"></i> Tambah Kendaraan</a>
                    @if($kendaraan->total())
                    <div class="my_vehicles_list">
                        <ul class="vehicle_listing">
                            @foreach($kendaraan as $car)
                            <li @if($car->status == 'digunakan') class="deactive_vehicle" @endif>
                                <div class="vehicle_img">
                                    <img src="{{ asset('assets/front/images/kp/' . $car->Foto->first()->nama) }}" alt="image">
                                </div>
                                <div class="vehicle_title">
                                    <h6>{{ $car->fullNameKendaraan }}</h6>
                                </div>
                                <div class="vehicle_status manage-car">
                                    {{ Form::model($car, ['route' => ['rental.car.delete', $car->kode], 'method' => 'delete']) }}
                                    <a href="{{ route('rental.car.edit', $car->kode) }}" class="btn btn-xs outline btn-block purple"><i class="fa fa-pencil-square-o"></i> Edit Kendaraan</a>
                                    <a href="{{ route('rental.car.foto', $car->kode) }}" class="btn btn-xs outline btn-block purple"><i class="fa fa-images"></i> Edit Foto</a>
                                    {!! Form::button('<i class="fa fa-trash"></i> Delete', ['class' => 'btn btn-xs outline btn-block delete', 'type' => 'submit']) !!}
                                    {{ Form::close() }}
                                    <span class="btn outline btn-xs active-btn btn-block">{{ $car->humanStatus }}</span>
                                    <div class="clearfix"></div>                                    
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="pagination">
                            {{ $kendaraan->links() }}
                        </div>
                    </div>
                    @else
                        <div class="car-not-found text-center">
                            <img src="{{ asset('assets/front/images/car-not-found.png') }}" alt="" style="width:500px">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--/my-vehicles-->
@endsection

@section('custom-js')
    <script src="{{ asset('assets/front/js/sweetalert.min.js') }}"></script>
    <script>
        $(document).on("click", ".delete", function(e) {
            e.preventDefault();
            var $form = $(this).closest("form");
            swal({
                title: "Hapus Kendaraan",
                text: "Apakah anda yakin menhapus kendaraan ini?\nData tidak dapat dikembalikan.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete) {
				    $form.submit();
                }
            });
        });
    </script>
@endsection