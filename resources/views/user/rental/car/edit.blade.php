@extends('user.misc.front')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.css') }}">
@endsection

@section('content')
<!--Post-vehicle-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-6 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Edit Kendaraan</h5>
                    {{ Form::model($car, ['route' => ['rental.car.edit', $car->kode], 'enctype' => 'multipart/form-data', 'method' => 'patch']) }}
                        <div class="form-group{{ $errors->has('jenis') ? ' has-error' : null }}">
                            {{ Form::label('jenis', 'Jenis Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::text('jenis', $car->jenis, ['class' => 'form-control white_bg', 'placeholder' => 'Jenis Kendaran', 'required']) }}
                            @if($errors->has('jenis'))
                                <span class="help-block">{{ $errors->first('jenis') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('kode_merk') ? ' has-error' : null }}">
                            {{ Form::label('kode_merk', 'Merk Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::select('kode_merk', $merk, $car->kode_merk, ['class' => 'form-control select2', 'required']) }}
                            @if($errors->has('kode_merk'))
                                <span class="help-block">{{ $errors->first('kode_merk') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : null }}">
                            {{ Form::label('deskripsi', 'Deskripsi Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::textarea('deskripsi', $car->deskripsi, ['class' => 'form-control white_bg', 'rows' => 4, 'placeholder' => 'Deskripsi Kendaraan', 'required']) }}
                            @if($errors->has('deskripsi'))
                                <span class="help-block">{{ $errors->first('deskripsi') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('harga') ? ' has-error' : null }}">
                            {{ Form::label('harga', 'Harga Sewa Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::number('harga', $car->harga, ['class' => 'form-control white_bg', 'placeholder' => 'Harga Sewa Kendaran', 'required']) }}
                            @if($errors->has('harga'))
                                <span class="help-block">{{ $errors->first('harga') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('diskon') ? ' has-error' : null }}">
                            {{ Form::label('diskon', 'Diskon Kendaraan (%)', ['class' => 'control-label']) }}
                            {{ Form::text('diskon', $car->diskon, ['class' => 'form-control white_bg', 'placeholder' => 'Diskon Kendaran']) }}
                            @if($errors->has('diskon'))
                                <span class="help-block">{{ $errors->first('diskon') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : null }}">
                            {{ Form::label('status', 'Status Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::select('status', $status, $car->status, ['class' => 'form-control white_bg', 'required']) }}
                            @if($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                        <div class="gray-bg field-title">
                            <h6>Basic Info</h6>
                        </div>
                        <div class="form-group{{ $errors->has('tahun') ? ' has-error' : null }}">
                            {{ Form::label('tahun', 'Tahun Kendaraan', ['class' => 'control-label']) }}
                            {{ Form::number('tahun', $car->tahun, ['class' => 'form-control white_bg', 'placeholder' => 'Tahun Kendaran', 'required']) }}
                            @if($errors->has('tahun'))
                                <span class="help-block">{{ $errors->first('tahun') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('total_km') ? ' has-error' : null }}">
                            {{ Form::label('total_km', 'Total KM', ['class' => 'control-label']) }}
                            {{ Form::number('total_km', $car->total_km, ['class' => 'form-control white_bg', 'placeholder' => 'Total KM', 'required']) }}
                            @if($errors->has('total_km'))
                                <span class="help-block">{{ $errors->first('total_km') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('bahan_bakar') ? ' has-error' : null }}">
                            {{ Form::label('bahan_bakar', 'Jenis Bahan Bakar', ['class' => 'control-label']) }}
                            {{ Form::select('bahan_bakar', $bahan_bakar, $car->bahan_bakar, ['class' => 'form-control white_bg', 'required']) }}
                            @if($errors->has('bahan_bakar'))
                                <span class="help-block">{{ $errors->first('bahan_bakar') }}</span>
                            @endif
                        </div>
                        <div class="gray-bg field-title">
                            <h6>Technical Specification</h6>
                        </div>
                        <div class="form-group{{ $errors->has('max_fuel') ? ' has-error' : null }}">
                            {{ Form::label('max_fuel', 'Kapasitas Tangki Bahan Bakar', ['class' => 'control-label']) }}
                            {{ Form::number('max_fuel', $car->max_fuel, ['class' => 'form-control white_bg', 'placeholder' => 'Kapasitas Tangki Bahan Bakar', 'required']) }}
                            @if($errors->has('max_fuel'))
                                <span class="help-block">{{ $errors->first('max_fuel') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('seat') ? ' has-error' : null }}">
                            {{ Form::label('seat', 'Jumlah Tempat Duduk', ['class' => 'control-label']) }}
                            {{ Form::number('seat', $car->seat, ['class' => 'form-control white_bg', 'placeholder' => 'Jumlah Tempat Duduk', 'required']) }}
                            @if($errors->has('seat'))
                                <span class="help-block">{{ $errors->first('seat') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('transmisi') ? ' has-error' : null }}">
                            {{ Form::label('transmisi', 'Jenis Transmisi', ['class' => 'control-label']) }}
                            {{ Form::select('transmisi', $transmisi, $car->transmisi, ['class' => 'form-control white_bg', 'required']) }}
                            @if($errors->has('transmisi'))
                                <span class="help-block">{{ $errors->first('transmisi') }}</span>
                            @endif
                        </div>
                        {{-- <div class="gray-bg field-title">
                            <h6>Accessories</h6>
                        </div>
                        <div class="vehicle_accessories">
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="air_conditioner" type="checkbox">
                                <label for="air_conditioner">Air Conditioner</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="door" type="checkbox">
                                <label for="door">Power Door Locks</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="antiLock" type="checkbox">
                                <label for="antiLock">AntiLock Braking System</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="brake" type="checkbox">
                                <label for="brake">Brake Assist</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="steering" type="checkbox">
                                <label for="steering">Power Steering</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="airbag" type="checkbox">
                                <label for="airbag">Driver Airbag</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="windows" type="checkbox">
                                <label for="windows">Power Windows</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="passenger_airbag" type="checkbox">
                                <label for="passenger_airbag">Passenger Airbag</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="player" type="checkbox">
                                <label for="player">CD Player</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="sensor" type="checkbox">
                                <label for="sensor">Crash Sensor</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="seats" type="checkbox">
                                <label for="seats">Leather Seats</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="engine_warning" type="checkbox">
                                <label for="engine_warning">Engine Check Warning</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="locking" type="checkbox">
                                <label for="locking">Central Locking</label>
                            </div>
                            <div class="form-group checkbox col-md-6 accessories_list">
                                <input id="headlamps" type="checkbox">
                                <label for="headlamps">Automatic Headlamps</label>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <button type="submit" class="btn">Submit Vehicle <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Post-vehicle-->
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $(".select2-container--default").addClass("white_input");
    $("#jenis").autocomplete({
        source: "/admin/search/jenis-kendaraan",
        minLength: 3,
    });
</script>
@endsection