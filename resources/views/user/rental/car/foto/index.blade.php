@extends('user.misc.front')

@section('content')
<!--my-vehicles-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Manage Foto Kendaraan <span>({{ $foto->total() }} foto)</span></h5>
                    <a href="{{ route('rental.car.foto.add', $foto->first()->kode_kendaraan) }}" class="btn btn-xs outline"><i class="fal fa-plus"></i> Tambah Foto</a>
                    <div class="my_vehicles_list">
                        <ul class="vehicle_listing">
                            @foreach($foto as $photo)
                            <li>
                                <div class="vehicle_img">
                                    <img src="{{ asset('assets/front/images/kp/' . $photo->nama) }}" alt="image">
                                </div>
                                <div class="vehicle_status">
                                    {{ Form::model($photo, ['route' => ['rental.car.foto.delete', $photo->kode_kendaraan, $photo->kode], 'method' => 'delete']) }}
                                    {!! Form::button('<i class="fa fa-trash"></i> Delete', ['class' => 'btn btn-xs outline btn-inline delete', 'type' => 'submit']) !!}
                                    {{ Form::close() }}
                                    <div class="clearfix"></div>                                    
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="pagination">
                            {{ $foto->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/my-vehicles-->
@endsection

@section('custom-js')
    <script src="{{ asset('assets/front/js/sweetalert.min.js') }}"></script>
    <script>
        $(document).on("click", ".delete", function(e) {
            e.preventDefault();
            var $form = $(this).closest("form");
            swal({
                title: "Hapus Kendaraan",
                text: "Apakah anda yakin menhapus kendaraan ini?\nData tidak dapat dikembalikan.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete) {
				    $form.submit();
                }
            });
        });
    </script>
@endsection