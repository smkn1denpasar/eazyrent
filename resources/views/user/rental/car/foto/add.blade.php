@extends('user.misc.front')

@section('content')
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Add Foto Kendaraan</h5>
                    {{ Form::open(['route' => ['rental.car.foto.add', $car->kode], 'enctype' => 'multipart/form-data']) }}
                        <div class="form-group{{ $errors->has('foto') || $errors->has('foto.*') ? ' has-error' : null }}">
                            {{ Form::label('harga', 'Upload Foto', ['class' => 'control-label']) }}
                            <div class="vehicle_images">
                                <div class="upload_more_img">
                                    {{ Form::file('foto[]', ['accept' => 'image/*', 'multiple', 'id' => 'foto', 'required']) }}
                                </div>
                            </div>
                            @if($errors->has('foto') || $errors->has('foto.*'))
                                <span class="help-block">{{ $errors->first('foto') ?: $errors->first('foto.*') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn">Submit Vehicle <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-js')
<script>
    $("#foto").change(function(e) {
        $(".image-preview").remove();
        if (window.File && window.FileList && window.FileReader) {
            if (this.files) {
                for(var i=0; i<this.files.length;i++) {
                    var file = this.files[i];
                    if (!file.type.match('image')) continue;
                    var reader = new FileReader();
                    reader.addEventListener("load", function(event) {
                        var picture = '<div class="image-preview"><img src="' + event.target.result + '"></div>';
                        $(".upload_more_img").parent().append(picture);
                    });
                    reader.readAsDataURL(file);
                }
            }
        } else {
            console.log("Your browser does not support File API");
        }
    });
</script>
@endsection