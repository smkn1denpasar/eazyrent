@extends('user.misc.front')

@section('content')
<section class="section-padding gray-bg" style="padding:50px 0">
    <div class="container">
        <div class="row">     
            <div class="col-md-6 col-md-offset-1 custom">
                <div class="rental-register-image">
                    <img src="{{ asset('assets/front/images/rental/register-rental.png') }}" alt="">
                    <div class="text-logo">
                        <h2 class="rental-register-title">Buka Rental Mobil Mudah Hanya di <span class="color-blue">EazyRent</span></h2>
                        <p>Gabung dan rasakan kemudahan bertransaksi di EazyRent</p>     
                    </div>   
                </div>    
            </div>                                   
            <div class="col-md-5">
                <div class="user-account-box rental-account-box">
                    <div class="user-account-header rental-box-header">
                        <div class="text-center">
                            <h3><i class="fa fa-store-alt"></i></h3>
                            <h2 class="user-account-title">Buka Rental</h2>
                        </div>
                    </div>
                    <div class="user-account-body">
                        {{ Form::open(['route' => 'rental.register']) }}
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                                {{ Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Rental']) }}
                                @if($errors->has('nama'))
                                <span class="help-block">{{ $errors->first('nama') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : null }}">
                                {{ Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username Rental']) }}
                                @if($errors->has('username'))
                                <span class="help-block">{{ $errors->first('username') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                                {{ Form::number('no_telp', null, ['class' => 'form-control', 'placeholder' => 'Nomor Telepon Rental']) }}
                                @if($errors->has('no_telp'))
                                <span class="help-block">{{ $errors->first('no_telp') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null  }}">
                                {{ Form::textarea('alamat', null, ['class' => 'form-control', 'placeholder' => 'Alamat Rental', 'rows' => 4]) }}
                                @if ($errors->has('alamat'))
                                    <span class="help-block">{{ $errors->first('alamat') }}</span>
                                @endif
                            </div>                                
                            <div class="form-group {{ $errors->has('kode_lokasi') ? ' has-error' : null }}">
                                {{ Form::select('kode_lokasi', $wlyh, null, ['class' => 'form-control select2', 'placeholder' => 'Silahkan Pilih Wilayah']) }}
                                @if ($errors->has('kode_lokasi'))
                                    <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                                @endif
                            </div>
                            <div class="form-group checkbox{{ $errors->has('terms_agree') ? ' has-error' : null }}">
                                {{ Form::checkbox('terms_agree', null, false, ['id' => 'terms_agree']) }}
                                <label for="terms_agree">Saya Menyetujui <a href="#">Syarat dan Ketentuan</a></label>
                                @if($errors->has('terms_agree'))
                                <span class="help-block">{{ $errors->first('terms_agree') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Buka Rental', ['class' => 'btn btn-block', 'type' => 'submit']) }}
                            </div>                                
                        {{ Form::close() }}
                    </div>
                </div>
            </div>                
        </div>
    </div>
</section>
@endsection

@section('custom-js')
    <script>
        $(".select2-container--default").addClass('grey_input');
    </script>
@endsection