@extends('user.misc.front')

@section('content')
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-6 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Detail Penyewaan</h5>
                    <div class="invoice-box">
                        <div class="general-invoice col-md-12">
                            <h4>Tinjauan Umum</h4>
                            <div class="col-md-6">
                                <div class="general-invoice__group">
                                    <p>Nomor Penyewaan</p>
                                    <p>#{{ $data->kode }}</p>
                                </div>
                                <div class="general-invoice__group">
                                    <p>Status</p>
                                    <p>{{ $data->realStatus }}</p>
                                </div>
                                <div class="general-invoice__group">
                                    <p>Nama Rental</p>
                                    <p>
                                        <a href="{{ route('rental', $data->Kendaraan->Rental->username) }}">{{ $data->Kendaraan->Rental->nama }}</a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="general-invoice__group">
                                    <p>Tanggal Transaksi</p>
                                    <p>{{ date('d M Y H:i', strtotime($data->tanggal_transaksi)) }}</p>
                                </div>
                                <div class="general-invoice__group">
                                    <p>Tanggal Mulai Sewa</p>
                                    <p>{{ date('d M Y', strtotime($data->tanggal_waktu_pengambilan)) }}</p>
                                </div>
                                <div class="general-invoice__group">
                                    <p>Tanggal Selesai Sewa</p>
                                    <p>{{ date('d M Y', strtotime($data->tanggal_waktu_pengembalian)) }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="personal-info col-md-12">
                            <h4>Data Diri</h4>
                            <p class="data-personal">Nama : {{ $data->Data->nama }}</p>
                            <p class="data-personal">Telp : {{ $data->Data->no_telp }}</p>
                            <p class="data-personal">Email : {{ $data->Data->email }}</p>
                            <p class="data-personal">{{ nl2br($data->Data->alamat) }}</p>
                        </div>
                        <div class="product-invoice col-md-12">
                            <h4>Info Penyewaan</h4>
                            <div class="product-invoice__body">
                                <div class="general-invoice__group">
                                    <div class="product-item-info">
                                        <p>Jenis Pembayaran</p>
                                        <p>COD</p>
                                    </div>
                                </div>
                            </div>
                            <div class="product-invoice__body">
                                <div class="general-invoice__group">
                                    <div class="product-item-info">
                                        <p>Total KM</p>
                                        <p>{{ $data->total_km }} KM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="product-invoice__body">
                                <div class="product-invoice__group">
                                    <div class="product-item-info">
                                        <p>Info Kendaraan</p>
                                        <a class="car-name" href="{{ route('car.detail', $data->kode_kendaraan) }}" target="_blank">{{ $data->Kendaraan->fullNameKendaraan }}</a>
                                        <p>{{ $data->durasi }} Hari x {{ $data->Kendaraan->humanTarif }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-total">
                            <div class="total-text">
                                <p>TOTAL</p> <span>{{ $data->rpTotal }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-32 button-back">
                        <a href="{{ route('rental.history') }}" class="btn btn-md"><i class="fa fa-chevron-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="profile_wrap">
                    <div class="form-group" style="margin-top:20px;">
                        {{ Form::label('deskripsi_kerusakan', 'Deskripsi Kerusakan', ['class' => 'control-label']) }}
                        {{ Form::textarea('deskripsi_kerusakan', $data->LaporanKerusakan->deskripsi_kerusakan, ['class' => 'form-control white_bg', 'disabled']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection