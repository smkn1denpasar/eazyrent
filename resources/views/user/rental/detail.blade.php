@extends('user.misc.front')

@section('custom-style')
<style>
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }

    .grid_listing .product-listing-img img {
        height: 185px;
    }
    </style>
@endsection

@section('content')
<!--Page Header-->
<section class="page-header profile_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <h1>RheznendraRentCar</h1>
            </div>
            <ul class="coustom-breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li>Profil Rental</li>
            </ul>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header-->

<!--Dealer-profile-->
<section class="dealer_profile inner_pages">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="dealer_logo"> <img src="{{ asset('assets/front/images/rental2.jpg') }}" alt="image"> </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-8">
                <div class="dealer_info">
                    <h4>RheznendraRentCar </h4>
                    <p>Jalan Kebo Iwa Utara III A, Perumahan Tegal Sari A2 <br>
                        Denpasar, Bali</p>
                    <p class="slogan">Online Shopping Mall Terkemuka di Indonesia</p>
                    <ul class="dealer_social_links">
                        <li class="facebook-icon"><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li class="twitter-icon"><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li class="linkedin-icon"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                        <li class="google-plus-icon"><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="dealer_contact_info gray-bg">
                    <h6><i class="fa fa-globe" aria-hidden="true"></i> Website</h6>
                    <a href="#">www.example.com</a>
                </div>
                <div class="dealer_contact_info gray-bg">
                    <h6><i class="fa fa-envelope" aria-hidden="true"></i> Alamat Email</h6>
                    <a href="mailto:contact@example.com">contact@example.com</a>
                </div>
                <div class="dealer_contact_info gray-bg">
                    <h6><i class="fa fa-phone" aria-hidden="true"></i> Nomor Telepon</h6>
                    <a href="tel:61-1234-5678-09">+61-1234-5678-09</a>
                </div>
            </div>
        </div>
        <div class="space-60"></div>
        <div class="row">
            <div class="col-md-9">
                <div class="dealer_more_info">
                    <h5 class="gray-bg info_title"> Tentang RheznendraRentCar</h5>
                    <p class="bio">RheznendraRentCar merupakan sebuah toko rental yang bergerak di bidang Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo veniam harum, neque ipsum placeat maiores dolor impedit alias tempora nulla dolorum magnam ad odit molestiae eos nihil quidem sunt quas. Tempore molestiae non pariatur odit rerum a, itaque voluptatem qui? Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ratione quaerat quos eligendi, adipisci nobis blanditiis nihil cupiditate quisquam neque quidem! </p>
                    <div class="comment_form">
                        <h6>Tinggalkan Komentar</h6>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Alamat Email">
                            </div>
                            <div class="form-group">
                                <textarea rows="5" class="form-control" placeholder="Tulis Komentar"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn" value="Kirim">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <aside class="col-md-3">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fa fa-envelope" aria-hidden="true"></i> Kirim Pesan</h5>
                    </div>
                    <form action="#">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea rows="4" class="form-control" placeholder="Pesan"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Kirim" class="btn btn-block">
                        </div>
                    </form>
                </div>
            </aside>
            <div class="col-md-12">
                <!--Similar-Cars-->
                <div class="similar_cars">
                    <h3>List Kendaraan</h3>
                    <div class="row">
                        <div id="rental_car">
                            <div class="grid_listing custom carousel-grid">
                                <div class="product-listing-m gray-bg">
                                    <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil18.jpg') }}"
                                                class="img-responsive" alt="image" /> </a>
                                        <div class="promo_tag deal">
                                            <label class="promo_label deal">Great Deal</label>
                                            <label class="promo_label popular">Most Popular</label>
                                            <label class="promo_label new">New</label>
                                        </div>
                                        <div class="rental_info">
                                            <a href="#" class="store_info"> <i class="fa fa-store-alt"></i>
                                                RheznendraRentCar </a>
                                        </div>
                                    </div>
                                    <div class="product-listing-content">
                                        <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                                        <div class="box-price">
                                            <span class="old_price">Rp. 200.000</span>
                                            <span class="price">Rp 150.000</span>
                                        </div>
                                        <ul class="features_list">
                                            <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                            <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                            <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                            <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                                        </ul>
                                    </div>
                                    <div class="read_btn">
                                        <a href="" class="btn btn-block">Info Lanjut</a>
                                    </div>
                                </div>
                            </div>
                            <div class="grid_listing custom carousel-grid">
                                <div class="product-listing-m gray-bg">
                                    <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil17.jpg') }}"
                                                class="img-responsive" alt="image" /> </a>
                                        <div class="promo_tag deal">
                                            <label class="promo_label deal">Great Deal</label>
                                            <label class="promo_label popular">Most Popular</label>
                                            <label class="promo_label new">New</label>
                                        </div>
                                        <div class="rental_info">
                                            <a href="#" class="store_info"> <i class="fa fa-store-alt"></i>
                                                RheznendraRentCar </a>
                                        </div>
                                    </div>
                                    <div class="product-listing-content">
                                        <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                                        <div class="box-price">
                                            <span class="old_price">Rp. 200.000</span>
                                            <span class="price">Rp 150.000</span>
                                        </div>
                                        <ul class="features_list">
                                            <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                            <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                            <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                            <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                                        </ul>
                                    </div>
                                    <div class="read_btn">
                                        <a href="" class="btn btn-block">Info Lanjut</a>
                                    </div>
                                </div>
                            </div>
                            <div class="grid_listing custom carousel-grid">
                                <div class="product-listing-m gray-bg">
                                    <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil20.jpg') }}"
                                                class="img-responsive" alt="image" /> </a>
                                        <div class="promo_tag deal">
                                            <label class="promo_label deal">Great Deal</label>
                                            <label class="promo_label popular">Most Popular</label>
                                            <label class="promo_label new">New</label>
                                        </div>
                                        <div class="rental_info">
                                            <a href="#" class="store_info"> <i class="fa fa-store-alt"></i>
                                                RheznendraRentCar </a>
                                        </div>
                                    </div>
                                    <div class="product-listing-content">
                                        <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                                        <div class="box-price">
                                            <span class="old_price">Rp. 200.000</span>
                                            <span class="price">Rp 150.000</span>
                                        </div>
                                        <ul class="features_list">
                                            <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                            <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                            <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                            <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                                        </ul>
                                    </div>
                                    <div class="read_btn">
                                        <a href="" class="btn btn-block">Info Lanjut</a>
                                    </div>
                                </div>
                            </div>
                            <div class="grid_listing custom carousel-grid">
                                <div class="product-listing-m gray-bg">
                                    <div class="product-listing-img"> <a href="#"><img src="{{ asset('assets/front/images/kp/mobil19.jpg') }}"
                                                class="img-responsive" alt="image" /> </a>
                                        <div class="promo_tag deal">
                                            <label class="promo_label deal">Great Deal</label>
                                            <label class="promo_label popular">Most Popular</label>
                                            <label class="promo_label new">New</label>
                                        </div>
                                        <div class="rental_info">
                                            <a href="#" class="store_info"> <i class="fa fa-store-alt"></i>
                                                RheznendraRentCar </a>
                                        </div>
                                    </div>
                                    <div class="product-listing-content">
                                        <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                                        <div class="box-price">
                                            <span class="old_price">Rp. 200.000</span>
                                            <span class="price">Rp 150.000</span>
                                        </div>
                                        <ul class="features_list">
                                            <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                            <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                            <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                            <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                                        </ul>
                                    </div>
                                    <div class="read_btn">
                                        <a href="" class="btn btn-block">Info Lanjut</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/Similar-Cars-->
            </div>
        </div>
    </div>
</section>
<!--/Dealer-profile-->
@endsection
