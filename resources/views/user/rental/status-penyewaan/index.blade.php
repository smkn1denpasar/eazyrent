@extends('user.misc.front')

@section('content')
<!--my-vehicles-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-9 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Manage Status Penyewaan <span>({{ $data->total() }} transaksi)</span></h5>
                    @if($data->total())
                    <div class="my_vehicles_list">
                        <ul class="vehicle_listing">
                            @foreach($data as $val)
                            <li>
                                <div class="vehicle_img">
                                    <img src="{{ asset('assets/front/images/kp/' . $val->Kendaraan->Foto->first()->nama) }}" alt="image">
                                </div>
                                <div class="vehicle_title">
                                    <h6>{{ $val->Kendaraan->fullNameKendaraan }}</h6>
                                    <div>
                                        <span class="rent-code">#{{ $val->kode }}</span>
                                        <span class="seperate-symbol">|</span>
                                        <span class="rent-date">{{ $val->tanggal_transaksi }}</span>
                                    </div>
                                </div>
                                <div class="vehicle_status manage-car">
                                    <a href="{{ route('rental.status-penyewaan.edit', $val->kode) }}" class="btn btn-xs outline btn-block purple">
                                        <i class="fa fa-pencil-square-o"></i> Ubah Status
                                    </a>
                                    <span class="btn outline btn-xs active-btn btn-block">{{ $val->realStatus }}</span>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="pagination">
                            {{ $data->links() }}
                        </div>
                    </div>
                    @else
                        <div class="car-not-found text-center">
                            <img src="{{ asset('assets/front/images/order-history-not-found.png') }}" alt="">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--/my-vehicles-->
@endsection