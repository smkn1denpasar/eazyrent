@extends('user.misc.front')

@section('content')
<section class="section-padding gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="user-account-box rental-account-box success-box">
                    <div class="user-account-header rental-box-header">
                        <div class="text-center">
                            <h2><i class="fa fa-store-alt"></i></h2>
                            <h2 class="user-account-title">Rental AutoSpotRentCar</h2>                            
                        </div>
                    </div>
                    <div class="user-account-body">                        
                        <div class="form-group">
                            <div class="body-text">
                                    <p class="title">Selamat! Rental Anda Sudah Aktif.</p>
                                    <p class="subtitle">Jangan sampai rental Anda kosong begitu saja. Tambahkan kendaraan pertamamu dan mulai sewakan sekarang.</p>
                            </div>
                            <div class="btn-footer">
                                <a href="#" class="btn btn-block">Tambahkan Produk Kendaraan</a>
                                <a href="#" class="btn btn-block outline hover-outline">Ke Halaman Rental Saya</a>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
