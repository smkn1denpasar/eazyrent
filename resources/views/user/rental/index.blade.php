@extends('user.misc.front')

@section('custom-style')
<style>
    .rental_info {
      padding: 10px!important;
    }

    .store_info{
      font-size: 14px!important;
      transition: all ease-in .2s;
      color: #eee!important;
    }

    .store_info:hover {
      color: #fff!important;
    }

    .grid_listing .product-listing-img img {
        height: 185px;
    }
    </style>
@endsection

@section('content')
<!--Dealer-profile-->
<section class="dealer_profile inner_pages">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="dealer_logo">
                    @if(!$data->logo)
                    <img src="{{ asset('assets/front/images/rental.png') }}" alt="image" width="200"/>
                    @else
                    <img src="{{ asset('assets/front/images/rental/logo/' . $data->logo) }}" alt="image" width="200"/>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-8">
                <div class="dealer_info">
                    <h4>{{ $data->nama }}</h4>
                    <p>{!! nl2br($data->alamat) !!}</p>
                    @if($data->slogan)
                    <p class="slogan">{{ $data->slogan }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                @if($owner)
                <div class="dealer_contact_info gray-bg">
                    <h6><a href="{{ route('rental.profile') }}"><i class="fa fa-cogs" aria-hidden="true"></i> Settings</a></h6>
                </div>
                @endif
                @if($data->email)
                <div class="dealer_contact_info gray-bg">
                    <h6><i class="fa fa-envelope" aria-hidden="true"></i> Alamat Email</h6>
                    <a href="mailto:{{ $data->email }}">{{ $data->email }}</a>
                </div>
                @endif
                @if($data->no_telp)
                <div class="dealer_contact_info gray-bg">
                    <h6><i class="fa fa-phone" aria-hidden="true"></i> Nomor Telepon</h6>
                    <a href="tel:{{ $data->no_telp }}">{{ $data->no_telp }}</a>
                </div>
                @endif
            </div>
        </div>
        <div class="space-60"></div>
        <div class="row">
            @if($data->deskripsi)
            <div class="col-md-9">
                <div class="dealer_more_info">
                    <h5 class="gray-bg info_title"> Sekilas Tentang {{ $data->nama }}</h5>
                    <p class="bio">{{ $data->deskripsi }}</p>
                </div>
            </div>
            @endif
            @if($data->Kendaraan->count() > 4)
            <div class="col-md-12">
                <!--Similar-Cars-->
                <div class="similar_cars">
                    <h4>List Kendaraan</h4>
                    <div class="row">
                        <div id="rental_car">
                            @foreach($data->Kendaraan as $car)
                            <div class="grid_listing custom carousel-grid">
                                <div class="product-listing-m gray-bg">
                                    <div class="product-listing-img">
                                        <a href="#">
                                            <img src="{{ asset('assets/front/images/kp/mobil18.jpg') }}" class="img-responsive" alt="image" />
                                        </a>
                                        <div class="promo_tag deal">
                                            <label class="promo_label deal">Great Deal</label>
                                            <label class="promo_label popular">Most Popular</label>
                                            <label class="promo_label new">New</label>
                                        </div>
                                        <div class="rental_info">
                                            <a href="#" class="store_info"><i class="fa fa-store-alt"></i> RheznendraRentCar </a>
                                        </div>
                                    </div>
                                    <div class="product-listing-content">
                                        <h5><a href="#">Maserati QUATTROPORTE 1,6</a></h5>
                                        <div class="box-price">
                                            <span class="old_price">Rp. 200.000</span>
                                            <span class="price">Rp 150.000</span>
                                        </div>
                                        <ul class="features_list">
                                            <li><i class="fa fa-map-marker-alt"></i>Tabanan</li>
                                            <li><i class="fa fa-road" aria-hidden="true"></i>35,000 km</li>
                                            <li><i class="fa fa-tachometer" aria-hidden="true"></i>30.000 miles</li>
                                            <li><i class="fa fa-car" aria-hidden="true"></i>Diesel</li>
                                        </ul>
                                    </div>
                                    <div class="read_btn">
                                        <a href="" class="btn btn-block">Info Lanjut</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--/Similar-Cars-->
            </div>
            @endif
        </div>
    </div>
</section>
<!--/Dealer-profile-->
@endsection
