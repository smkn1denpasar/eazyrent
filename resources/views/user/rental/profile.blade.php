@extends('user.misc.front')

@section('content')
<!--Profile-setting-->
<section class="user_profile inner_pages">
    <div class="container">
        <div class="row">
            @include('user.misc.menu-rental')
            <div class="col-md-6 col-sm-8">
                @include('user.misc.alert_')
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Pengaturan Umum</h5>
                    {{ Form::open(['route' => 'rental.profile']) }}
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                            {{ Form::label('nama', 'Nama Rental', ['class' => 'control-label']) }}
                            {{ Form::text('nama', $data->DetailRental->nama, ['class' => 'form-control white_bg', 'required']) }}
                            @if($errors->has('nama'))
                                <span class="help-block">{{ $errors->first('nama') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('slogan') ? ' has-error' : null }}">
                            {{ Form::label('slogan', 'Slogan Rental', ['class' => 'control-label']) }}
                            {{ Form::text('slogan', $data->DetailRental->slogan, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('slogan'))
                                <span class="help-block">{{ $errors->first('slogan') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : null }}">
                            {{ Form::label('username', 'Username Rental', ['class' => 'control-label']) }}
                            {{ Form::text('username', $data->DetailRental->username, ['class' => 'form-control white_bg', 'required']) }}
                            @if($errors->has('username'))
                                <span class="help-block">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                            {{ Form::label('email', 'Email Rental (Opsional)', ['class' => 'control-label']) }}
                            {{ Form::email('email', $data->DetailRental->email, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                            {{ Form::label('no_telp', 'Nomor Telepon Rental', ['class' => 'control-label']) }}
                            {{ Form::number('no_telp', $data->DetailRental->no_telp, ['class' => 'form-control white_bg']) }}
                            @if($errors->has('no_telp'))
                                <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                            {{ Form::label('alamat', 'Alamat Rental', ['class' => 'control-label']) }}
                            {{ Form::textarea('alamat', $data->DetailRental->alamat, ['class' => 'form-control white_bg', 'required', 'rows' => 4]) }}
                            @if($errors->has('alamat'))
                                <span class="help-block">{{ $errors->first('alamat') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('kode_lokasi') ? ' has-error' : null }}">
                            {{ Form::label('kode_lokasi', 'Lokasi Rental', ['class' => 'control-label']) }}
                            {{ Form::select('kode_lokasi', $wlyh, $data->DetailRental->kode_lokasi, ['class' => 'form-control white_bg select2', 'required', 'rows' => 4]) }}
                            @if($errors->has('kode_lokasi'))
                                <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : null }}">
                            {{ Form::label('deskripsi', 'Deskripsi Rental', ['class' => 'control-label']) }}
                            {{ Form::textarea('deskripsi', $data->DetailRental->deskripsi, ['class' => 'form-control white_bg', 'rows' => 5]) }}
                            @if($errors->has('deskripsi'))
                                <span class="help-block">{{ $errors->first('deskripsi') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn">Simpan Perubahan <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Profile-setting-->
@endsection

@section('custom-js')
<script>
    $(".select2-container--default").addClass("white_input");
</script>
@endsection