@extends('admin.misc.layout_')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
		<li>
            <a href="{{ route('master-data.social-media') }}"><i class="fa fa-user-tie"></i>Social Media</a>
        </li>
		<li class="active">
            <a href="{{ route('master-data.social-media.edit', $data->kode) }}">Edit</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Social Media</h3>
                </div>
                {{ Form::model($data, ['route' => ['master-data.social-media.edit', $data->kode], 'class' => 'form-horizontal', 'method' => 'patch']) }}
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('type', 'Type', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::select('type', $type, null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => '--- Please Select ---']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('link', 'Link', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::url('link', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection