@extends('admin.misc.layout_')
@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li>
			<a href="{{ route('master-data.sopir') }}"><i class="fa fa-user"></i> Sopir</a>
		</li>
		<li class="active">
			<a href="{{ Request::url() }}"> Edit</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			@include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Sopir</h3>
				</div>
                {{ Form::model($data, ['route' => ['master-data.sopir.edit', $data->id], 'class' => 'form-horizontal', 'method' => 'patch']) }}
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : null }}">
                            {{ Form::label('no_ktp', 'No KTP', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('no_ktp', null, ['class' => 'form-control', 'readonly']) }}
                                @if($errors->has('no_ktp'))
                                    <span class="help-block">{{ $errors->first('no_ktp') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('no_sim') ? ' has-error' : null }}">
                            {{ Form::label('no_sim', 'No SIM', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('no_sim', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('no_sim'))
                                    <span class="help-block">{{ $errors->first('no_sim') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                            {{ Form::label('nama', 'Nama', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('nama', null, ['class' => 'form-control', 'readonly', 'autocomplete' => 'off']) }}
                                @if($errors->has('nama'))
                                    <span class="help-block">{{ $errors->first('nama') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                            {{ Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::textarea('alamat', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('alamat'))
                                    <span class="help-block">{{ $errors->first('alamat') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('no_tlp') ? ' has-error' : null }}">
                            {{ Form::label('no_tlp', 'No Telepon', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('no_tlp', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('no_tlp'))
                                    <span class="help-block">{{ $errors->first('no_tlp') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : null }}">
                            {{ Form::label('status', 'Status', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::select('status', [1 => 'Tersedia', 2 => 'Tidak Tersedia'], null, ['class' => 'form-control', 'required']) }}
                                @if($errors->has('status'))
                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Submit</button>
						<button type="reset" class="btn btn-danger">Reset</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</section>
@endsection