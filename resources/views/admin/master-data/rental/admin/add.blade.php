@extends('admin.misc.layout_')
@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
		<li>
			<a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
        </li>
		<li>
			{{ $data->Detail->nama }}
        </li>
        <li>
            <a href="{{ route('master-data.rental.admin', $data->kode) }}"><i class="fa fa-users"></i>Admin</a>
        </li>
        <li class="active">
            <a href="{{ route('master-data.rental.admin.add', $data->kode) }}">Add</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Admin Rental {{ $data->Detail->nama }}</h3>
                </div>
                {{ Form::open(['route' => ['master-data.rental.admin.add', $data->kode], 'class' => 'form-horizontal form-bordered', 'role' => 'form']) }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('kode_user') ? ' has-error' : null }}">
                        {{ Form::label('kode_user', 'No KTP / Nama', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::select('kode_user', $user, null, ['class' => 'form-control select2', 'required', 'autocomplete' => 'off', 'placeholder' => '--- Please Select ---']) }}
                            @if($errors->has('kode_user'))
                            <span class="help-block">{{ $errors->first('kode_user') }}</span>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="form-group{{ $errors->has('kode_rental') ? ' has-error' : null }}">
                        {{ Form::label('kode_rental', 'Rental', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::select('kode_rental', $rental, null, ['class' => 'form-control select2', 'required', 'autocomplete' => 'off', 'placeholder' => '--- Please Select ---']) }}
                            @if($errors->has('kode_rental'))
                            <span class="help-block">{{ $errors->first('kode_rental') }}</span>
                            @endif
                        </div>
                    </div> --}}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-js')
    <script src="{{ asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(".select2").select2();
    </script>
@endsection
