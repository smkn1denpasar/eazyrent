@extends('admin.misc.layout_')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li class="active">
			<a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
            @include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">List Rental</h3>
				</div>
				<div class="box-body">
                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a href="{{ route('master-data.rental.add') }}">
                                    {!! Form::button('Add New <i class="fa fa-plus"></i>', ['class' => 'btn btn-sm btn-info']) !!}
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered order-column" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center" width="2.5%">No</th>
                                <th class="text-center">Rental</th>
                                <th class="text-center">Lokasi</th>
                                <th class="text-center">Menu</th>
                            </tr>
                        </thead>
                        <tbody>
@foreach ($data as $value)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}.</td>
                                <td class="text-center">{{ $value->nama }}</td>
                                <td class="text-center">{{ $value->RentalUtama->Lokasi->nama }}</td>
                                <td class="text-center">
                                    {{ Form::model($value, ['route' => ['master-data.rental.delete', $value->kode], 'method' => 'delete']) }}
                                        <a href="{{ route('master-data.cabang-rental', $value->kode) }}" class="btn btn-success"><i class="fa fa-store"></i></a>
                                        <a href="{{ route('master-data.rental.admin', $value->RentalUtama->kode) }}" class="btn bg-navy"><i class="fa fa-users"></i></a>
                                        <a href="{{ route('master-data.rental.kendaraan', $value->RentalUtama->kode) }}" class="btn bg-maroon"><i class="fa fa-car"></i></a>
                                        <a href="{{ route('master-data.rental.edit', $value->kode) }}" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger delete', 'type' => 'submit']) !!}
                                    {{ Form::close() }}
                                </td>
                            </tr>
@endforeach
                        </tbody>
                    </table>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/dist/js/dataTables.js') }}"></script>
<script>
	$(function () {
        $("#dataTable").dataTable();
		$(document).on("click", ".delete", function(e) {
			e.preventDefault();
			var $form = $(this).closest("form");
			if(confirm("Apakah anda yakin?\nData tidak dapat dikembalikan.")) {
				$form.submit();
			}
		});
	})
</script>
@endsection