@extends('admin.misc.layout_')
@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
		<li>
			<a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
		</li>
		<li>
			<a href="{{ route('master-data.cabang-rental', $data->kode_rental_utama) }}"><i class="fa fa-store-alt"></i>Cabang Rental</a>
		</li>
		<li class="active">
            <a href="{{ route('master-data.cabang-rental.edit', [$data->kode_rental_utama, $data->kode]) }}">Edit</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Cabang Rental</h3>
                </div>
                {{ Form::model($data, ['route' => ['master-data.cabang-rental.edit', $data->kode_rental_utama, $data->kode], 'class' => 'form-horizontal','method' => 'patch']) }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                        {{ Form::label('no_telp', 'No Telepon', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::number('no_telp', $data->no_telp, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                            @if($errors->has('no_telp'))
                            <span class="help-block">{{ $errors->first('no_telp') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                        {{ Form::label('email', 'Email', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::email('email', $data->email, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                            @if($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kode_lokasi') ? ' has-error' : null }}">
                        {{ Form::label('kode_lokasi', 'Lokasi', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::select('kode_lokasi', $wlyh, $data->kode_lokasi, ['class' => 'form-control select2', 'required', 'autocomplete' => 'off']) }}
                            @if($errors->has('kode_lokasi'))
                            <span class="help-block">{{ $errors->first('kode_lokasi') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                        {{ Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::textarea('alamat', $data->alamat, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                            @if($errors->has('alamat'))
                            <span class="help-block">{{ $errors->first('alamat') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('pemilik', 'Admin Utama', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::text('pemilik', $data->AdminUtama->no_ktp . ' - ' . $data->AdminUtama->nama, ['class' => 'form-control', 'disabled']) }}</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-js')
    <script src="{{ asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(".select2").select2();
    </script>
@endsection
