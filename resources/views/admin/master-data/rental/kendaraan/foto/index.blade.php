@extends('admin.misc.layout_')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li>
			<a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
        </li>
		<li>
            {{ $data_rental->nama }}
        </li>
        <li>
			<a href="{{ route('master-data.rental.kendaraan', $data_rental->kode) }}"><i class="fa fa-car"></i>Kendaraan</a>
        </li>
        <li class="active">
                <a href="{{ route('master-data.rental.kendaraan.foto', [$data_rental->kode, $data->kode]) }}"><i class="fa fa-image"></i>Foto</a>
        </li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
            @include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">List Foto Kendaraan</h3>
				</div>
				<div class="box-body">
                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a href="{{ route('master-data.rental.kendaraan.foto.add', [$data_rental->kode, $data->kode]) }}">
                                    {!! Form::button('Add New <i class="fa fa-plus"></i>', ['class' => 'btn btn-sm btn-info']) !!}
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered order-column" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center" width="2.5%">No</th>
                                <th class="text-center">Foto</th>
                                <th class="text-center">Menu</th>
                            </tr>
                        </thead>
                        <tbody>
@foreach ($data->Foto as $value)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}.</td>
                                <td class="text-center"><img src="{{ asset('assets/front/images/kp/' . $value->nama) }}" width="200"></td>
                                <td class="text-center">
                                    {{ Form::model($value, ['route' => ['master-data.rental.kendaraan.foto.delete', $data_rental->kode, $data->kode, $value->kode], 'method' => 'delete']) }}
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger delete', 'type' => 'submit']) !!}
                                    {{ Form::close() }}
                                </td>
                            </tr>
@endforeach
                        </tbody>
                    </table>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/front/js/sweetalert.min.js') }}"></script>
<script>
	$(function () {
        $("#dataTable").dataTable({
            searching: false,
        });
		$(document).on("click", ".delete", function(e) {
			e.preventDefault();
			var $form = $(this).closest("form");
            swal({
                title: "Hapus Foto Kendaraan",
                text: "Apakah anda yakin?\nData tidak dapat dikembalikan.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete) {
				    $form.submit();
                }
            });
		});
	})
</script>
@endsection