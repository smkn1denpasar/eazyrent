@extends('admin.misc.layout_')
@section('custom-css')
<style>
    #list-file
    {
        margin-top: 10px;
    }
    .image-name
    {
        padding: 5px;
        display: inline-block;
    }
    .image-name > img
    {
        width: 150px;
    }
</style>
@endsection
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
        <li>
            <a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
        </li>
        <li>
            {{ $data_rental->nama }}
        </li>
        <li>
            <a href="{{ route('master-data.rental.kendaraan', $data_rental->kode) }}"><i class="fa fa-car"></i>Kendaraan</a>
        </li>
        <li>
            <a href="{{ route('master-data.rental.kendaraan.foto', [$data_rental->kode, $data->kode]) }}"><i class="fa fa-image"></i>Foto</a>
        </li>
        <li class="active">
            <a href="{{ route('master-data.rental.kendaraan.foto.add', [$data_rental->kode, $data->kode]) }}">Add</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        {{ Form::open(['route' => ['master-data.rental.kendaraan.foto.add', $data_rental->kode, $data->kode], 'enctype' => 'multipart/form-data']) }}
        <div class="col-lg-12">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Foto Kendaraan</h3>
                </div>
                <div class="box-body">
                    <div class="form-group{{ $errors->has('foto') || $errors->has('foto.*') ? ' has-error' : null }}">
                        {{ Form::label('foto', 'Foto') }}
                        {{ Form::file('foto[]', ['accept' => 'image/*', 'multiple', 'id' => 'foto', 'required']) }}
                        <div id="list-file"></div>
                        @if($errors->has('foto') || $errors->has('foto.*'))
                            <span class="help-block">{{ $errors->first('foto') ? $errors->first('foto') : $errors->first('foto.*') }}</span>
                        @endif
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection

@section('custom-js')
<script>
    $("button[type=reset]").on("click", function(e) {
        e.preventDefault();
        var foto = $("#foto").get(0).files.length;
        if(foto != 0) $(".image-name").remove();
        $("form").trigger("reset");
    })
    $("#foto").change(function(e) {
        $(".image-name").remove();
        if (window.File && window.FileList && window.FileReader) {
            if (this.files) {
                for(var i=0; i<this.files.length;i++) {
                    var file = this.files[i];
                    if (!file.type.match('image')) continue;
                    var reader = new FileReader();
                    reader.addEventListener("load", function(event) {
                        var picture = '<div class="image-name"><img src="' + event.target.result + '"></div>';
                        $("#list-file").append(picture);
                    });
                    reader.readAsDataURL(file);
                }
            }
        } else {
            console.log("Your browser does not support File API");
        }
    });
</script>
@endsection
