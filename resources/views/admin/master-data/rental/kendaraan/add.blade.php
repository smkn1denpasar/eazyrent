@extends('admin.misc.layout_')
@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
        <li>
            <a href="{{ route('master-data.rental') }}"><i class="fa fa-store"></i>Rental</a>
        </li>
        <li>
            {{ $rental->Detail->nama }}
        </li>
        <li>
            <a href="{{ route('master-data.rental.kendaraan', $rental->kode) }}"><i class="fa fa-car"></i>Kendaraan</a>
        </li>
        <li class="active">
            <a href="{{ route('master-data.rental.kendaraan.add', $rental->kode) }}">Add</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        {{ Form::open(['route' => ['master-data.rental.kendaraan.add', $rental->kode]]) }}
        <div class="col-lg-6">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Pemilik</h3>
                </div>
                <div class="box-body">
                    <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : null }}">
                        {{ Form::label('no_ktp', 'No KTP') }}
                        {{ Form::text('no_ktp', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('no_ktp'))
                        <span class="help-block">{{ $errors->first('no_ktp') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                        {{ Form::label('nama', 'Nama') }}
                        {{ Form::text('nama', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('nama'))
                        <span class="help-block">{{ $errors->first('nama') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                        {{ Form::label('alamat', 'Alamat') }}
                        {{ Form::textarea('alamat', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('alamat'))
                        <span class="help-block">{{ $errors->first('alamat') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                        {{ Form::label('no_telp', 'No Telepon') }}
                        {{ Form::number('no_telp', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('no_telp'))
                        <span class="help-block">{{ $errors->first('no_telp') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Kendaraan</h3>
                </div>
                <div class="box-body">
                    <div class="form-group{{ $errors->has('merk') ? ' has-error' : null }}">
                        {{ Form::label('merk', 'Merk') }}
                        {{ Form::select('merk', $merk, null, ['class' => 'form-control select2', 'placeholder' => '--- Please Select ---', 'width' => '100%']) }}
                        @if($errors->has('merk'))
                        <span class="help-block">{{ $errors->first('merk') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('jenis') ? ' has-error' : null }}">
                        {{ Form::label('jenis', 'Jenis') }}
                        {{ Form::text('jenis', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('jenis'))
                        <span class="help-block">{{ $errors->first('jenis') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('transmisi') ? ' has-error' : null }}">
                        {{ Form::label('transmisi', 'Transmisi') }}
                        {{ Form::select('transmisi', $transmisi, null, ['class' => 'form-control', 'placeholder' => '--- Please Select ---']) }}
                        @if($errors->has('transmisi'))
                        <span class="help-block">{{ $errors->first('transmisi') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('plat') ? ' has-error' : null }}">
                        {{ Form::label('plat', 'Plat') }}
                        {{ Form::text('plat', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('plat'))
                        <span class="help-block">{{ $errors->first('plat') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('tahun') ? ' has-error' : null }}">
                        {{ Form::label('tahun', 'Tahun') }}
                        {{ Form::number('tahun', null, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) }}
                        @if($errors->has('tahun'))
                        <span class="help-block">{{ $errors->first('tahun') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('tarif') ? ' has-error' : null }}">
                        {{ Form::label('tarif', 'Tarif') }}
                        {{ Form::number('tarif', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
                        @if($errors->has('tarif'))
                        <span class="help-block">{{ $errors->first('tarif') }}</span>
                        @endif
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/node_modules/axios/dist/axios.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/dist/js/custom.js') }}"></script>
<script>
    $("#no_ktp").autocomplete({
        source: "/admin/search/user-ktp",
        minLength: 3,
        select: function (event, ui) {
            $("#nama, #alamat, #no_telp, #no_ktp").attr('disabled', 'disabled');
            axios('/admin/search/user-data/' + ui.item.kode, {
                method: 'get',
            })
            .then(function (response) {
                $("#no_ktp").removeAttr("disabled");
                $("#nama").val(response.data.nama);
                $("#alamat").val(response.data.alamat);
                $("#no_telp").val(response.data.no_telp);
            })
            .catch(function (response) {
                console.log(response)
            });
        }
    });
    $("#jenis").autocomplete({
        source: "/admin/search/jenis-kendaraan",
        minLength: 3,
    });
    $("#no_ktp").on("keyup", function () {
        $("#nama, #alamat, #no_telp").removeAttr('disabled').val("");
    });
</script>
@endsection
