@extends('admin.misc.layout_')
@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li>
			<a href="{{ route('master-data.kendaraan') }}"><i class="fa fa-car"></i> Kendaraan</a>
		</li>
		<li class="active">
			<a href="{{ Request::url() }}"> Edit</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			@include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Kendaraan</h3>
				</div>
                {{ Form::model($data, ['route' => ['master-data.kendaraan.edit', $data->id], 'class' => 'form-horizontal', 'method' => 'patch']) }}
					<div class="box-body">
                        {{ Form::hidden('type', 0) }}
                        <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : null }}">
                            {{ Form::label('no_ktp', 'No KTP', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('no_ktp', $data->pemilik->no_ktp, ['class' => 'form-control', 'required']) }}
                                @if($errors->has('no_ktp'))
                                    <span class="help-block">{{ $errors->first('no_ktp') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
                            {{ Form::label('nama', 'Nama', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('nama', $data->pemilik->nama, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('nama'))
                                    <span class="help-block">{{ $errors->first('nama') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
                            {{ Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::textarea('alamat', $data->pemilik->alamat, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('alamat'))
                                    <span class="help-block">{{ $errors->first('alamat') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
                            {{ Form::label('no_telp', 'No Telepon', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('no_telp', $data->pemilik->no_telp, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('no_telp'))
                                    <span class="help-block">{{ $errors->first('no_telp') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('kendaraan', 'Kendaraan', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('kendaraan', $data->full_name_kendaraan, ['class' => 'form-control', 'disabled', 'autocomplete' => 'off']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('plat', 'Plat', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('plat', $data->plat, ['class' => 'form-control', 'disabled', 'autocomplete' => 'off']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('tahun', 'Tahun', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::number('tahun', $data->tahun, ['class' => 'form-control', 'disabled', 'autocomplete' => 'off']) }}
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('tarif') ? ' has-error' : null }}">
                            {{ Form::label('tarif', 'Tarif', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::number('tarif', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                                @if($errors->has('tarif'))
                                    <span class="help-block">{{ $errors->first('tarif') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('transmisi', 'Transmisi', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::text('transmisi', $data->human_transmisi, ['class' => 'form-control', 'disabled']) }}
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : null }}">
                            {{ Form::label('status', 'Status', ['class' => 'control-label col-md-2']) }}
                            <div class="col-md-10">
                                {{ Form::select('status', $status, null, ['class' => 'form-control', 'required']) }}
                                @if($errors->has('status'))
                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Submit</button>
						<button type="reset" class="btn btn-danger">Reset</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('custom-js')
    <script src="{{ asset('assets/admin/global/scripts/jquery.mockjax.min.js') }}"></script>
    <script src="{{ asset('assets/admin/global/scripts/jquery.autocomplete.min.js') }}"></script>
    <script src="{{ asset('assets/admin/global/scripts/custom.autocomplete.kendaraan.js') }}"></script>
@endsection