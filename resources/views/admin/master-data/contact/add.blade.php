@extends('admin.misc.layout_')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
        </li>
		<li>
            <a href="{{ route('master-data.contact') }}"><i class="fa fa-phone"></i>Contact</a>
        </li>
		<li class="active">
            <a href="{{ route('master-data.contact.add') }}">Add</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            @include('admin.misc.alert_')
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Contact</h3>
                </div>
                {{ Form::open(['route' => 'master-data.contact.add', 'class' => 'form-horizontal', 'method' => 'post']) }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : null }}">
                        {{ Form::label('type', 'Type', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::select('type', $type, null, ['class' => 'form-control', 'required', 'autocomplete' => 'off', 'placeholder' => '--- Please Select ---']) }}
                            @if ($errors->has('type'))
                                <span class="help-block">{{ $errors->first('type') }}</span>                                
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('data') ? ' has-error' : null }}">
                        {{ Form::label('data', 'Data', ['class' => 'control-label col-md-2']) }}
                        <div class="col-md-10">
                            {{ Form::text('data', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
                            @if ($errors->has('data'))
                                <span class="help-block">{{ $errors->first('data') }}</span>                                
                            @endif
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection