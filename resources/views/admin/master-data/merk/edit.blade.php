@extends('admin.misc.layout_')
@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li>
			<a href="{{ route('master-data.merk') }}"><i class="fa fa-list"></i> Merk Kendaraan</a>
		</li>
		<li class="active">
			<a href="{{ Request::url() }}"> Edit</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			@include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Merk Kendaraan</h3>
				</div>
				{{ Form::model($data, ['route' => ['master-data.merk.edit', $data->id], 'class' => 'form-horizontal', 'method' => 'patch']) }}
					<div class="box-body">
						{{ Form::hidden('type', 0) }}
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
							{{ Form::label('nama', 'Nama Merk', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::text('nama', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('nama'))
									<span class="help-block">{{ $errors->first('nama') }}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Submit</button>
						<button type="reset" class="btn btn-danger">Reset</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</section>
@endsection