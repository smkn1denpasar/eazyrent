@extends('admin.misc.layout_')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li class="active">
			<a href="{{ route('master-data.merk') }}"><i class="fa fa-list"></i> Merk Kendaraan</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
            @include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">List Merk Kendaraan</h3>
				</div>
				<div class="box-body">
                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a href="{{ route('master-data.merk.add') }}">
                                    {!! Form::button('Add New <i class="fa fa-plus"></i>', ['class' => 'btn btn-sm btn-info']) !!}
                                </a>
                            </div>
                        </div>
                    </div>
					<table class="table table-bordered" id="dataTable" width="100%">
                        <thead>
                            <tr>
                                <th width="2.5%">No</th>
                                <th>Nama Merk</th>
                                <th class="col-md-2">Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $value->nama }}</td>
                                    <td>
                                        {{ Form::model($value, ['route' => ['master-data.merk.delete', $value->kode], 'method' => 'delete']) }}
                                            <a href="{{ route('master-data.merk.edit', $value->kode) }}" class="btn btn-info">Edit</a>
                                            {{ Form::button('Delete', ['class' => 'btn btn-danger delete', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/dist/js/dataTables.js') }}"></script>
<script>
	$(function () {
        $("#dataTable").dataTable();
		$(document).on("click", ".delete", function(e) {
			e.preventDefault();
			var $form = $(this).closest("form");
			if(confirm("Apakah anda yakin?\nData tidak dapat dikembalikan.")) {
				$form.submit();
			}
		});
	})
</script>
@endsection