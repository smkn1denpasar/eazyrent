@extends('admin.misc.layout_')
@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li>
			<a href="{{ route('master-data.karyawan') }}"><i class="fa fa-users"></i> Karyawan</a>
		</li>
		<li class="active">
			<a href="{{ route('master-data.karyawan.add') }}"> Add</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			@include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Add Karyawan</h3>
				</div>
				{{ Form::open(['route' => 'master-data.karyawan.add', 'class' => 'form-horizontal']) }}
					<div class="box-body">
						{{ Form::hidden('type', 0) }}
						<div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : null }}">
							{{ Form::label('no_ktp', 'No KTP', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::number('no_ktp', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('no_ktp'))
									<span class="help-block">{{ $errors->first('no_ktp') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
							{{ Form::label('email', 'Email', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::email('email', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('email'))
									<span class="help-block">{{ $errors->first('email') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : null }}">
							{{ Form::label('nama', 'Nama', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::text('nama', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('nama'))
									<span class="help-block">{{ $errors->first('nama') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('jenis_kelamin', 'Jenis Kelamin', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::select('jenis_kelamin', $gender, null, ['class' => 'form-control', 'placeholder' => '-- Please Select --', 'required']) }}
								@if($errors->has('jenis_kelamin'))
									<span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('alamat') ? ' has-error' : null }}">
							{{ Form::label('alamat', 'Alamat', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::textarea('alamat', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('alamat'))
									<span class="help-block">{{ $errors->first('alamat') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('no_telp') ? ' has-error' : null }}">
							{{ Form::label('no_telp', 'No Telepon/Hp', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::number('no_telp', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) }}
								@if($errors->has('no_telp'))
									<span class="help-block">{{ $errors->first('no_telp') }}</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('role', 'Role', ['class' => 'control-label col-md-2']) }}
							<div class="col-md-10">
								{{ Form::select('role', $role, null, ['class' => 'form-control', 'placeholder' => '-- Please Select --', 'required']) }}
								@if($errors->has('role'))
									<span class="help-block">{{ $errors->first('role') }}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Submit</button>
						<button type="reset" class="btn btn-danger">Reset</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</section>
@endsection