@extends('admin.misc.layout_')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-cogs"></i> Master Data</a>
		</li>
		<li class="active">
			<a href="{{ route('master-data.karyawan') }}"><i class="fa fa-users"></i> Karyawan</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
            @include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">List Karyawan</h3>
				</div>
				<div class="box-body">
                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a href="{{ route('master-data.karyawan.add') }}">
                                    {!! Form::button('Add New <i class="fa fa-plus"></i>', ['class' => 'btn btn-sm btn-info']) !!}
                                </a>
                            </div>
                        </div>
                    </div>
					<table class="table table-bordered" id="dataTable" width="100%">
                        <thead>
                            <tr>
                                <th width="2.5%">No</th>
                                <th>No KTP</th>
                                <th>Nama</th>
                                <th>No Telepon/Hp</th>
                                <th>Jenis Kelamin</th>
                                <th>Status</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $value->no_ktp }}</td>
                                    <td>{{ $value->nama }}</td>
                                    <td>{{ $value->no_telp }}</td>
                                    <td>{{ $value->human_gender }}</td>
                                    <td>{{ $value->human_role }}</td>
                                    <td>
                                        {{ Form::model($value, ['route' => ['master-data.karyawan.delete', $value->id], 'method' => 'delete']) }}
                                            <a href="{{ route('master-data.karyawan.edit', $value->id) }}" class="btn btn-info">Edit</a>
                                            {{ Form::button('Delete', ['class' => 'btn btn-danger delete', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/dist/js/dataTables.js') }}"></script>
<script>
	$(function () {
        $("#dataTable").dataTable();
		$(document).on("click", ".delete", function(e) {
			e.preventDefault();
			var $form = $(this).closest("form");
			if(confirm("Apakah anda yakin?\nData tidak dapat dikembalikan.")) {
				$form.submit();
			}
		});
	})
</script>
@endsection