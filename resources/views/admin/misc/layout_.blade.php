<!DOCTYPE html>
<html>
@include('admin.misc.css_')
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('admin.misc.header_')
		@include('admin.misc.sidebar_')
		<div class="content-wrapper">
			@yield('content')
		</div>
		<div class="control-sidebar-bg"></div>
	</div>
@include('admin.misc.js_')	
</body>

</html>