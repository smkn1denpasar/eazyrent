<header class="main-header">
    <a href="index.html" class="logo">
        <span class="logo-mini"><b>EZR</b></span>
        <span class="logo-lg"><b>Eazy</b> Rent</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
    </nav>
</header>