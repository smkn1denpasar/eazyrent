<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->nama }}</p>
                <a href="javascript:;"><i class="fa fa-user"></i> {{ Auth::user()->human_role }}</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li @if(Request::segment(2) == "") class="active" @endif>
                <a href="{{ route('admin.home') }}">
                    <i class="fal fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            @if(\Gate::denies('as_customer_service'))
            <li class="treeview @if(Request::segment(2) == "master-data") active @endif">
                <a href="#">
                    <i class="fal fa-cogs"></i> <span>Master Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(\Gate::allows('as_master_admin'))
                    <li @if(Request::segment(3) == "karyawan") class="active"@endif>
                        <a href="{{ route('master-data.karyawan') }}"><i class="fal fa-users"></i> Manage Karyawan</a>
                    </li>
                    @endif
                    <li @if(Request::segment(3) == "rental") class="active"@endif>
                        <a href="{{ route('master-data.rental') }}"><i class="fal fa-store"></i> Manage Rental</a>
                    </li>
                    <li @if(Request::segment(3) == "merk") class="active"@endif>
                        <a href="{{ route('master-data.merk') }}"><i class="fal fa-list"></i> Manage Merk Kendaraan</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(\Gate::denies('as_administrator'))
            <li class="treeview">
                <a href="#">
                    <i class="fal fa-exchange"></i> <span>Transaksi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(Request::segment(3) == "transaksi") class="active"@endif>
                        <a href="{{ route('transaksi') }}"><i class="fal fa-list-alt"></i> Semua Transaksi</a>
                    </li>
                    <li @if(Request::segment(3) == "transaksi" && Request::segment(4) == "bulan") class="active"@endif>
                        <a href="{{ route('transaksi-per-bulan') }}"><i class="fal fa-calendar-alt"></i> Transaksi Per Bulan</a>
                    </li>
                </ul>
            </li>
            @endif
            <li>
                <a href="{{ route('admin.logout') }}">
                    <i class="fal fa-power-off"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
</aside>