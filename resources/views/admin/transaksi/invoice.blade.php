@extends('admin.misc.layout_')
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('transaksi') }}"><i class="fa fa-exchange-alt"></i> Transaksi</a>
		</li>
		<li class="active">
			<a href="{{ route('transaksi.show', $data->kode) }}"><i class="fa fa-file-invoice"></i> Invoice</a>
		</li>
    </ol>
</section>
<section class="invoice" id="invoice">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                Invoice #{{ $data->kode }}
                <small class="pull-right"><b>Tanggal & Waktu Transaksi:</b> {{ date('d/m/Y', strtotime($data->tanggal_transaksi)) }}</small>
            </h2>
        </div>
    </div>
    <div class="row invoice-info">
		<div class="col-sm-6 invoice-col">
			<b>Nama:</b> {{ $data->Data->nama }}<br>
			<b>No Telepon:</b> {{ $data->Data->no_telp }}<br>
			<b>Email:</b> {{ $data->Data->email }}<br>
			@if($data->Data->alamat_diantar)
			<b>Alamat:</b> {{ $data->Data->alamat_diantar }}
			@endif
		</div>
        <div class="col-sm-6 invoice-col">
			<b>Customer:</b> {{ $data->Customer->nama }}<br>
			<b>Rental:</b> {{ $data->Rental->nama }}<br>
            <b>Status:</b> <p class="label label-{{ $data->status == "selesai" ? 'success' : 'danger' }}">{{ $data->realStatus }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped detail-table">
                <thead>
                    <tr>
                        <th class="text-center">Kendaraan</th>
						<th class="text-center">Plat</th>
						<th class="text-center">Jenis Pengambilan</th>
						<th class="text-center">Jenis Pembayaran</th>
                        <th class="text-center">Tanggal & Waktu Pengambilan</th>
                        <th class="text-center">Tanggal & Waktu Pengembalian</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">{{ $data->Kendaraan->fullNameKendaraan }}</td>
                        <td class="text-center">{{ $data->Kendaraan->plat }}</td>
                        <td class="text-center">{{ $data->realPengambilan }}</td>
                        <td class="text-center">{{ $data->realPembayaran }}</td>
                        <td class="text-center">{{ $data->tanggal_waktu_pengambilan }}</td>
                        <td class="text-center">{{ $data->tanggal_waktu_pengembalian }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
		@if($data->Data->informasi)
		<div class="col-xs-6">
			<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
				{{ $data->Data->informasi }}
			</p>
		</div>
		@endif
		<div class="col-xs-{{ $data->Data->informasi ? 6 : 12 }}">
			<div class="table-responsive">
				<table class="table">
					<tr>
						<th style="width:80%">Durasi</th>
						<td>{{ $data->durasi }} Hari</td>
					</tr>
					<tr>
						<th>Harga Sewa</th>
						<td>{{ $data->rpHarga }}</td>
					</tr>
					@if($data->diskon)
					<tr>
						<th>Diskon:</th>
						<td>{{ $data->diskon }}%</td>
					</tr>
					@endif
					@if($data->biaya_antar)
					<tr>
						<th>Biaya Antar:</th>
						<td>{{ $data->rp_biaya_antar }}</td>
					</tr>
					@endif
					<tr>
						<th>Total:</th>
						<td>{{ $data->rpTotal }}</td>
					</tr>
				</table>
			</div>
        </div>
    </div>
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="{{ route('transaksi.print', $data->kode) }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <a href="{{ route('transaksi.pdf', $data->kode) }}" class="btn btn-primary pull-right"><i class="fa fa-file-pdf"></i> Generate PDF</a>
        </div>
    </div>
</section>
<div class="clearfix"></div>
@endsection