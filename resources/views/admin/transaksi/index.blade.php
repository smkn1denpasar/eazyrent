@extends('admin.misc.layout_')

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li>
			<a href="javascript:;"><i class="fa fa-exchange"></i> Transaksi</a>
		</li>
		<li class="active">
			<a href="{{ route('master-data.kendaraan') }}"><i class="fa fa-car"></i>Kendaraan</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
            @include('admin.misc.alert_')
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">List Kendaraan</h3>
				</div>
				<div class="box-body">
                    <table class="table table-striped table-bordered order-column" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center" width="2.5%">No</th>
                                <th class="text-center">Merk/Jenis</th>
                                <th class="text-center">Plat</th>
                                <th class="text-center">Tarif</th>
                                <th class="text-center">Tahun</th>
                                <th class="text-center">Transmisi</th>
                                <th class="text-center">Pemilik</th>
                                <th class="text-center">Rental</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Menu</th>
                            </tr>
                        </thead>
                        <tbody>
@foreach ($data as $value)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}.</td>
                                <td class="text-center">{{ $value->merk->nama . " " .$value->jenis }}</td>
                                <td class="text-center">{{ $value->plat }}</td>
                                <td class="text-center">{{ $value->human_tarif }}</td>
                                <td class="text-center">{{ $value->tahun }}</td>
                                <td class="text-center">{{ $value->human_transmisi }}</td>
                                <td class="text-center">{{ $value->Pemilik->nama }}</td>
                                <td class="text-center">{{ $value->Rental->nama }}</td>
                                <td class="text-center">{{ $value->human_status }}</td>
                                <td class="text-center">
                                    {{ Form::model($value, ['route' => ['master-data.kendaraan.delete', $value->id], 'method' => 'delete']) }}
                                        <a href="{{ route('master-data.kendaraan.edit', $value->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger delete', 'type' => 'submit']) !!}
                                    {{ Form::close() }}
                                </td>
                            </tr>
@endforeach
                        </tbody>
                    </table>
				</div>
			</div>

		</div>
	</div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/dist/js/dataTables.js') }}"></script>
<script>
	$(function () {
        $("#dataTable").dataTable();
		$(document).on("click", ".delete", function(e) {
			e.preventDefault();
			var $form = $(this).closest("form");
			if(confirm("Apakah anda yakin?\nData tidak dapat dikembalikan.")) {
				$form.submit();
			}
		});
	})
</script>
@endsection