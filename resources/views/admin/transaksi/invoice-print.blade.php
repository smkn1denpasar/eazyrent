<!DOCTYPE html>
<html>
@include('admin.misc.css_')
    <body onload="window.print();">
        <style>
            .detail-table > thead > tr > th
            {
                vertical-align: middle;
            }
        </style>
        <div class="wrapper">
            <section class="invoice">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            Invoice #{{ $data->kode }}
                            <small class="pull-right"><b>Tanggal & Waktu Transaksi:</b> {{ date('d/m/Y', strtotime($data->tanggal_transaksi)) }}</small>
                        </h2>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-xs-6 invoice-col">
                        <b>Nama:</b> {{ $data->Data->nama }}<br>
                        <b>No Telepon:</b> {{ $data->Data->no_telp }}<br>
                        <b>Email:</b> {{ $data->Data->email }}<br>
                        @if($data->Data->alamat_diantar)
                        <b>Alamat:</b> {{ $data->Data->alamat_diantar }}
                        @endif
                    </div>
                    <div class="col-xs-6 invoice-col">
                        <b>Customer:</b> {{ $data->Customer->nama }}<br>
                        <b>Rental:</b> {{ $data->Rental->nama }}<br>
                        <b>Status:</b> {{ $data->realStatus }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped detail-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Kendaraan</th>
                                    <th class="text-center">Plat</th>
                                    <th class="text-center">Jenis Pengambilan</th>
                                    <th class="text-center">Jenis Pembayaran</th>
                                    <th class="text-center">Tanggal & Waktu Pengambilan</th>
                                    <th class="text-center">Tanggal & Waktu Pengembalian</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{ $data->Kendaraan->fullNameKendaraan }}</td>
                                    <td class="text-center">{{ $data->Kendaraan->plat }}</td>
                                    <td class="text-center">{{ $data->realPengambilan }}</td>
                                    <td class="text-center">{{ $data->realPembayaran }}</td>
                                    <td class="text-center">{{ $data->tanggal_waktu_pengambilan }}</td>
                                    <td class="text-center">{{ $data->tanggal_waktu_pengembalian }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    @if($data->Data->informasi)
                    <div class="col-xs-6">
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            {{ $data->Data->informasi }}
                        </p>
                    </div>
                    @endif
                    <div class="col-xs-{{ $data->Data->informasi ? 6 : 12 }}">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="width:50%">Durasi</th>
                                    <td class="pull-right">{{ $data->durasi }} Hari</td>
                                </tr>
                                <tr>
                                    <th>Harga Sewa</th>
                                    <td class="pull-right">{{ $data->rpHarga }}</td>
                                </tr>
                                @if($data->diskon)
                                <tr>
                                    <th>Diskon:</th>
                                    <td class="pull-right">{{ $data->diskon }}%</td>
                                </tr>
                                @endif
                                @if($data->biaya_antar)
                                <tr>
                                    <th>Biaya Antar:</th>
                                    <td class="pull-right">{{ $data->rp_biaya_antar }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Total:</th>
                                    <td class="pull-right">{{ $data->rpTotal }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>