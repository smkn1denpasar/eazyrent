-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 04, 2019 at 07:03 PM
-- Server version: 8.0.13
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eazy_rent`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `no_ktp` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('master_admin','administrator','customer_service') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` char(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `kode`, `no_ktp`, `nama`, `email`, `alamat`, `no_telp`, `jenis_kelamin`, `role`, `password`, `remember_token`) VALUES
(1, '', '1234567890123456', 'Rheznendra Praditya Laksma Putra', 'rheznendra@gmail.com', 'Jalan Bung Tomo 1F/3B', '081239852799', 'laki-laki', 'master_admin', '$2y$12$bkYHtBB8ziARdGnl.PycJenJCIFwBaIZ2wZY37JdWBLaJSFmu98XG', 'xg4B4jaHQgiKu8bFRwoAGf3JhzX4ST2hyEK7pCxvG5MZp968nNosSgYr5HtJ'),
(2, '', '0987654321123456', 'Komang Pramayasa', 'pramayasa@gmail.com', 'Kebo Iwa2111123', '08123123113', 'laki-laki', 'master_admin', '$2y$10$Aw5k3hGAuT71GJo3CmMhgOoEMncQ.opzaMFwO44jh4w9F7f0mstKq', 'O7asd9FipVuD0jWIwv1nB652nVN2MkiyjqKdN3Mq6ieSQ93uNrcLNVDI2yBz'),
(3, '', '7531613790519830', 'Raharjo Ary Salim', 'RaharjoArySalim@collectablesexpo.com', '6355 Hickman Street Sugar Grove, IL 4458, United States', '02157951536', 'laki-laki', 'administrator', '$2y$10$VWnOcTrxPuiPrjnwikUz5emPQtjwwFyQkJ1qlRm.6pvgCs6zQdYVm', 'entRAmXTUIwZt86Lo4mf4euUrGGuzPODK7YR4Orm9imOOHDxymRQO9VamY42'),
(4, '', '8513508704770769', 'Pamela M Carter', 'stefan_murp@hotmail.com', '123 Young Road', '2083064105', 'laki-laki', 'customer_service', '$2y$10$SgX8b0pRBXkfMoO9UT6UfetnI97Rkk0XrgUlU.e7E3lgTB1KApfru', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_transaksi`
--

CREATE TABLE `tb_data_transaksi` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_transaksi` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) COLLATE utf8_unicode_ci NOT NULL,
  `informasi` text COLLATE utf8_unicode_ci,
  `alamat_diantar` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_data_transaksi`
--

INSERT INTO `tb_data_transaksi` (`id`, `kode`, `kode_transaksi`, `nama`, `no_telp`, `email`, `informasi`, `alamat_diantar`) VALUES
(1, 'I054H', '3O97E', 'Rheznendra Praditya', '81239852799', 'rheznendra@gmail.com', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aperiam, officia facere numquam, officiis quod, inventore earum quam possimus aliquid necessitatibus vel exercitationem. Ab corporis impedit voluptate quod quas sunt saepe!', NULL),
(4, 'CZZNS', 'OKPW7', 'Rheznendra Praditya', '8484842213', 'rheznendra@gmail.com', NULL, 'lorem ipsum'),
(5, 'DA0EL', 'ISRQQ', 'Rheznendra Praditya', '081239852799', 'rheznendra@gmail.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_foto_kendaraan`
--

CREATE TABLE `tb_foto_kendaraan` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_kendaraan` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_foto_kendaraan`
--

INSERT INTO `tb_foto_kendaraan` (`id`, `kode`, `kode_kendaraan`, `nama`) VALUES
(15, 'FKM30', 'TNKVI', '1892rc3rtz0-j5dv0e0r5a-10nk0-b29cecb7-eeb3-46a0-9d6d-20e3b319c11e.jpeg'),
(17, 'EHPEB', 'TNKVI', '15r-k03z800ar95crte1nvj0-2d0-50c38977-161b-42bf-81bd-190c30b4f06a.jpeg'),
(18, 'B9JPC', 'Z6X6H', 'c910rv8td8j03z11r-1k324enra--f5304811-fc2c-4415-91a9-25b2a796dc8a.jpeg'),
(19, '4PNSL', 'Z6X6H', '0-j231d81-n8cr1ra0t1k943zrev-b13fb3b9-bab1-4133-a86b-2136f7beb666.jpeg'),
(20, '6SP4F', 'Z6X6H', 'rr-1v1dre8k01t8c193zn-j0342a-9d516efc-536f-44f3-8441-1fe669ff986c.jpeg'),
(21, 'H1PHE', 'TNKVI', 'c4kt8-v9r1r021z90ned-jr3813a-729e3665-d1f3-4635-99bb-089af557a26f.jpeg'),
(22, 'U0EWD', 'TNKVI', 'rzv1d0-c0jrr08-4knt8e1924a10-379ca40e-f60d-40e6-97e3-296b2b38d305.jpeg'),
(23, 'YF7KJ', 'TNKVI', '4j8ae0tnr01k018910r-zcr4-v2d-27eb7807-a1e6-41b9-b3c9-29d59cb2c26b.jpeg'),
(42, 'ZOIPQ', 'WJCNZ', 'k00nr104dbt85zr9e4ar-io420j7-a6d7737e-3e4d-4ac6-a779-1874e2d357e1.jpeg'),
(43, 'JVOYN', 'WJCNZ', 'r1n20-r0o94jikeab84rt50dz740-5f7704a4-cd6f-417e-863c-1fc19ead60bf.jpeg'),
(44, '4DOSD', 'WJCNZ', 'j407aobr5k0e0z01d2t44ri9n-r8-097f0d23-57cd-49c9-9c56-0b3590444ae1.jpeg'),
(45, 'SQO2H', 'WJCNZ', '4-4ert58900ik2d7a0br0j41zonr-ca076e5f-9516-47ca-85e8-1a6d4536d427.jpeg'),
(46, '810UF', 'WJCNZ', 'riz2ja004re449b7n8k1t-r0d5o0-030cbe9d-f684-417b-930b-0f407090b453.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kendaraan`
--

CREATE TABLE `tb_kendaraan` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `plat` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `harga` double UNSIGNED NOT NULL,
  `diskon` float UNSIGNED DEFAULT NULL,
  `biaya_antar` double UNSIGNED DEFAULT NULL,
  `total_harga` double UNSIGNED NOT NULL,
  `transmisi` enum('manual','matic') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_km` double UNSIGNED NOT NULL,
  `bahan_bakar` enum('premium','pertamax','pertamax_plus','pertalite') COLLATE utf8_unicode_ci NOT NULL,
  `max_fuel` double UNSIGNED NOT NULL,
  `seat` tinyint(1) NOT NULL,
  `status` enum('tersedia','digunakan','service') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alamat_pengambilan` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `kode_merk` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_rental` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_lokasi` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_kendaraan`
--

INSERT INTO `tb_kendaraan` (`id`, `kode`, `plat`, `jenis`, `tahun`, `harga`, `diskon`, `biaya_antar`, `total_harga`, `transmisi`, `total_km`, `bahan_bakar`, `max_fuel`, `seat`, `status`, `alamat_pengambilan`, `kode_merk`, `kode_rental`, `kode_lokasi`, `deskripsi`) VALUES
(3, 'TNKVI', 'DK 500 ER', 'CR-V', 2018, 250000, 20, 30000, 200000, 'matic', 25444, 'pertamax_plus', 200, 5, 'digunakan', '', 'DA8KJ', 'E4RFR', '11.02', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(4, 'Z6X6H', 'LL 2221 OI', 'CR-V', 2013, 300000, NULL, 25000, 300000, 'matic', 1287, 'pertamax', 2000, 4, 'service', '', 'DA8KJ', 'E4RFR', '11.01', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati libero dolore facilis excepturi, nulla rerum quod, quaerat voluptates ad vel quas architecto quam doloribus iure. Ullam a ducimus consectetur dolor voluptatem ut esse qui? Fuga eius eligendi omnis excepturi cupiditate.'),
(7, 'WJCNZ', 'DK 2003 JI', 'Brio', 2019, 300000, NULL, 39987, 300000, 'matic', 40002, 'pertamax_plus', 35, 6, 'tersedia', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', 'DA8KJ', 'E4RFR', '11.12', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.');

-- --------------------------------------------------------

--
-- Table structure for table `tb_merk`
--

CREATE TABLE `tb_merk` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_merk`
--

INSERT INTO `tb_merk` (`id`, `kode`, `nama`) VALUES
(1, 'DA8KJ', 'Honda'),
(2, 'C151C', 'Toyota'),
(3, 'JN35Z', 'Porsche'),
(4, '45GB4', 'Mercedes-Benz'),
(5, '64A6Y', 'Ninja');

-- --------------------------------------------------------

--
-- Table structure for table `tb_password_resets`
--

CREATE TABLE `tb_password_resets` (
  `email` varchar(320) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_rental`
--

CREATE TABLE `tb_rental` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slogan` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_lokasi` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_rental`
--

INSERT INTO `tb_rental` (`id`, `kode`, `nama`, `slogan`, `username`, `email`, `no_telp`, `logo`, `alamat`, `kode_lokasi`, `deskripsi`) VALUES
(1, 'E4RFR', 'RZ Rent', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'rz-rent', 'rheznendra@gmail.com', '081239852799', NULL, 'Jalan Bung Tomo 1F No.3B\r\nDenpasar, Bali, Indonesia. 80119.', '11.12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rental_admin_utama`
--

CREATE TABLE `tb_rental_admin_utama` (
  `id` int(11) NOT NULL,
  `kode_user` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_rental` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_rental_admin_utama`
--

INSERT INTO `tb_rental_admin_utama` (`id`, `kode_user`, `kode_rental`) VALUES
(7, '6BD9O', 'E4RFR');

-- --------------------------------------------------------

--
-- Table structure for table `tb_social_media`
--

CREATE TABLE `tb_social_media` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('facebook','twitter','instagram','whatsapp','email','no_telp','location') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_social_media`
--

INSERT INTO `tb_social_media` (`id`, `kode`, `type`, `link`) VALUES
(1, 'O5UWM', 'facebook', 'http://eazyrent.devapp/admin/master-data/social-media/add'),
(2, 'C8DDO', 'instagram', 'http://instagram.com/'),
(3, 'SL1S3', 'no_telp', '+62 812 3985 2799'),
(9, 'VFZII', 'email', 'rheznendra@gmail.com'),
(10, '48945', 'location', 'Jalan Cokroaminoto No.84, Pemecutan Kaja, Denpasar Utara, Kota Denpasar, Bali 80116');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_user` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_kendaraan` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kode_rental` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_pengambilan` enum('diantar','ambil_sendiri') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `jenis_pembayaran` enum('cod') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `harga` double UNSIGNED NOT NULL,
  `diskon` float UNSIGNED DEFAULT NULL,
  `biaya_antar` double UNSIGNED DEFAULT NULL,
  `total` double UNSIGNED NOT NULL,
  `tanggal_waktu_pengambilan` datetime NOT NULL,
  `tanggal_waktu_pengembalian` datetime NOT NULL,
  `tanggal_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('pending','batal','selesai','digunakan') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id`, `kode`, `kode_user`, `kode_kendaraan`, `kode_rental`, `jenis_pengambilan`, `jenis_pembayaran`, `harga`, `diskon`, `biaya_antar`, `total`, `tanggal_waktu_pengambilan`, `tanggal_waktu_pengembalian`, `status`) VALUES
(1, '3O97E', '6BD9O', 'TNKVI', 'E4RFR', 'ambil_sendiri', 'cod', 200000, NULL, NULL, 200000, '2019-02-05 23:13:00', '2019-02-06 23:13:00', 'selesai'),
(4, 'OKPW7', '6BD9O', 'TNKVI', 'E4RFR', 'diantar', 'cod', 250000, 20, 30000, 230000, '2019-02-05 02:17:00', '2019-02-06 02:17:00', 'selesai'),
(5, 'ISRQQ', '6BD9O', 'WJCNZ', 'E4RFR', 'ambil_sendiri', 'cod', 300000, NULL, 39987, 300000, '2019-02-06 02:40:00', '2019-02-07 02:40:00', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `no_ktp` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` char(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `no_telp` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_lokasi` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `kode`, `no_ktp`, `nama`, `email`, `password`, `alamat`, `no_telp`, `jenis_kelamin`, `kode_lokasi`, `remember_token`, `email_verified_at`) VALUES
(6, '6BD9O', '1234567890123456', 'Rheznendra praditya', 'rheznendra@gmail.com', '$2y$10$TzvoVCu0MnoorxGg.bb2Nuc1P50zF1GVVpZLr.GNgX78CJbQWylFu', NULL, '081239852799', 'laki-laki', '11.05', '3Yo5fr8FK05vM0rJ9zsDaKUKJV00wn6x556XnV3Ga4194sPYWHTNoekX6aIU', '2019-01-12 22:16:31'),
(9, '6IX3G', NULL, 'Test User', 'test@test.com', '$2y$10$4DDWncscqubaOOBKnXECa.tuQ/MHbzbOnkPishsdL1P8bQnLgBIdO', NULL, '08240210', NULL, NULL, 'EWe6EIQg5JmtRPaCBMnesHWuGelXhJtfKW8ycPJCrWUXZ2ITZcPLGYCo4NxG', '2019-01-16 20:56:42');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wilayah`
--

CREATE TABLE `tb_wilayah` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_wilayah`
--

INSERT INTO `tb_wilayah` (`id`, `kode`, `nama`) VALUES
(1, '11', 'Aceh'),
(2, '11.01', 'Kab. Aceh Selatan'),
(3, '11.02', 'Kab. Aceh Tenggara'),
(4, '11.03', 'Kab. Aceh Timur'),
(5, '11.04', 'Kab. Aceh Tengah'),
(6, '11.05', 'Kab. Aceh Barat'),
(7, '11.06', 'Kab. Aceh Besar'),
(8, '11.07', 'Kab. Pidie'),
(9, '11.08', 'Kab. Aceh Utara'),
(10, '11.09', 'Kab. Simeulue'),
(11, '11.10', 'Kab. Aceh Singkil'),
(12, '11.11', 'Kab. Bireuen'),
(13, '11.12', 'Kab. Aceh Barat Daya'),
(14, '11.13', 'Kab. Gayo Lues'),
(15, '11.14', 'Kab. Aceh Jaya'),
(16, '11.15', 'Kab. Nagan Raya'),
(17, '11.16', 'Kab. Aceh Tamiang'),
(18, '11.17', 'Kab. Bener Meriah'),
(19, '11.18', 'Kab. Pidie Jaya'),
(20, '11.71', 'Kota Banda Aceh'),
(21, '11.72', 'Kota Sabang'),
(22, '11.73', 'Kota Lhokseumawe'),
(23, '11.74', 'Kota Langsa'),
(24, '11.75', 'Kota Subulussalam'),
(25, '12', 'Sumatera Utara'),
(26, '12.01', 'Kab. Tapanuli Tengah'),
(27, '12.02', 'Kab. Tapanuli Utara'),
(28, '12.03', 'Kab. Tapanuli Selatan'),
(29, '12.04', 'Kab. Nias'),
(30, '12.05', 'Kab. Langkat'),
(31, '12.06', 'Kab. Karo'),
(32, '12.07', 'Kab. Deli Serdang'),
(33, '12.08', 'Kab. Simalungun'),
(34, '12.09', 'Kab. Asahan'),
(35, '12.10', 'Kab. Labuhanbatu'),
(36, '12.11', 'Kab. Dairi'),
(37, '12.12', 'Kab. Toba Samosir'),
(38, '12.13', 'Kab. Mandailing Natal'),
(39, '12.14', 'Kab. Nias Selatan'),
(40, '12.15', 'Kab. Pakpak Bharat'),
(41, '12.16', 'Kab. Humbang Hasundutan'),
(42, '12.17', 'Kab. Samosir'),
(43, '12.18', 'Kab. Serdang Bedagai'),
(44, '12.19', 'Kab. Batu Bara'),
(45, '12.20', 'Kab. Padang Lawas Utara'),
(46, '12.21', 'Kab. Padang Lawas'),
(47, '12.22', 'Kab. Labuhanbatu Selatan'),
(48, '12.23', 'Kab. Labuhanbatu Utara'),
(49, '12.24', 'Kab. Nias Utara'),
(50, '12.25', 'Kab. Nias Barat'),
(51, '12.71', 'Kota Medan'),
(52, '12.72', 'Kota Pematangsiantar'),
(53, '12.73', 'Kota Sibolga'),
(54, '12.74', 'Kota Tanjung Balai'),
(55, '12.75', 'Kota Binjai'),
(56, '12.76', 'Kota Tebing Tinggi'),
(57, '12.77', 'Kota Padangsidimpuan'),
(58, '12.78', 'Kota Gunungsitoli'),
(59, '13', 'Sumatera Barat'),
(60, '13.01', 'Kab. Pesisir Selatan'),
(61, '13.02', 'Kab. Solok'),
(62, '13.03', 'Kab. Sijunjung'),
(63, '13.04', 'Kab. Tanah Datar'),
(64, '13.05', 'Kab. Padang Pariaman'),
(65, '13.06', 'Kab. Agam'),
(66, '13.07', 'Kab. Lima Puluh Kota'),
(67, '13.08', 'Kab. Pasaman'),
(68, '13.09', 'Kab. Kepulauan Mentawai'),
(69, '13.10', 'Kab. Dharmasraya'),
(70, '13.11', 'Kab. Solok Selatan'),
(71, '13.12', 'Kab. Pasaman Barat'),
(72, '13.71', 'Kota Padang'),
(73, '13.72', 'Kota Solok'),
(74, '13.73', 'Kota Sawahlunto'),
(75, '13.74', 'Kota Padang Panjang'),
(76, '13.75', 'Kota Bukittinggi'),
(77, '13.76', 'Kota Payakumbuh'),
(78, '13.77', 'Kota Pariaman'),
(79, '14', 'Riau'),
(80, '14.01', 'Kab. Kampar'),
(81, '14.02', 'Kab. Indragiri Hulu'),
(82, '14.03', 'Kab. Bengkalis'),
(83, '14.04', 'Kab. Indragiri Hilir'),
(84, '14.05', 'Kab. Pelalawan'),
(85, '14.06', 'Kab. Rokan Hulu'),
(86, '14.07', 'Kab. Rokan Hilir'),
(87, '14.08', 'Kab. Siak'),
(88, '14.09', 'Kab. Kuantan Singingi'),
(89, '14.10', 'Kab. Kepulauan Meranti'),
(90, '14.71', 'Kota Pekanbaru'),
(91, '14.72', 'Kota Dumai'),
(92, '15', 'Jambi'),
(93, '15.01', 'Kab. Kerinci'),
(94, '15.02', 'Kab. Merangin'),
(95, '15.03', 'Kab. Sarolangun'),
(96, '15.04', 'Kab. Batanghari'),
(97, '15.05', 'Kab. Muaro Jambi'),
(98, '15.06', 'Kab. Tanjung Jabung Barat'),
(99, '15.07', 'Kab. Tanjung Jabung Timur'),
(100, '15.08', 'Kab. Bungo'),
(101, '15.09', 'Kab. Tebo'),
(102, '15.71', 'Kota Jambi'),
(103, '15.72', 'Kota Sungai Penuh'),
(104, '16', 'Sumatera Selatan'),
(105, '16.01', 'Kab. Ogan Komering Ulu'),
(106, '16.02', 'Kab. Ogan Komering Ilir'),
(107, '16.03', 'Kab. Muara Enim'),
(108, '16.04', 'Kab. Lahat'),
(109, '16.05', 'Kab. Musi Rawas'),
(110, '16.06', 'Kab. Musi Banyuasin'),
(111, '16.07', 'Kab. Banyuasin'),
(112, '16.08', 'Kab. Ogan Komering Ulu Timur'),
(113, '16.09', 'Kab. Ogan Komering Ulu Selatan'),
(114, '16.10', 'Kab. Ogan Ilir'),
(115, '16.11', 'Kab. Empat Lawang'),
(116, '16.12', 'Kab. Penukal Abab Lematang Ilir'),
(117, '16.13', 'Kab. Musi Rawas Utara'),
(118, '16.71', 'Kota Palembang'),
(119, '16.72', 'Kota Pagar Alam'),
(120, '16.73', 'Kota Lubuk Linggau'),
(121, '16.74', 'Kota Prabumulih'),
(122, '17', 'Bengkulu'),
(123, '17.01', 'Kab. Bengkulu Selatan'),
(124, '17.02', 'Kab. Rejang Lebong'),
(125, '17.03', 'Kab. Bengkulu Utara'),
(126, '17.04', 'Kab. Kaur'),
(127, '17.05', 'Kab. Seluma'),
(128, '17.06', 'Kab. Muko Muko'),
(129, '17.07', 'Kab. Lebong'),
(130, '17.08', 'Kab. Kepahiang'),
(131, '17.09', 'Kab. Bengkulu Tengah'),
(132, '17.71', 'Kota Bengkulu'),
(133, '18', 'Lampung'),
(134, '18.01', 'Kab. Lampung Selatan'),
(135, '18.02', 'Kab. Lampung Tengah'),
(136, '18.03', 'Kab. Lampung Utara'),
(137, '18.04', 'Kab. Lampung Barat'),
(138, '18.05', 'Kab. Tulang Bawang'),
(139, '18.06', 'Kab. Tanggamus'),
(140, '18.07', 'Kab. Lampung Timur'),
(141, '18.08', 'Kab. Way Kanan'),
(142, '18.09', 'Kab. Pesawaran'),
(143, '18.10', 'Kab. Pringsewu'),
(144, '18.11', 'Kab. Mesuji'),
(145, '18.12', 'Kab. Tulang Bawang Barat'),
(146, '18.13', 'Kab. Pesisir Barat'),
(147, '18.71', 'Kota Bandar Lampung'),
(148, '18.72', 'Kota Metro'),
(149, '19', 'Kepulauan Bangka Belitung'),
(150, '19.01', 'Kab. Bangka'),
(151, '19.02', 'Kab. Belitung'),
(152, '19.03', 'Kab. Bangka Selatan'),
(153, '19.04', 'Kab. Bangka Tengah'),
(154, '19.05', 'Kab. Bangka Barat'),
(155, '19.06', 'Kab. Belitung Timur'),
(156, '19.71', 'Kota Pangkal Pinang'),
(157, '21', 'Kepulauan Riau'),
(158, '21.01', 'Kab. Bintan'),
(159, '21.02', 'Kab. Karimun'),
(160, '21.03', 'Kab. Natuna'),
(161, '21.04', 'Kab. Lingga'),
(162, '21.05', 'Kab. Kepulauan Anambas'),
(163, '21.71', 'Kota Batam'),
(164, '21.72', 'Kota Tanjung Pinang'),
(165, '31', 'Dki Jakarta'),
(166, '31.01', 'Kab. Adm. Kep. Seribu'),
(167, '31.71', 'Kota Adm. Jakarta Pusat'),
(168, '31.72', 'Kota Adm. Jakarta Utara'),
(169, '31.73', 'Kota Adm. Jakarta Barat'),
(170, '31.74', 'Kota Adm. Jakarta Selatan'),
(171, '31.75', 'Kota Adm. Jakarta Timur'),
(172, '32', 'Jawa Barat'),
(173, '32.01', 'Kab. Bogor'),
(174, '32.02', 'Kab. Sukabumi'),
(175, '32.03', 'Kab. Cianjur'),
(176, '32.04', 'Kab. Bandung'),
(177, '32.05', 'Kab. Garut'),
(178, '32.06', 'Kab. Tasikmalaya'),
(179, '32.07', 'Kab. Ciamis'),
(180, '32.08', 'Kab. Kuningan'),
(181, '32.09', 'Kab. Cirebon'),
(182, '32.10', 'Kab. Majalengka'),
(183, '32.11', 'Kab. Sumedang'),
(184, '32.12', 'Kab. Indramayu'),
(185, '32.13', 'Kab. Subang'),
(186, '32.14', 'Kab. Purwakarta'),
(187, '32.15', 'Kab. Karawang'),
(188, '32.16', 'Kab. Bekasi'),
(189, '32.17', 'Kab. Bandung Barat'),
(190, '32.18', 'Kab. Pangandaran'),
(191, '32.71', 'Kota Bogor'),
(192, '32.72', 'Kota Sukabumi'),
(193, '32.73', 'Kota Bandung'),
(194, '32.74', 'Kota Cirebon'),
(195, '32.75', 'Kota Bekasi'),
(196, '32.76', 'Kota Depok'),
(197, '32.77', 'Kota Cimahi'),
(198, '32.78', 'Kota Tasikmalaya'),
(199, '32.79', 'Kota Banjar'),
(200, '33', 'Jawa Tengah'),
(201, '33.01', 'Kab. Cilacap'),
(202, '33.02', 'Kab. Banyumas'),
(203, '33.03', 'Kab. Purbalingga'),
(204, '33.04', 'Kab. Banjarnegara'),
(205, '33.05', 'Kab. Kebumen'),
(206, '33.06', 'Kab. Purworejo'),
(207, '33.07', 'Kab. Wonosobo'),
(208, '33.08', 'Kab. Magelang'),
(209, '33.09', 'Kab. Boyolali'),
(210, '33.10', 'Kab. Klaten'),
(211, '33.11', 'Kab. Sukoharjo'),
(212, '33.12', 'Kab. Wonogiri'),
(213, '33.13', 'Kab. Karanganyar'),
(214, '33.14', 'Kab. Sragen'),
(215, '33.15', 'Kab. Grobogan'),
(216, '33.16', 'Kab. Blora'),
(217, '33.17', 'Kab. Rembang'),
(218, '33.18', 'Kab. Pati'),
(219, '33.19', 'Kab. Kudus'),
(220, '33.20', 'Kab. Jepara'),
(221, '33.21', 'Kab. Demak'),
(222, '33.22', 'Kab. Semarang'),
(223, '33.23', 'Kab. Temanggung'),
(224, '33.24', 'Kab. Kendal'),
(225, '33.25', 'Kab. Batang'),
(226, '33.26', 'Kab. Pekalongan'),
(227, '33.27', 'Kab. Pemalang'),
(228, '33.28', 'Kab. Tegal'),
(229, '33.29', 'Kab. Brebes'),
(230, '33.71', 'Kota Magelang'),
(231, '33.72', 'Kota Surakarta'),
(232, '33.73', 'Kota Salatiga'),
(233, '33.74', 'Kota Semarang'),
(234, '33.75', 'Kota Pekalongan'),
(235, '33.76', 'Kota Tegal'),
(236, '34', 'Daerah Istimewa Yogyakarta'),
(237, '34.01', 'Kab. Kulon Progo'),
(238, '34.02', 'Kab. Bantul'),
(239, '34.03', 'Kab. Gunungkidul'),
(240, '34.04', 'Kab. Sleman'),
(241, '34.71', 'Kota Yogyakarta'),
(242, '35', 'Jawa Timur'),
(243, '35.01', 'Kab. Pacitan'),
(244, '35.02', 'Kab. Ponorogo'),
(245, '35.03', 'Kab. Trenggalek'),
(246, '35.04', 'Kab. Tulungagung'),
(247, '35.05', 'Kab. Blitar'),
(248, '35.06', 'Kab. Kediri'),
(249, '35.07', 'Kab. Malang'),
(250, '35.08', 'Kab. Lumajang'),
(251, '35.09', 'Kab. Jember'),
(252, '35.10', 'Kab. Banyuwangi'),
(253, '35.11', 'Kab. Bondowoso'),
(254, '35.12', 'Kab. Situbondo'),
(255, '35.13', 'Kab. Probolinggo'),
(256, '35.14', 'Kab. Pasuruan'),
(257, '35.15', 'Kab. Sidoarjo'),
(258, '35.16', 'Kab. Mojokerto'),
(259, '35.17', 'Kab. Jombang'),
(260, '35.18', 'Kab. Nganjuk'),
(261, '35.19', 'Kab. Madiun'),
(262, '35.20', 'Kab. Magetan'),
(263, '35.21', 'Kab. Ngawi'),
(264, '35.22', 'Kab. Bojonegoro'),
(265, '35.23', 'Kab. Tuban'),
(266, '35.24', 'Kab. Lamongan'),
(267, '35.25', 'Kab. Gresik'),
(268, '35.26', 'Kab. Bangkalan'),
(269, '35.27', 'Kab. Sampang'),
(270, '35.28', 'Kab. Pamekasan'),
(271, '35.29', 'Kab. Sumenep'),
(272, '35.71', 'Kota Kediri'),
(273, '35.72', 'Kota Blitar'),
(274, '35.73', 'Kota Malang'),
(275, '35.74', 'Kota Probolinggo'),
(276, '35.75', 'Kota Pasuruan'),
(277, '35.76', 'Kota Mojokerto'),
(278, '35.77', 'Kota Madiun'),
(279, '35.78', 'Kota Surabaya'),
(280, '35.79', 'Kota Batu'),
(281, '36', 'Banten'),
(282, '36.01', 'Kab. Pandeglang'),
(283, '36.02', 'Kab. Lebak'),
(284, '36.03', 'Kab. Tangerang'),
(285, '36.04', 'Kab. Serang'),
(286, '36.71', 'Kota Tangerang'),
(287, '36.72', 'Kota Cilegon'),
(288, '36.73', 'Kota Serang'),
(289, '36.74', 'Kota Tangerang Selatan'),
(290, '51', 'Bali'),
(291, '51.01', 'Kab. Jembrana'),
(292, '51.02', 'Kab. Tabanan'),
(293, '51.03', 'Kab. Badung'),
(294, '51.04', 'Kab. Gianyar'),
(295, '51.05', 'Kab. Klungkung'),
(296, '51.06', 'Kab. Bangli'),
(297, '51.07', 'Kab. Karangasem'),
(298, '51.08', 'Kab. Buleleng'),
(299, '51.71', 'Kota Denpasar'),
(300, '52', 'Nusa Tenggara Barat'),
(301, '52.01', 'Kab. Lombok Barat'),
(302, '52.02', 'Kab. Lombok Tengah'),
(303, '52.03', 'Kab. Lombok Timur'),
(304, '52.04', 'Kab. Sumbawa'),
(305, '52.05', 'Kab. Dompu'),
(306, '52.06', 'Kab. Bima'),
(307, '52.07', 'Kab. Sumbawa Barat'),
(308, '52.08', 'Kab. Lombok Utara'),
(309, '52.71', 'Kota Mataram'),
(310, '52.72', 'Kota Bima'),
(311, '53', 'Nusa Tenggara Timur'),
(312, '53.01', 'Kab. Kupang'),
(313, '53.02', 'Kab Timor Tengah Selatan'),
(314, '53.03', 'Kab. Timor Tengah Utara'),
(315, '53.04', 'Kab. Belu'),
(316, '53.05', 'Kab. Alor'),
(317, '53.06', 'Kab. Flores Timur'),
(318, '53.07', 'Kab. Sikka'),
(319, '53.08', 'Kab. Ende'),
(320, '53.09', 'Kab. Ngada'),
(321, '53.10', 'Kab. Manggarai'),
(322, '53.11', 'Kab. Sumba Timur'),
(323, '53.12', 'Kab. Sumba Barat'),
(324, '53.13', 'Kab. Lembata'),
(325, '53.14', 'Kab. Rote Ndao'),
(326, '53.15', 'Kab. Manggarai Barat'),
(327, '53.16', 'Kab. Nagekeo'),
(328, '53.17', 'Kab. Sumba Tengah'),
(329, '53.18', 'Kab. Sumba Barat Daya'),
(330, '53.19', 'Kab. Manggarai Timur'),
(331, '53.20', 'Kab. Sabu Raijua'),
(332, '53.21', 'Kab. Malaka'),
(333, '53.71', 'Kota Kupang'),
(334, '61', 'Kalimantan Barat'),
(335, '61.01', 'Kab. Sambas'),
(336, '61.02', 'Kab. Mempawah'),
(337, '61.03', 'Kab. Sanggau'),
(338, '61.04', 'Kab. Ketapang'),
(339, '61.05', 'Kab. Sintang'),
(340, '61.06', 'Kab. Kapuas Hulu'),
(341, '61.07', 'Kab. Bengkayang'),
(342, '61.08', 'Kab. Landak'),
(343, '61.09', 'Kab. Sekadau'),
(344, '61.10', 'Kab. Melawi'),
(345, '61.11', 'Kab. Kayong Utara'),
(346, '61.12', 'Kab. Kubu Raya'),
(347, '61.71', 'Kota Pontianak'),
(348, '61.72', 'Kota Singkawang'),
(349, '92', 'Papua Barat'),
(350, '92.01', 'Kab. Sorong'),
(351, '92.02', 'Kab. Manokwari'),
(352, '92.03', 'Kab. Fak Fak'),
(353, '92.04', 'Kab. Sorong Selatan'),
(354, '92.05', 'Kab. Raja Ampat'),
(355, '92.06', 'Kab. Teluk Bintuni'),
(356, '92.07', 'Kab. Teluk Wondama'),
(357, '92.08', 'Kab. Kaimana'),
(358, '92.09', 'Kab. Tambrauw'),
(359, '92.10', 'Kab. Maybrat'),
(360, '92.11', 'Kab. Manokwari Selatan'),
(361, '92.12', 'Kab. Pegunungan Arfak'),
(362, '92.71', 'Kota Sorong'),
(363, '62', 'Kalimantan Tengah'),
(364, '62.01', 'Kab. Kotawaringin Barat'),
(365, '62.02', 'Kab. Kotawaringin Timur'),
(366, '62.03', 'Kab. Kapuas'),
(367, '62.04', 'Kab. Barito Selatan'),
(368, '62.05', 'Kab. Barito Utara'),
(369, '62.06', 'Kab. Katingan'),
(370, '62.07', 'Kab. Seruyan'),
(371, '62.08', 'Kab. Sukamara'),
(372, '62.09', 'Kab. Lamandau'),
(373, '62.10', 'Kab. Gunung Mas'),
(374, '62.11', 'Kab. Pulang Pisau'),
(375, '62.12', 'Kab. Murung Raya'),
(376, '62.13', 'Kab. Barito Timur'),
(377, '62.71', 'Kota Palangkaraya'),
(378, '63', 'Kalimantan Selatan'),
(379, '63.01', 'Kab. Tanah Laut'),
(380, '63.02', 'Kab. Kotabaru'),
(381, '63.03', 'Kab. Banjar'),
(382, '63.04', 'Kab. Barito Kuala'),
(383, '63.05', 'Kab. Tapin'),
(384, '63.06', 'Kab. Hulu Sungai Selatan'),
(385, '63.07', 'Kab. Hulu Sungai Tengah'),
(386, '63.08', 'Kab. Hulu Sungai Utara'),
(387, '63.09', 'Kab. Tabalong'),
(388, '63.10', 'Kab. Tanah Bumbu'),
(389, '63.11', 'Kab. Balangan'),
(390, '63.71', 'Kota Banjarmasin'),
(391, '63.72', 'Kota Banjarbaru'),
(392, '64', 'Kalimantan Timur'),
(393, '64.01', 'Kab. Paser'),
(394, '64.02', 'Kab. Kutai Kartanegara'),
(395, '64.03', 'Kab. Berau'),
(396, '64.07', 'Kab. Kutai Barat'),
(397, '64.08', 'Kab. Kutai Timur'),
(398, '64.09', 'Kab. Penajam Paser Utara'),
(399, '64.11', 'Kab. Mahakam Ulu'),
(400, '64.71', 'Kota Balikpapan'),
(401, '64.72', 'Kota Samarinda'),
(402, '64.74', 'Kota Bontang'),
(403, '65', 'Kalimantan Utara'),
(404, '65.01', 'Kab. Bulungan'),
(405, '65.02', 'Kab. Malinau'),
(406, '65.03', 'Kab. Nunukan'),
(407, '65.04', 'Kab. Tana Tidung'),
(408, '65.71', 'Kota Tarakan'),
(409, '71', 'Sulawesi Utara'),
(410, '71.01', 'Kab. Bolaang Mongondow'),
(411, '71.02', 'Kab. Minahasa'),
(412, '71.03', 'Kab. Kepulauan Sangihe'),
(413, '71.04', 'Kab. Kepulauan Talaud'),
(414, '71.05', 'Kab. Minahasa Selatan'),
(415, '71.06', 'Kab. Minahasa Utara'),
(416, '71.07', 'Kab. Minahasa Tenggara'),
(417, '71.08', 'Kab. Bolaang Mongondow Utara'),
(418, '71.09', 'Kab. Kep. Siau Tagulandang Biaro'),
(419, '71.10', 'Kab. Bolaang Mongondow Timur'),
(420, '71.11', 'Kab. Bolaang Mongondow Selatan'),
(421, '71.71', 'Kota Manado'),
(422, '71.72', 'Kota Bitung'),
(423, '71.73', 'Kota Tomohon'),
(424, '71.74', 'Kota Kotamobagu'),
(425, '72', 'Sulawesi Tengah'),
(426, '72.01', 'Kab. Banggai'),
(427, '72.02', 'Kab. Poso'),
(428, '72.03', 'Kab. Donggala'),
(429, '72.04', 'Kab. Toli Toli'),
(430, '72.05', 'Kab. Buol'),
(431, '72.06', 'Kab. Morowali'),
(432, '72.07', 'Kab. Banggai Kepulauan'),
(433, '72.08', 'Kab. Parigi Moutong'),
(434, '72.09', 'Kab. Tojo Una Una'),
(435, '72.10', 'Kab. Sigi'),
(436, '72.11', 'Kab. Banggai Laut'),
(437, '72.12', 'Kab. Morowali Utara'),
(438, '72.71', 'Kota Palu'),
(439, '73', 'Sulawesi Selatan'),
(440, '73.01', 'Kab. Kepulauan Selayar'),
(441, '73.02', 'Kab. Bulukumba'),
(442, '73.03', 'Kab. Bantaeng'),
(443, '73.04', 'Kab. Jeneponto'),
(444, '73.05', 'Kab. Takalar'),
(445, '73.06', 'Kab. Gowa'),
(446, '73.07', 'Kab. Sinjai'),
(447, '73.08', 'Kab. Bone'),
(448, '73.09', 'Kab. Maros'),
(449, '73.10', 'Kab. Pangkajene Kepulauan'),
(450, '73.11', 'Kab. Barru'),
(451, '73.12', 'Kab. Soppeng'),
(452, '73.13', 'Kab. Wajo'),
(453, '73.14', 'Kab. Sidenreng Rappang'),
(454, '73.15', 'Kab. Pinrang'),
(455, '73.16', 'Kab. Enrekang'),
(456, '73.17', 'Kab. Luwu'),
(457, '73.18', 'Kab. Tana Toraja'),
(458, '73.22', 'Kab. Luwu Utara'),
(459, '73.24', 'Kab. Luwu Timur'),
(460, '73.26', 'Kab. Toraja Utara'),
(461, '73.71', 'Kota Makassar'),
(462, '73.72', 'Kota Pare Pare'),
(463, '73.73', 'Kota Palopo'),
(464, '74', 'Sulawesi Tenggara'),
(465, '74.01', 'Kab. Kolaka'),
(466, '74.02', 'Kab. Konawe'),
(467, '74.03', 'Kab. Muna'),
(468, '74.04', 'Kab. Buton'),
(469, '74.05', 'Kab. Konawe Selatan'),
(470, '74.06', 'Kab. Bombana'),
(471, '74.07', 'Kab. Wakatobi'),
(472, '74.08', 'Kab. Kolaka Utara'),
(473, '74.09', 'Kab. Konawe Utara'),
(474, '74.10', 'Kab. Buton Utara'),
(475, '74.11', 'Kab. Kolaka Timur'),
(476, '74.12', 'Kab. Konawe Kepulauan'),
(477, '74.13', 'Kab. Muna Barat'),
(478, '74.14', 'Kab. Buton Tengah'),
(479, '74.15', 'Kab. Buton Selatan'),
(480, '74.71', 'Kota Kendari'),
(481, '74.72', 'Kota Bau Bau'),
(482, '75', 'Gorontalo'),
(483, '75.01', 'Kab. Gorontalo'),
(484, '75.02', 'Kab. Boalemo'),
(485, '75.03', 'Kab. Bone Bolango'),
(486, '75.04', 'Kab. Pahuwato'),
(487, '75.05', 'Kab. Gorontalo Utara'),
(488, '75.71', 'Kota Gorontalo'),
(489, '76', 'Sulawesi Barat'),
(490, '76.01', 'Kab. Mamuju Utara'),
(491, '76.02', 'Kab. Mamuju'),
(492, '76.03', 'Kab. Mamasa'),
(493, '76.04', 'Kab. Polewali Mandar'),
(494, '76.05', 'Kab. Majene'),
(495, '76.06', 'Kab. Mamuju Tengah'),
(496, '81', 'Maluku'),
(497, '81.01', 'Kab. Maluku Tengah'),
(498, '81.02', 'Kab. Maluku Tenggara'),
(499, '81.03', 'Kab. Maluku Tenggara Barat'),
(500, '81.04', 'Kab. Buru'),
(501, '81.05', 'Kab. Seram Bagian Timur'),
(502, '81.06', 'Kab. Seram Bagian Barat'),
(503, '81.07', 'Kab. Kepulauan Aru'),
(504, '81.08', 'Kab. Maluku Barat Daya'),
(505, '81.09', 'Kab. Buru Selatan'),
(506, '81.71', 'Kota Ambon'),
(507, '81.72', 'Kota Tual'),
(508, '82', 'Maluku Utara'),
(509, '82.01', 'Kab. Halmahera Barat'),
(510, '82.02', 'Kab. Halmahera Tengah'),
(511, '82.03', 'Kab. Halmahera Utara'),
(512, '82.04', 'Kab. Halmahera Selatan'),
(513, '82.05', 'Kab. Kepulauan Sula'),
(514, '82.06', 'Kab. Halmahera Timur'),
(515, '82.07', 'Kab. Pulau Morotai'),
(516, '82.08', 'Kab. Pulau Taliabu'),
(517, '82.71', 'Kota Ternate'),
(518, '82.72', 'Kota Tidore Kepulauan'),
(519, '91', 'P A P U A'),
(520, '91.01', 'Kab. Merauke'),
(521, '91.02', 'Kab. Jayawijaya'),
(522, '91.03', 'Kab. Jayapura'),
(523, '91.04', 'Kab. Nabire'),
(524, '91.05', 'Kab. Kepulauan Yapen'),
(525, '91.06', 'Kab. Biak Numfor'),
(526, '91.07', 'Kab. Puncak Jaya'),
(527, '91.08', 'Kab. Paniai'),
(528, '91.09', 'Kab. Mimika'),
(529, '91.10', 'Kab. Sarmi'),
(530, '91.11', 'Kab. Keerom'),
(531, '91.12', 'Kab. Pegunungan Bintang'),
(532, '91.13', 'Kab. Yahukimo'),
(533, '91.14', 'Kab. Tolikara'),
(534, '91.15', 'Kab. Waropen'),
(535, '91.16', 'Kab. Boven Digoel'),
(536, '91.17', 'Kab. Mappi'),
(537, '91.18', 'Kab. Asmat'),
(538, '91.19', 'Kab. Supiori'),
(539, '91.20', 'Kab. Mamberamo Raya'),
(540, '91.21', 'Kab. Mamberamo Tengah'),
(541, '91.22', 'Kab. Yalimo'),
(542, '91.23', 'Kab. Lanny Jaya'),
(543, '91.24', 'Kab. Nduga'),
(544, '91.25', 'Kab. Puncak'),
(545, '91.26', 'Kab. Dogiyai'),
(546, '91.27', 'Kab. Intan Jaya'),
(547, '91.28', 'Kab. Deiyai'),
(548, '91.71', 'Kota Jayapura');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`no_ktp`) USING BTREE,
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `tb_data_transaksi`
--
ALTER TABLE `tb_data_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_transaksi` (`kode_transaksi`);

--
-- Indexes for table `tb_foto_kendaraan`
--
ALTER TABLE `tb_foto_kendaraan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`),
  ADD KEY `kode_rental` (`kode_kendaraan`);

--
-- Indexes for table `tb_kendaraan`
--
ALTER TABLE `tb_kendaraan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plat` (`plat`),
  ADD KEY `kode` (`kode`),
  ADD KEY `kode_rental` (`kode_rental`) USING BTREE,
  ADD KEY `kode_merk` (`kode_merk`) USING BTREE,
  ADD KEY `kode_lokasi` (`kode_lokasi`);

--
-- Indexes for table `tb_merk`
--
ALTER TABLE `tb_merk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `tb_password_resets`
--
ALTER TABLE `tb_password_resets`
  ADD KEY `email` (`email`);

--
-- Indexes for table `tb_rental`
--
ALTER TABLE `tb_rental`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `kode` (`kode`),
  ADD KEY `kode_lokasi` (`kode_lokasi`);

--
-- Indexes for table `tb_rental_admin_utama`
--
ALTER TABLE `tb_rental_admin_utama`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_user` (`kode_user`),
  ADD KEY `kode_rental` (`kode_rental`);

--
-- Indexes for table `tb_social_media`
--
ALTER TABLE `tb_social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_kendaraan` (`kode_kendaraan`),
  ADD KEY `kode_user` (`kode_user`),
  ADD KEY `kode` (`kode`),
  ADD KEY `kode_rental` (`kode_rental`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_ktp` (`no_ktp`),
  ADD KEY `kode` (`kode`),
  ADD KEY `kode_lokasi` (`kode_lokasi`);

--
-- Indexes for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_data_transaksi`
--
ALTER TABLE `tb_data_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_foto_kendaraan`
--
ALTER TABLE `tb_foto_kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_kendaraan`
--
ALTER TABLE `tb_kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_merk`
--
ALTER TABLE `tb_merk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_rental`
--
ALTER TABLE `tb_rental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_rental_admin_utama`
--
ALTER TABLE `tb_rental_admin_utama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_social_media`
--
ALTER TABLE `tb_social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=549;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_data_transaksi`
--
ALTER TABLE `tb_data_transaksi`
  ADD CONSTRAINT `tb_data_transaksi_ibfk_1` FOREIGN KEY (`kode_transaksi`) REFERENCES `tb_transaksi` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_foto_kendaraan`
--
ALTER TABLE `tb_foto_kendaraan`
  ADD CONSTRAINT `tb_foto_kendaraan_ibfk_1` FOREIGN KEY (`kode_kendaraan`) REFERENCES `tb_kendaraan` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_kendaraan`
--
ALTER TABLE `tb_kendaraan`
  ADD CONSTRAINT `tb_kendaraan_ibfk_10` FOREIGN KEY (`kode_rental`) REFERENCES `tb_rental` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kendaraan_ibfk_11` FOREIGN KEY (`kode_lokasi`) REFERENCES `tb_wilayah` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kendaraan_ibfk_9` FOREIGN KEY (`kode_merk`) REFERENCES `tb_merk` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_rental`
--
ALTER TABLE `tb_rental`
  ADD CONSTRAINT `tb_rental_ibfk_1` FOREIGN KEY (`kode_lokasi`) REFERENCES `tb_wilayah` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_rental_admin_utama`
--
ALTER TABLE `tb_rental_admin_utama`
  ADD CONSTRAINT `tb_rental_admin_utama_ibfk_1` FOREIGN KEY (`kode_rental`) REFERENCES `tb_rental` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_rental_admin_utama_ibfk_2` FOREIGN KEY (`kode_user`) REFERENCES `tb_user` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD CONSTRAINT `tb_transaksi_ibfk_2` FOREIGN KEY (`kode_kendaraan`) REFERENCES `tb_kendaraan` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_transaksi_ibfk_3` FOREIGN KEY (`kode_user`) REFERENCES `tb_user` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_transaksi_ibfk_4` FOREIGN KEY (`kode_rental`) REFERENCES `tb_rental` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`kode_lokasi`) REFERENCES `tb_wilayah` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
