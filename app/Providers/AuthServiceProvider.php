<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('as_master_admin', function($user) {
            return $user->role == 'master_admin';
        });
        Gate::define('as_administrator', function($user) {
            return $user->role == 'administrator';
        });
        Gate::define('as_customer_service', function($user) {
            return $user->role == 'customer_service';
        });
    }
}
