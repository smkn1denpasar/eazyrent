<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    public $timestamps = false;
    protected $table = 'tb_wilayah';
    protected $guarded = [];
    protected $primaryKey = 'kode';
    public $incrementing = false;
}
