<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataTransaksi extends Model
{
    public $timestamps = false;
    protected $table = 'tb_data_transaksi';
    protected $guarded = [];
}