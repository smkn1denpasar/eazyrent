<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaporanKerusakan extends Model
{
    public $timestamps = false;
    protected $table = 'tb_laporan_kerusakan';
    protected $guarded = [];

    public function Transaksi() {
        return $this->hasOne(Transaksi::class, 'kode', 'kode_transaksi');
    }
}
