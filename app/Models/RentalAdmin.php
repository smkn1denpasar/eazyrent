<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentalAdmin extends Model
{
    public $timestamps = false;
    protected $table = 'tb_rental_admin';
    protected $guarded = [];

    public function DetailRental() {
        return $this->hasOne(Rental::class, 'kode', 'kode_rental');
    }

    public function DetailUser() {
        return $this->hasOne(User::class, 'kode', 'kode_user');
    }
}
