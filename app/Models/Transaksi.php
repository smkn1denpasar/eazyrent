<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public $timestamps = false;
    protected $table = 'tb_transaksi';
    protected $guarded = [];
    
    // public function Pelanggan() {
    //     return $this->hasOne(Pelanggan::class, 'id', 'id_pelanggan');
    // }
    // public function Kendaraan() {
    //     return $this->hasOne(Kendaraan::class, 'id', 'id_kendaraan');
    // }
    // public function Sopir() {
    //     return $this->hasOne(Sopir::class, 'id', 'id_sopir');
    // }
}
