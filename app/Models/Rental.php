<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    public $timestamps = false;
    protected $table = 'tb_rental';
    protected $guarded = [];

    public function Detail() {
        return $this->belongsTo(DetailRental::class, 'kode_rental_utama', 'kode');
    }

    public function Lokasi() {
        return $this->belongsTo(Wilayah::class, 'kode_lokasi', 'kode');
    }

    public function Kendaraan() {
        return $this->hasMany(Kendaraan::class, 'kode_rental', 'kode');
    }

    public function Admin() {
        return $this->hasMany(RentalAdmin::class, 'kode_rental', 'kode');
    }

    // public function AdminUtama() {
    //     return $this->hasOne(User::class, 'kode_rental_admin_utama', 'kode');
    // }

    public static function roleList() {
        return [
            'rental' => 'Rental',
            'cabang_rental' => 'Cabang Rental'
        ];
    }
    // public function setRoleAttribute() {
    //     return static::roleList()[$this->role];
    // }
}
