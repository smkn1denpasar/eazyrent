<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    public $timestamps = false;
    protected $table = 'tb_foto_kendaraan';
    protected $guarded = [];
}
