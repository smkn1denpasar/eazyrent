<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    public $timestamps = false;
    protected $table = 'tb_karyawan';
    protected $guarded = [];

    public function User() {
        return $this->belongsTo(User::class, 'id_karyawan', 'id_karyawan');
    }
}