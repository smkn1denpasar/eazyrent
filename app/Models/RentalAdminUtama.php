<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentalAdminUtama extends Model
{
    public $timestamps = false;
    protected $table = 'tb_rental_admin_utama';
    protected $guarded = [];

    public function DetailRental() {
        return $this->hasOne(DetailRental::class, 'kode', 'kode_rental');
    }

    public function DetailUser() {
        return $this->hasOne(User::class, 'kode', 'kode_user');
    }
}
