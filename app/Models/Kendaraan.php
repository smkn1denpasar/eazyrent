<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    public $timestamps = false;
    protected $table = 'tb_kendaraan';
    protected $guarded = [];

    public function Pemilik()
    {
        return $this->hasOne(Pemilik::class, 'id', 'pemilik_id');
    }
    public function Merk()
    {
        return $this->hasOne(Merk::class, 'kode', 'kode_merk');
    }
    public function Rental() {
        return $this->hasOne(Rental::class, 'kode', 'kode_rental');
    }
    // public function RentalUtama() {
    //     return $this->hasOne(DetailRental::class, 'kode', 'kode_rental');
    // }

    public static function transmisiList()
    {
        return [
          'manual' => 'Manual',
          'matic' => 'Matic',
        ];
    }

    public static function statusList()
    {
        return [
          'tersedia' => 'Tersedia',
          'digunakan' => 'Digunakan',
          'service' => 'Service',
        ];
    }

    public function getHumanTarifAttribute()
    {
        return "Rp. " . number_format($this->tarif, 0, ',', '.');
    }

    public function getFullNameKendaraanAttribute()
    {
        return "{$this->merk->nama} {$this->jenis}";
    }

    public function getHumanTransmisiAttribute()
    {
        return static::transmisiList()[$this->transmisi];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedTransmisi()
    {
        return array_keys(static::transmisiList());
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }
}
