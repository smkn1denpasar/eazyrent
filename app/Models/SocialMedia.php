<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table = "tb_social_media";
    public $timestamps = false;
    protected $guarded = [];

    public static function socialMediaList()
    {
        return [
          'facebook' => 'Facebook',
          'instagram' => 'Instagram',
          'twitter' => 'Twitter',
          'whatsapp' => 'Whatsapp',
        ];
    }

    public static function contactList()
    {
        return [
            'email' => 'Email',
            'no_telp' => 'No Telepon'
        ];
    }

    public static function allowedContact()
    {
        return implode(",", array_keys(static::contactList()));
    }

    public static function allowedSocialMedia()
    {
        return implode(",", array_keys(static::socialMediaList()));
    }

    public function getRealContactAttribute()
    {
        return static::contactList()[$this->type];
    }

    public function getRealSocialAttribute()
    {
        return static::socialMediaList()[$this->type];
    }
}
