<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
    public $timestamps = false;
    protected $table = 'tb_pemilik';
    protected $guarded = [];

    public function Kendaraan() {
        return $this->hasMany(Kendaraan::class, 'pemilik_id', 'id');
    }
}
