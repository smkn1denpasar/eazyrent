<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    public $timestamps = false;
    protected $table = 'tb_pelanggan';
    protected $guarded = [];

    public function Transaksi() {
        return $this->hasMany(Transaksi::class, 'id_pelanggan', 'id');
    }
}
