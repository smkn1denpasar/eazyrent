<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    public $timestamps = false;
    protected $table = 'tb_merk';
    protected $guarded = [];

    public function Kendaraan() {
        return $this->hasMany(Kendaraan::class, 'merk_id', 'id');
    }
}
