<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {
    
    protected $table = "tb_admin";
    protected $guarded = [];
    public $timestamps = false;
    protected $guard = 'admin';
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function karyawan() {
        return $this->hasOne(Karyawan::class, 'id_karyawan', 'id_karyawan');
    }
    public static function roleList() {
        return [
            'master_admin' => 'Master Admin',
            'administrator' => 'Administrator',
            'customer_service' => 'Customer Service',
        ];
    }
    public static function genderList() {
        return [
            'laki-laki' => 'Laki-laki',
            'perempuan' => 'Perempuan',
        ];
    }
    public function getHumanRoleAttribute() {
        return static::roleList()[$this->role];
    }
    public function getHumanGenderAttribute() {
        return static::genderList()[$this->jenis_kelamin];
    }
    public static function allowedRole() {
        return array_keys(static::roleList());
    }
    public static function allowedGender() {
        return array_keys(static::genderList());
    }
}
