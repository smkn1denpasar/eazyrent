<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "tb_user";
    protected $guarded = [];
    public $timestamps = false;
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Rental() {
        return $this->hasOne(RentalAdmin::class, 'kode_user', 'kode');
    }
}
