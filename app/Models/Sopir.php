<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sopir extends Model
{
    public $timestamps = false;
    protected $table = 'tb_sopir';
    protected $guarded = [];
}
