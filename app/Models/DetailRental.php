<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailRental extends Model
{
    public $timestamps = false;
    protected $table = 'tb_rental_detail';
    protected $guarded = [];

    public function RentalUtama() {
        return $this->hasOne(Rental::class, 'kode_rental_utama', 'kode');
    }

    public function Rental() {
        return $this->hasMany(Rental::class, 'kode_rental_utama', 'kode');
    }

    public function Pemilik() {
        return $this->hasOne(RentalAdminUtama::class, 'kode_rental', 'kode');
    }
}
