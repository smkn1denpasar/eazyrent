<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Merk;
use App\Models\Rental;
use App\Service\Upload;
use App\Service\Random;
use App\Models\RentalAdminUtama;

class KendaraanRentalController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            $rental = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            if(\Request::route()->getName() != 'rental.register' && !$rental) {
                return redirect()->route('rental.register');
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $kendaraan = Kendaraan::where('kode_rental', $data->kode_rental)->paginate(5);
        return view('user.rental.car.index', compact('kendaraan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merk = Merk::pluck('nama', 'kode');
        $transmisi = Kendaraan::transmisiList();
        $bahan_bakar = Kendaraan::bahanBakariList();
        return view('user.rental.car.add', compact('merk', 'transmisi', 'bahan_bakar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();

        $this->validate($request, [
            'jenis' => 'required|max:50',
            'kode_merk' => 'required|exists:tb_merk,kode',
            'deskripsi' => 'required',
            'harga' => 'required|numeric|max:5000000',
            'biaya_antar' => 'required|numeric|max:50000',
            'alamat_pengambilan' => 'required',
            'plat' => 'required',
            'foto' => 'required|min:5',
            'foto.*' => 'image|max:3000|dimensions:min_width=500,min_height=300',
            'tahun' => 'required|numeric',
            'total_km' => 'required|numeric',
            'bahan_bakar' => 'required|in:' . implode(",", Kendaraan::allowedBahanBakar()),
            'max_fuel' => 'required|numeric',
            'seat' => 'required|numeric|min:2|max:10',
            'transmisi' => 'required|in:' . implode(",", Kendaraan::allowedTransmisi()),
        ]);

        $upload = new Upload();
        $foto = $request->file('foto');
        $rental = Rental::where('kode', $data->kode_rental)->firstOrFail();
        $name = $rental->nama . $request->jenis . $request->kode_merk;
        foreach($foto as $index => $val) {
            $i[$index]['nama'] = $upload->fileName($val, $name);
            $i[$index]['kode'] = Random::generate(5);
        }

        $kode = Random::generate(5);
        $total_harga = $request->harga;
        $kendaraan = $request->only('jenis', 'deskripsi', 'harga', 'tahun', 'total_km', 'plat', 'biaya_antar', 'alamat_pengambilan', 'bahan_bakar', 'max_fuel', 'seat', 'transmisi', 'kode_merk') + ['kode' => $kode, 'total_harga' => $total_harga, 'kode_lokasi' => $rental->kode_lokasi];
        $kd = $rental->Kendaraan()->create($kendaraan);
        $kd->Foto()->createMany($i);

        $dir = 'images/kp';
        foreach($foto as $index => $val) {
            $upload->saveFile($val, $i[$index]['nama'], $dir);
        }

        return redirect()->route('rental.car.add')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();
        $merk = Merk::pluck('nama', 'kode');
        $transmisi = Kendaraan::transmisiList();
        $bahan_bakar = Kendaraan::bahanBakariList();
        $status = Kendaraan::statusList();
        unset($status['transaksi_pending']);

        return view('user.rental.car.edit', compact('car', 'merk', 'transmisi', 'bahan_bakar', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();

        $this->validate($request, [
            'jenis' => 'required|max:50',
            'kode_merk' => 'required|exists:tb_merk,kode',
            'deskripsi' => 'required',
            'harga' => 'required|numeric|max:5000000',
            'diskon' => 'nullable|numeric',
            'status' => 'required|in:' . implode(",", Kendaraan::allowedStatus()),
            'tahun' => 'required|numeric',
            'total_km' => 'required|numeric',
            'bahan_bakar' => 'required|in:' . implode(",", Kendaraan::allowedBahanBakar()),
            'max_fuel' => 'required|numeric',
            'seat' => 'required|numeric|min:2|max:10',
            'transmisi' => 'required|in:' . implode(",", Kendaraan::allowedTransmisi()),
        ]);

        $total_harga = $request->harga;
        if(!empty($request->diskon)) {
            $total_harga = $request->harga-(($request->diskon/100)*$request->harga);
        }

        $kendaraan = $request->only('jenis', 'deskripsi', 'harga', 'tahun', 'total_km', 'bahan_bakar', 'max_fuel', 'seat', 'transmisi', 'kode_merk', 'diskon', 'status') + ['total_harga' => $total_harga];
        $car->update($kendaraan);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('status', 'tersedia')->where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();
        $car->delete();
        $car->Foto()->delete();

        $file = new Upload();
        $dir = 'images/kp';
        foreach($car->Foto() as $index => $foto) {
            $file->deleteFile($foto, $dir);
        }

        return redirect()->route('rental.car')->with(['success' => 'Data berhasil dihapus...']);
    }
}
