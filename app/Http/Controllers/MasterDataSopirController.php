<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Sopir;

class MasterDataSopirController extends Controller
{
    public function index() {
        $data = Sopir::all();
        return view('admin.master-data.sopir.index', compact('data'));
    }
    public function view_add() {
        return view('admin.master-data.sopir.add');
    }
    public function view_edit($id) {
        $data = Sopir::where('id', $id)->firstOrFail();
        return view('admin.master-data.sopir.edit', compact('data'));
    }
    public function add(Request $request) {
        $this->validate($request, [
            'no_ktp' => 'required|numeric|digits:16|unique:tb_sopir,no_ktp',
            'no_sim' => 'required|numeric|unique:tb_sopir,no_sim',
            'nama' => 'required|max:100|alpha_cds',
            'alamat' => 'required|max:200',
            'no_tlp' => 'required|digits_between:8,13|regex:/(0)[0-9]/',
        ]);
        $req = $request->only('no_ktp', 'no_sim', 'nama', 'alamat', 'no_tlp');
        Sopir::create($req);
        return redirect()->route('master-data.sopir')->with(['success' => 'Data berhasil ditambahkan...']);
    }
    public function edit(Request $request, $id) {
        $data = Sopir::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'no_sim' => ['required', 'numeric', Rule::unique('tb_sopir')->ignore($data->no_sim, 'no_sim')],
            'alamat' => 'required|max:200',
            'no_tlp' => 'required|digits_between:8,13|regex:/(0)[0-9]/',
            'status' => 'required|in:1,2'
        ]);
        $req = $request->only('no_sim', 'alamat', 'no_tlp', 'status');
        $data->update($req);
        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }
    public function delete($id) {
        $data = Sopir::where('id', $id)->firstOrFail();
        if($data->status == 2) {
            return redirect()->back()->with(['error' => 'Gagal menghapus data...<br>Sopir sedang dalam pekerjaan...']);
        } else {
            $data->delete();
            return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
        }
    }
}