<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Transaksi;
use App\Models\RentalAdminUtama;

class RentalCheckKondisi extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            $rental = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            if(\Request::route()->getName() != 'rental.register' && !$rental) {
                return redirect()->route('rental.register');
            }
            return $next($request);
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::where('status', 'digunakan')->where('kode', $kode)->firstOrFail();

        return view('user.rental.status-penyewaan.check-kondisi.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::where('status', 'digunakan')->where('kode', $kode)->firstOrFail();
        $kendaraan = Kendaraan::where('kode', $data->kode_kendaraan)->firstOrFail();

        $this->validate($request, [
            'total_km' => 'required|numeric|gt:' . $kendaraan->total_km,
            'deskripsi_kerusakan' => 'required'
        ]);

        $total_km = array('total_km' => ($request->total_km-$kendaraan->total_km));
        $status_transaksi = array('status' => 'selesai');
        $status_kendaraan = array('status' => 'tersedia');
        $deskripsi_kerusakan = $request->only('deskripsi_kerusakan');

        $req = $total_km + $status_transaksi;

        $data->update($req);
        $kendaraan->update($status_kendaraan);
        $data->LaporanKerusakan()->create($deskripsi_kerusakan);

        return redirect()->route('rental.status-penyewaan')->with(['success' => 'Status berhasil diubah...']);
    }
}
