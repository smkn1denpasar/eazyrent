<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Service\Upload;
use App\Service\Random;
use App\Models\Kendaraan;
use App\Models\Foto;
use App\Models\Rental;
use App\Models\RentalAdminUtama;

class FotoKendaraanRentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();
        $foto = Foto::where('kode_kendaraan', $kode)->paginate(3);

        return view('user.rental.car.foto.index', compact('foto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();
        return view('user.rental.car.foto.add', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $kode)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();

        $this->validate($request, [
            'foto' => 'required',
            'foto.*' => 'image|max:3000|dimensions:min_width=0,min_height=0',
        ]);
        $upload = new Upload();
        $foto = $request->file('foto');
        $rental = Rental::where('kode', $data->kode_rental)->firstOrFail();
        $name = $rental->nama . $car->jenis . $car->kode_merk;
        foreach($foto as $index => $val) {
            $i[$index]['nama'] = $upload->fileName($val, $name);
            $i[$index]['kode'] = Random::generate(5);
        }

        $car->Foto()->createMany($i);

        $dir = 'images/kp';
        foreach($foto as $index => $val) {
            $upload->saveFile($val, $i[$index]['nama'], $dir);
        }
        return redirect()->route('rental.car.foto', $car->kode)->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode, $kodeFoto)
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $car = Kendaraan::where('kode_rental', $data->kode_rental)->where('kode', $kode)->firstOrFail();
        $foto = Foto::where('kode_kendaraan', $kode)->where('kode', $kodeFoto)->firstOrFail();

        $check = Foto::where('kode_kendaraan', $kode)->count();
        if($check <= 5) {
            return redirect()->back()->with(['danger' => 'Gagal mengapus data. Minimal terdapat 3 foto.']);
        }

        $file = new Upload();
        $dir = 'images/kp';
        $foto->delete();
        $file->deleteFile($foto->nama, $dir);

        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
