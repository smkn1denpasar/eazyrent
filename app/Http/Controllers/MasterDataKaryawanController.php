<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Service\Random;
use App\Models\Admin;

class MasterDataKaryawanController extends Controller
{
    public function index() {
        $data = Admin::whereNotIn('role', ['master_admin'])->get();
        // $data = Admin::all();
        return view('admin.master-data.karyawan.index', compact('data'));
    }
    public function view_add() {
        $role = Admin::roleList();
        $gender = Admin::genderList();
        return view('admin.master-data.karyawan.add', compact('role', 'gender'));
    }
    public function view_edit($id) {
        $auth = Auth::user();
        $data = Admin::whereNotIn('id', [$auth->id])->whereNotIn('role', ['master_data'])->where('id', $id)->firstOrFail();
        $role = Admin::roleList();
        $gender = Admin::genderList();
        // $data = Admin::where('id', $id)->firstOrFail();
        return view('admin.master-data.karyawan.edit', compact('data', 'role', 'gender'));
    }
    public function add(Request $request) {

        //validation
        $this->validate($request, [
            'no_ktp' => 'required|digits:16|numeric|unique:tb_admin,no_ktp',
            'email' => 'required|email|unique:tb_admin,email',
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'required|in:' . implode(",", Admin::allowedGender()),
            'alamat' => 'required|max:150',
            'no_telp' => 'required|digits_between:8,13',
            'role' => 'required|in:' . implode(",", Admin::allowedRole()),
        ]);

        //generate the required values
        $password = Random::generate(8, 'mix');

        $req = $request->only('no_ktp', 'email', 'nama', 'jenis_kelamin', 'alamat', 'no_telp', 'role') + ['password' => Hash::make($password)];

        Admin::create($req);

        return redirect()->route('master-data.karyawan')->with(['success' => 'Data berhasil ditambahkan...']);
    }
    public function edit(Request $request, $id) {
        $auth = Auth::user();
        $data = Admin::whereNotIn('id', [$auth->id])->whereNotIn('role', ['master_data'])->where('id', $id)->firstOrFail();

        $this->validate($request, [
            'no_ktp' => [
                'required', 
                'digits:16',
                'numeric',
                Rule::unique('tb_admin')->ignore($data->no_ktp, 'no_ktp')
            ],
            'email' => [
                'required', 'email',
                Rule::unique('tb_admin')->ignore($data->email, 'email')
            ],
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'required|in:' . implode(",", Admin::allowedGender()),
            'alamat' => 'required|max:150',
            'no_telp' => 'required|digits_between:8,13',
            'role' => 'required|in:' . implode(",", Admin::allowedRole()),
        ]);
        $status = $request->status == 1 ? 'Customer Service' : 'Administrator';
        $req = $request->only('no_ktp', 'email', 'nama', 'jenis_kelamin', 'alamat', 'no_telp', 'role');
        if(!empty($request->password)) {
            $this->validate($request, ['password' => 'required|confirmed']);
            $req['password'] = Hash::make($request->password);
        }

        $data->update($req);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }
    public function delete($id) {
        $auth = Auth::user();
        $data = Admin::whereNotIn('id', [$auth->id])->where('id', $id)->firstOrFail();
        $data->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
