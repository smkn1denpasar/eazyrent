<?php

namespace App\Http\Controllers;

use Auth;
use Cart;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Service\Random;
use App\Models\Kendaraan;
use App\Models\Transaksi;

class SewaController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            if(\Request::route()->getName() != 'car.create_cart' && Cart::session($auth->kode)->isEmpty()) {
                return redirect()->route('car');
            }
            return $next($request);
        });
    }

    public function create_cart($kode)
    {
        $auth = Auth::user();
        Cart::session($auth->kode)->clear();
        Session::forget('step_num');
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $cart = Cart::session($auth->kode)->add([
            'id' => $data->kode,
            'name' => $data->fullNameKendaraan,
            'price' => $data->total_harga,
            'quantity' => 1
        ]);
        Session::put('step_num', 1);
        return redirect()->route('car.sewa.step_1', $kode);
    }
    public function step_1($kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $cart = Cart::session($auth->kode)->get($kode);
        if(Session::has('step_num'))
        {
            if(Session::get('step_num') === 1)
            {
                return view('user.sewa.index', compact('data'));
            }
            return redirect()->route('car.sewa.step_' . Session::get('step_num'), $data->kode)->with(compact('data'));
        }
        return view('user.sewa.index', compact('data'));
    }
    public function step_2($kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $cart = Cart::session($auth->kode)->get($kode);
        if(Session::has('step_num'))
        {
            if(Session::get('step_num') === 2)
            {
                $twp_1 = $cart->attributes->tanggal_waktu_pengambilan;
                $twp_2 = $cart->attributes->tanggal_waktu_pengembalian;
                $diff = strtotime(date("d-m-Y", strtotime($twp_2))) - strtotime(date("d-m-Y", strtotime($twp_1)));
                $day = round($diff / (60 * 60 * 24));
                return view('user.sewa.step2', compact('data', 'twp_1', 'twp_2', 'day'));
            }
            return redirect()->route('car.sewa.step_' . Session::get('step_num'), $data->kode)->with(compact('data'));
        }
        return view('user.sewa.index', compact('data'));
    }

    public function step_3($kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $cart = Cart::session($auth->kode)->get($kode);
        if(Session::has('step_num'))
        {
            if(Session::get('step_num') === 3)
            {
                $twp_1 = $cart->attributes->tanggal_waktu_pengambilan;
                $twp_2 = $cart->attributes->tanggal_waktu_pengembalian;
                $jenis_pembayaran = Transaksi::jenisPembayaran();
                $jenis_pengambilan = Transaksi::jenisPengambilan();
                $diff = strtotime(date("d-m-Y", strtotime($twp_2))) - strtotime(date("d-m-Y", strtotime($twp_1)));
                $day = round($diff / (60 * 60 * 24));
                $pick_address = $data->alamat_pengambilan;
                return view('user.sewa.step3', compact('data', 'twp_1', 'twp_2', 'day', 'jenis_pembayaran', 'jenis_pengambilan', 'pick_address'));
            }
            return redirect()->route('car.sewa.step_' . Session::get('step_num'), $data->kode)->with(compact('data'));
        }
        return view('user.sewa.index', compact('data'));
    }
    public function step_1_post(Request $request, $kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        if(date('H') >= 17) {
            $vldt = ['tanggal_waktu_pengambilan' => 'required|date|date_format:"Y-m-d H:i"|after:today'];
        } else {
            $vldt = ['tanggal_waktu_pengambilan' => 'required|date|date_format:"Y-m-d H:i"|after:tomorrow'];
        }
        $this->validate($request, [
            $vldt,
            'tanggal_waktu_pengembalian' => 'required|date|date_format:"Y-m-d H:i"|after:tanggal_waktu_pengambilan',
        ]);
        Session::put([
            'step_num' => 2
        ]);
        $diff = strtotime(date("d-m-Y", strtotime($request->tanggal_waktu_pengembalian))) - strtotime(date("d-m-Y", strtotime($request->tanggal_waktu_pengambilan)));
        $day = round($diff / (60 * 60 * 24));
        $cart = Cart::session($auth->kode)->get($kode);
        Cart::session($auth->kode)->update($kode, [
            'price' => $cart->price*$day,
            'attributes' => $request->except('_token')
        ]);
        return redirect()->route('car.sewa.step_2', $data->kode);
    }
    public function step_2_post(Request $request, $kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $this->validate($request, [
            'nama' => 'required',
            'no_telp' => 'required|numeric|digits_between:8,13|regex:/[0-9]/',
            'email' => 'required|email',
            'informasi' => 'nullable'
        ]);
        $cart = Cart::session($auth->kode)->get($kode);
        Cart::session($auth->kode)->update($kode, [
            'attributes' => array_merge(json_decode($cart->attributes, true) + $request->except('_token'))
        ]);
        Session::put([
            'step_num' => 3
        ]);
        return redirect()->route('car.sewa.step_3', $data->kode);
    }
    public function step_3_post(Request $request, $kode)
    {
        $auth = Auth::user();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        $this->validate($request, [
            'jenis_pembayaran' => 'required|in:' . Transaksi::allowedPembayaran(),
            'jenis_pengambilan' => 'required|in:' . Transaksi::allowedPengambilan(),
            'alamat_diantar' => 'required_if:jenis_pengambilan,"diantar"',
        ]);
        $cart = Cart::session($auth->kode)->get($kode);
        $total = Cart::session($auth->kode)->getTotal();
        if($request->jenis_pengambilan == 'diantar')
        {
            $total = $total+$data->biaya_antar;
        }
        $transaksi = [
            'kode' => Random::generate(5),
            'kode_user' => $auth->kode,
            'kode_kendaraan' => $kode,
            'kode_rental' => $data->Rental->kode,
            'jenis_pengambilan' => $request->jenis_pengambilan,
            'jenis_pembayaran' => $request->jenis_pembayaran,
            'harga' => $data->harga,
            'diskon' => $data->diskon,
            'biaya_antar' => $data->biaya_antar,
            'total' => $total,
            'tanggal_waktu_pengambilan' => $cart->attributes->tanggal_waktu_pengambilan,
            'tanggal_waktu_pengembalian' => $cart->attributes->tanggal_waktu_pengembalian,
        ];
        $dataT = [
            'kode' => Random::generate(5),
            'nama' => $cart->attributes->nama,
            'no_telp' => $cart->attributes->no_telp,
            'email' => $cart->attributes->email,
            'informasi' => $cart->attributes->informasi,
            'alamat_diantar' => $request->alamat_diantar
        ];
        $create = Transaksi::create($transaksi);
        $create->Data()->create($dataT);
        $data->update(['status' => 'transaksi_pending']);
        Session::forget('step_num');
        return redirect()->route('car.sewa.success');
    }
    public function success()
    {
        $auth = Auth::user();
        Cart::session($auth->kode)->clear();
        return view('user.sewa.success', compact('kode'));
    }
}
