<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SocialMedia;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SocialMedia::whereIn('type', ['email', 'no_telp', 'location'])->get();
        $icon = static::icon();
        return view('user.contact-us.index', compact('data', 'icon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public static function icon()
    {
        return [
            'email' => [
                'icon' => 'fa fa-envelope'
            ],
            'no_telp' => [
                'icon' => 'fa fa-phone'
            ],
            'location' => [
                'icon' => 'fa fa-map-marker-alt'
            ]
        ];
    }
}
