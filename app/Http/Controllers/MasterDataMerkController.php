<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Merk;

class MasterDataMerkController extends Controller
{
    public function index() {
        $data = Merk::orderBy('nama', 'asc')->get();
        return view('admin.master-data.merk.index', compact('data'));
    }
    public function view_add() {
        return view('admin.master-data.merk.add');
    }
    public function view_edit($id) {
        $data = Merk::where('id', $id)->firstOrFail();
        return view('admin.master-data.merk.edit', compact('data'));
    }
    public function add(Request $request) {
        $this->validate($request, [
            'nama' => 'required|unique:tb_merk,nama',
        ]);

        $req = $request->only('nama');
        Merk::create($req);

        return redirect()->route('master-data.merk')->with(['success' => 'Data berhasil ditambahkan...']);
    }
    public function edit(Request $request, $id) {
        $data = Merk::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'nama' => ['required', Rule::unique('tb_merk')->ignore($data->nama, 'nama')],
        ]);

        $req = $request->only('nama');
        $data->update($req);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }
    public function delete($id) {
        $data = Merk::where('id', $id)->firstOrFail();
        $result = ['danger' => 'Data gagal dihapus. Pastikan tidak terdapat kendaraan dengan merk ' . $data->nama];
        if($data->Kendaraan->count() == 0) {
            $data->delete();
            $result = ['success' => 'Data berhasil dihapus...'];
        }
        return redirect()->back()->with($result);
    }
}
