<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Transaksi;

class TransaksiPengembalianController extends Controller
{
    public function index() {
        $data = Transaksi::where('status', 4)->get();
        return view('transaksi.pengembalian.index', compact('data'));
    }
    public function view_add() {
        return view('transaksi.pengembalian.add');
    }
    public function add(Request $request) {
        $data = Transaksi::where('no_transaksi', $request->no_transaksi)->where('status', 2)->firstOrFail();
        $data->update(['status' => 4]);
        return redirect()->route('transaksi.pengembalian');
    }
    public function api_transaksi() {
        $data = Transaksi::where('status', 2)->select(DB::RAW('no_transaksi AS value'))->get();
        return $data;
    }
}
