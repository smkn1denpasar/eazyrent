<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Transaksi;
use App\Models\RentalAdminUtama;

class RentalHistoryTransaksi extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            $rental = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            if(\Request::route()->getName() != 'rental.register' && !$rental) {
                return redirect()->route('rental.register');
            }
            return $next($request);
        });
    }
    public function index()
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::where('kode_rental', $rau->kode_rental)
        ->whereIn('status', ['batal', 'selesai'])
        ->orderBy('id', 'desc')
        ->paginate(3);
        return view('user.rental.history-penyewaan.index', compact('data'));
    }
    public function show($kode)
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::where('kode_rental', $rau->kode_rental)->whereIn('status', ['batal', 'selesai'])->where('kode', $kode)->firstOrFail();
        return view('user.rental.history-penyewaan.detail', compact('data'));
    }
}