<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Kendaraan;

class SearchController extends Controller
{
    public function user_ktp(Request $request) {
        $term = $request->get('term');

        if(!empty($term)) {
	
            $results = array();
            
            $queries = User::where('no_ktp', 'LIKE', '%'.$term.'%')->take(5)->get();
            
            foreach ($queries as $query)
            {
                $results[] = [ 'kode' => $query->kode, 'value' => $query->no_ktp ];
            }
            return response()->json($results, 200);
        }
    }

    public function user_data($kode) {
        $data = User::where('kode', $kode)->firstOrFail();
        return response()->json($data, 200);
    }

    public function jenis_kendaraan(Request $request) {
        $term = $request->get('term');

        if(!empty($term)) {
	
            $results = array();
            
            $queries = Kendaraan::where('jenis', 'LIKE', '%'.$term.'%')->take(5)->get();
            
            foreach ($queries as $query)
            {
                $results[] = [ 'kode' => $query->kode, 'value' => $query->no_ktp ];
            }
            return response()->json($results, 200);
        }
    }
}