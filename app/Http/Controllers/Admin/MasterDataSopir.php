<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Sopir;

class MasterDataSopir extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sopir::all();
        return view('admin.master-data.sopir.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master-data.sopir.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'no_ktp' => 'required|numeric|digits:16|unique:tb_sopir,no_ktp',
            'no_sim' => 'required|numeric|unique:tb_sopir,no_sim',
            'nama' => 'required|max:100|alpha_cds',
            'alamat' => 'required|max:200',
            'no_tlp' => 'required|digits_between:8,13|regex:/(0)[0-9]/',
        ]);
        $req = $request->only('no_ktp', 'no_sim', 'nama', 'alamat', 'no_tlp');
        Sopir::create($req);
        return redirect()->route('master-data.sopir')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Sopir::where('id', $id)->firstOrFail();
        return view('admin.master-data.sopir.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Sopir::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'no_sim' => ['required', 'numeric', Rule::unique('tb_sopir')->ignore($data->no_sim, 'no_sim')],
            'alamat' => 'required|max:200',
            'no_tlp' => 'required|digits_between:8,13|regex:/(0)[0-9]/',
            'status' => 'required|in:1,2'
        ]);
        $req = $request->only('no_sim', 'alamat', 'no_tlp', 'status');
        $data->update($req);
        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Sopir::where('id', $id)->firstOrFail();
        if($data->status == 2) {
            return redirect()->back()->with(['error' => 'Gagal menghapus data...<br>Sopir sedang dalam pekerjaan...']);
        } else {
            $data->delete();
            return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
        }
    }
}
