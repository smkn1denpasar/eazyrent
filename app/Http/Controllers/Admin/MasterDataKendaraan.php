<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Kendaraan;
use App\Models\Merk;
use App\Models\Pemilik;
use App\Models\Rental;
use App\Models\User;

class MasterDataKendaraan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($kode)
    {
        $data_rental = Rental::where('kode', $kode)->firstOrFail();
        $data = Kendaraan::where('kode_rental', $kode)->get();
        return view('admin.master-data.rental.kendaraan.index', compact('data', 'data_rental'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($kode)
    {
        $rental = Rental::where('kode', $kode)->firstOrFail();
        $merk = Merk::pluck('nama', 'id')->all();
        $transmisi = Kendaraan::transmisiList();
        $status = Kendaraan::statusList();
        return view('admin.master-data.rental.kendaraan.add', compact('rental', 'merk', 'transmisi', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $kode)
    {
        $data = User::where('no_ktp', $request->no_ktp)->first();
        if(!$data || $data->isEmpty()) {
            $this->validate($request, [
                'no_ktp' => 'required|numeric',
                'nama' => 'required|max:100',
                'alamat' => 'required|max:100',
                'no_telp' => 'required|digits_between:8,13|regex:/(0)[0-9]/',
            ]);
        } else {
            $this->validate($request, [
                'no_ktp' => 'required|numeric|exists:tb_user,no_ktp',
            ]);
        }
        $this->validate($request, [
            'jenis' => 'required|max:50',
            'plat' => 'required|between:5,12',
            'tahun' => 'required|date_format:"Y"|after:1980',
            'tarif' => 'required|numeric',
            'transmisi' => 'required|in:' . implode(",", Kendaraan::allowedTransmisi()),
        ]);
        // $req['data_kendaraan'] = $request->only('jenis', 'plat', 'tahun', 'tarif', 'transmisi', 'status');
        // if($request->type == 0) {
        //     $req['data_pemilik'] = $request->only('no_ktp', 'nama', 'alamat', 'no_telp');
        //     $data = Pemilik::create($req['data_pemilik']);
        //     $data->kendaraan()->create($req['data_kendaraan']);
        // } else {
        //     $data = Pemilik::where('no_ktp', $request->no_ktp)->firstOrFail();
        //     $data->kendaraan()->create($req['data_kendaraan']);
        // }
        // return redirect()->route('master-data.kendaraan')->with(['success' => 'Data berhasil dtambahkan...']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_kendaraan()
    {
        $data = Kendaraan::select(DB::RAW('DISTINCT jenis AS value'))->get();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_pemilik($id = null)
    {
        $data = Pemilik::select('no_ktp AS label', 'id')->get();
        if(!empty($id)) {
            $data = Pemilik::where('id', $id)->first();
            if(!$data) {
                return response()->json(['status' => 400]);
            }
            return response()->json(['status' => 200, 'data' => $data]);
        }
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        $transmisi = Kendaraan::transmisiList();
        $status = Kendaraan::statusList();
        return view('admin.master-data.kendaraan.edit', compact('data', 'transmisi', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'type' => 'required|in:0,1',
            'no_ktp' => 'required',
            'nama' => 'required_if:type,0|max:100',
            'alamat' => 'required_if:type,0|max:100',
            'no_telp' => 'required_if:type,0|digits_between:8,13|regex:/(0)[0-9]/',
            'tarif' => 'required|numeric',
            'status' => 'required|in:' . implode(",", Kendaraan::allowedStatus()),
        ]);
        $req['data_kendaraan'] = $request->only('tarif', 'status');
        $data_pemilik = Pemilik::where('no_ktp', $request->no_ktp)->first();
        if($data_pemilik->count() >= 1) {
            $data->update($req['data_kendaraan']);
        } else {
            $req['data_pemilik'] = $request->only('no_ktp', 'nama', 'alamat', 'no_telp');
            $create = Pemilik::update($req);
            $create->Kendaraan->update($req['data_kendaraan']);
        }
        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        $check_ = Kendaraan::where('pemilik_id', $data->pemilik_id)->count();
        $data->delete();
        if($check_ <= 1) {
            $data->Pemilik->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
