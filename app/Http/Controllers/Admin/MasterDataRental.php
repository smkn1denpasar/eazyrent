<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\Rental;
use App\Models\Kendaraan;
use App\Models\DetailRental;
use App\Models\Wilayah;
use App\Models\User;
use App\Service\Random;
use App\Service\Upload;

class MasterDataRental extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DetailRental::get();
        return view('admin.master-data.rental.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        $pemilik = User::select(DB::raw('CONCAT(no_ktp, " - ", nama) AS nama_new'), 'kode')
        ->whereNotIn('kode', function($query) {
            $query->select('kode_user')->from('tb_rental_admin');
        })->whereNotIn('kode', function($query) {
            $query->select('kode_user')->from('tb_rental_admin_utama');
        })->pluck('nama_new', 'kode');
        return view('admin.master-data.rental.add', compact('wlyh', 'pemilik'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'logo' => 'required|file|image|mimes:jpg,png,jpeg',
            'no_telp' => 'required|digits_between:8,13|regex:/[0-9]/',
            'email' => 'required|email',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'alamat' => 'required|max:200',
            'kode_user' => 'required|exists:tb_user,kode'
        ]);
        if ($request->hasFile('logo')) {
            $upload = new Upload();
            $logo = $upload->fileName($request->file('logo'), $request->nama);
        }
        $detail = $request->only('nama') + ['kode' => Random::generate(5), 'logo' => $logo];
        $rental = $request->only('alamat', 'email', 'kode_lokasi', 'no_telp') + ['role' => 'rental', 'kode' => Random::generate(5)];
        $pemilik = $request->only('kode_user');

        $create = DetailRental::create($detail);
        $create->RentalUtama()->create($rental);
        $create->Pemilik()->create($pemilik);
        if($create) {
            $upload->saveFile($request->file('logo'), $logo, 'images/logo');
        }
        return redirect()->route('master-data.rental')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DetailRental::where('kode', $id)->firstOrFail();
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('admin.master-data.rental.edit', compact('data', 'wlyh'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DetailRental::where('kode', $id)->firstOrFail();
        $this->validate($request, [
            'nama' => 'required',
            'logo' => 'nullable|file|image|mimes:jpg,png,jpeg',
            'no_telp' => 'required|digits_between:8,13|regex:/[0-9]/',
            'email' => 'required|email',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'alamat' => 'required|max:200',
        ]);
        $detail = $request->only('nama');
        $old_logo = $data->logo;
        if ($request->hasFile('logo')) {
            $upload = new Upload();
            $detail['logo'] = $logo = $upload->fileName($request->file('logo'), $request->nama);
        }
        $rental = $request->only('alamat', 'email', 'kode_lokasi', 'no_telp');

        $update = $data->update($detail);
        $update .= $data->RentalUtama()->update($rental);

        if($request->hasFile('logo') && $update) {
            $upload->deleteFile($old_logo, 'images/logo');
            $upload->saveFile($request->file('logo'), $logo, 'images/logo');
        }

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $data = DetailRental::where('kode', $kode)->firstOrFail();
        foreach($data->Rental()->get() as $val) {
            foreach($val->Kendaraan()->get() as $value) {
                $data_pemilik = Kendaraan::where('pemilik_id', $value->pemilik_id)->whereNotIn('kode_rental', [$value->kode_rental])->count();
                if($data_pemilik < 1) {
                    $value->Pemilik()->delete();
                }
                $value->delete();
            }
        }
        $data->Rental()->delete();
        $data->Pemilik()->delete();
        if ($data->logo != '') {
            $file = new Upload();
                $file->deleteFile($data->logo, 'images/logo');
        }
        $data->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
