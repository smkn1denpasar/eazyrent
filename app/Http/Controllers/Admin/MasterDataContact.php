<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Service\Random;

class MasterDataContact extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SocialMedia::whereIn('type', ['email', 'no_telp'])->get();
        return view('admin.master-data.contact.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = SocialMedia::contactList();
        return view('admin.master-data.contact.add', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:' . SocialMedia::allowedContact()
        ]);
        if($request->type == 'email') {
            $this->validate($request, [
                'data' => 'required|email'
            ]);
        } else {
            $this->validate($request, [
                'data' => 'required|digits_between:8,13'
            ]);
        }

        $kode = Random::generate(5);
        $req = $request->only('type') + ['kode' => $kode, 'link' => $request->data];

        SocialMedia::create($req);

        return redirect()->route('master-data.contact')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $data = SocialMedia::whereIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();
        $type = SocialMedia::contactList();

        return view('admin.master-data.contact.edit', compact('data', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $data = SocialMedia::whereIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();

        $this->validate($request, [
            'type' => 'required|in:' . SocialMedia::allowedContact()
        ]);
        if($request->type == 'email') {
            $this->validate($request, [
                'data' => 'required|email'
            ]);
        } else {
            $this->validate($request, [
                'data' => 'required|digits_between:8,13'
            ]);
        }

        $req = $request->only('type') + ['link' => $request->data];

        $data->update($req);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $data = SocialMedia::whereIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();
        $data->delete();

        return redirect()->route('master-data.contact')->with(['success' => 'Data berhasil dihapus...']);
    }
}
