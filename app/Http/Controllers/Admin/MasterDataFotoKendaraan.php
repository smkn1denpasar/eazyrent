<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kendaraan;
use App\Models\Foto;
use App\Models\Rental;
use App\Service\Upload;
use App\Service\Random;

class MasterDataFotoKendaraan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $kode)
    {
        $data_rental = Rental::where('kode', $id)->firstOrFail();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        return view('admin.master-data.rental.kendaraan.foto.index', compact('data', 'data_rental'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, $kode)
    {
        $data_rental = Rental::where('kode', $id)->firstOrFail();
        $data = Kendaraan::where('kode', $kode)->firstOrFail();
        return view('admin.master-data.rental.kendaraan.foto.add', compact('data', 'data_rental'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, $kode)
    {
        $car = Kendaraan::where('kode_rental', $id)->where('kode', $kode)->firstOrFail();

        $this->validate($request, [
            'foto' => 'required',
            'foto.*' => 'mimes:jpg,jpeg,png|max:3000|dimensions:min_width=500,min_height=300',
        ]);
        $upload = new Upload();
        $foto = $request->file('foto');
        $rental = Rental::where('kode', $id)->firstOrFail();
        $name = $rental->nama . $car->jenis . $car->kode_merk;
        foreach($foto as $index => $val) {
            $i[$index]['nama'] = $upload->fileName($val, $name);
            $i[$index]['kode'] = Random::generate(5);
        }

        $car->Foto()->createMany($i);

        $dir = 'images/kp';
        foreach($foto as $index => $val) {
            $upload->saveFile($val, $i[$index]['nama'], $dir);
        }
        return redirect()->back()->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $kode, $kode_foto)
    {
        $car = Kendaraan::where('kode_rental', $id)->where('kode', $kode)->firstOrFail();
        $foto = Foto::where('kode_kendaraan', $kode)->where('kode', $kode_foto)->firstOrFail();

        $check = Foto::where('kode_kendaraan', $kode)->count();
        if($check <= 5) {
            return redirect()->back()->with(['danger' => 'Gagal mengapus data. Minimal terdapat 3 foto.']);
        }

        $file = new Upload();
        $dir = 'images/kp';
        $foto->delete();
        $file->deleteFile($foto->nama, $dir);

        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
