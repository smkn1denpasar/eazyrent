<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Service\Random;

class MasterDataSocialMedia extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SocialMedia::whereNotIn('type', ['email', 'no_telp', 'location'])->orderBy('type', 'asc')->get();
        return view('admin.master-data.social-media.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = SocialMedia::socialMediaList();
        return view('admin.master-data.social-media.add', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:' . SocialMedia::allowedSocialMedia(),
            'link' => 'required|url'
        ]);

        $kode = Random::generate(5);
        $req = $request->only('type', 'link') + ['kode' => $kode];

        SocialMedia::create($req);

        return redirect()->route('master-data.social-media')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $type = SocialMedia::socialMediaList();
        $data = SocialMedia::whereNotIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();
        return view('admin.master-data.social-media.edit', compact('data', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $data = SocialMedia::whereNotIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();
        $this->validate($request, [
            'type' => 'required|in:' . SocialMedia::allowedSocialMedia(),
            'link' => 'required|url'
        ]);

        $req = $request->only('type', 'link');
        $data->update($req);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $data = SocialMedia::whereNotIn('type', ['email', 'no_telp'])->where('kode', $kode)->firstOrFail();
        $data->delete();
        return redirect()->route('master-data.social-media')->with(['success' => 'Data berhasil dihapus...']);
    }
}
