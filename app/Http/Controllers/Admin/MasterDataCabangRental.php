<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Rental;
use App\Models\DetailRental;
use App\Models\Wilayah;
use App\Models\User;
use App\Service\Random;

class MasterDataCabangRental extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = Rental::where('role', 'cabang_rental')->where('kode_rental_utama', $id)->get();
        $kode = $id;
        return view('admin.master-data.rental.cabang.index', compact('data', 'kode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = DetailRental::where('kode', $id)->firstOrFail();
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        // $pemilik = User::select(DB::raw('CONCAT(no_ktp, " - ", nama) AS nama_new'), 'kode')->whereNotIn('kode', function($query) {
        //     $query->select('kode_pemilik')->from('tb_rental_detail');
        // })->whereNull('kode_rental_admin_utama')->where(function($query) {
        //     $query->whereNull('kode_rental_admin')->orWhereIn('kode_rental_admin', function($query) {
        //         $query->select('kode')->from('tb_rental');
        //     });
        // })->pluck('nama_new', 'kode');
        $pemilik = User::select(DB::raw('CONCAT(no_ktp, " - ", nama) AS nama_new'), 'kode')
        ->whereNotIn('kode', function($query) {
            $query->select('kode_pemilik')->from('tb_rental_detail');
        })->whereNotIn('kode', function($query) {
            $query->select('kode_user')->from('tb_rental_admin_utama');
        })->whereNotIn('kode', function($query) {
            $query->select('kode_user')->from('tb_rental_admin');
        })->pluck('nama_new', 'kode');
        return view('admin.master-data.rental.cabang.add', compact('data', 'wlyh', 'pemilik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = DetailRental::where('kode', $id)->firstOrFail();
        $this->validate($request,[
            'no_telp' => 'required|digits_between:8,13|regex:/[0-9]/',
            'email' => 'required|email',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'alamat' => 'required|max:200',
            'kode_user' => 'required|exists:tb_user,kode',
        ]);

        $kode = Random::generate(5);
        $rental = $request->only('no_telp', 'email', 'kode_lokasi', 'alamat') + ['kode' => $kode, 'role' => 'cabang_rental'];
        $admin = $request->only('kode_user');

        $create = $data->Rental()->create($rental);
        $create->Admin()->create($admin);

        return redirect()->route('master-data.cabang-rental', $data->kode)->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode, $id)
    {
        $data = Rental::where('kode_rental_utama', $kode)->where('kode', $id)->where('role', 'cabang_rental')->firstOrFail();
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('admin.master-data.rental.cabang.edit', compact('data', 'wlyh'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode, $id)
    {
        $data = Rental::where('kode_rental_utama', $kode)->where('kode', $id)->where('role', 'cabang_rental')->firstOrFail();
        $this->validate($request,[
            'no_telp' => 'required|digits_between:8,13|regex:/[0-9]/',
            'email' => 'required|email',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'alamat' => 'required|max:200',
        ]);

        $rental = $request->only('no_telp', 'email', 'kode_lokasi', 'alamat');
        $data->update($rental);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode, $id)
    {
        $data = Rental::where('kode_rental_utama', $kode)->where('kode', $id)->where('role', 'cabang_rental')->firstOrFail();
        $delete = $data->Kendaraan()->delete();
        foreach($data->Kendaraan()->get() as $dk) {
            $data_pemilik = Kendaraan::where('pemilik_id', $dk->pemilik_id)->whereNotIn('kode_rental', [$dk->kode])->count();
            if($data_pemilik < 1) {
                $data->Kendaraan()->Pemilik()->delete();
            }
        }
        $delete .= $data->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
}
