<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Merk;
use App\Service\Random;

class MasterDataMerk extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Merk::orderBy('nama', 'asc')->get();
        return view('admin.master-data.merk.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master-data.merk.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:tb_merk,nama',
        ]);

        $kode = Random::generate(5);
        $req = $request->only('nama') + ['kode' => $kode];
        Merk::create($req);

        return redirect()->route('master-data.merk')->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $data = Merk::where('kode', $kode)->firstOrFail();
        return view('admin.master-data.merk.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Merk::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'nama' => ['required', Rule::unique('tb_merk')->ignore($data->nama, 'nama')],
        ]);

        $req = $request->only('nama');
        $data->update($req);

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Merk::where('id', $id)->firstOrFail();
        $result = ['danger' => 'Data gagal dihapus. Pastikan tidak terdapat kendaraan dengan merk ' . $data->nama];
        if($data->Kendaraan->count() == 0) {
            $data->delete();
            $result = ['success' => 'Data berhasil dihapus...'];
        }
        return redirect()->back()->with($result);
    }
}
