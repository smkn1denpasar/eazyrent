<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\DetailRental;
use App\Models\Rental;
use App\Models\RentalAdmin;

class MasterDataRentalAdmin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($kode)
    {
        $data = RentalAdmin::where('kode_rental', $kode)->get();
        $data_rental = Rental::where('kode', $kode)->firstOrFail();
        return view('admin.master-data.rental.admin.index', compact('data', 'data_rental'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($kode)
    {
        $data = Rental::where('kode', $kode)->where('role', 'rental')->firstOrFail();
        $user = User::whereNotIn('kode', function($query) {
            $query->select('kode_user')->from('tb_rental_admin');
        })
        ->select(DB::raw('CONCAT(no_ktp, " - ", nama) AS nama_new'), 'kode')
        ->pluck('nama_new', 'kode');
        $rental_utama = DetailRental::get();
        foreach($rental_utama as $value) {
            $rtl = Rental::where('kode_rental_utama', $value['kode'])->join('tb_wilayah', 'tb_wilayah.kode', '=', 'tb_rental.kode_lokasi')->get();
            foreach($rtl as $val) {
                $rental[$value['nama']][$val->kode] = $value['nama'] . ' - ' . $val->Lokasi->nama;
            }
        }
        return view('admin.master-data.rental.admin.add', compact('data', 'rental', 'user'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $kode)
    {
        $data = Rental::where('kode', $kode)->where('role', 'rental')->firstOrFail();
        $this->validate($request, [
            'kode_user' => 'required|exists:tb_user,kode',
        ]);

        $kode_user = $request->only('kode_user');

        $data->Admin()->create($kode_user);

        return redirect()->route('master-data.rental.admin', $data->kode)->with(['success' => 'Data berhasil ditambahkan...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_rental, $kode_user)
    {
        $data = RentalAdmin::where('kode_user', $kode_user)->where('kode_rental', $kode_rental)->firstOrFail();
        $data->delete();
        return redirect()->back()->with(['success' => 'Data berhasi dihapus...']);
    }
}
