<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\RentalAdminUtama;
use App\Models\Rental;
use App\Models\Wilayah;
use App\Service\Random;
use Auth;

class RentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        $data = Rental::where('username', $username)->firstOrFail();
        $owner = false;

        if(Auth::check()) {
            $auth = \Auth::user();
            $check_rental = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            if($check_rental) {
                $owner = true;
            }
        }
        $view = view('user.rental.index', compact('data', 'owner'));
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->first();
        if($data) {
            return redirect()->route('rental');
        }
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('user.rental.register', compact('wlyh'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        $data = RentalAdminUtama::whereNotIn('kode_user', [$user->kode])->first();
        $this->validate($request, [
            'nama' => 'required',
            'username' => 'required|unique:tb_rental,username|max:100',
            'no_telp' => 'required|digits_between:8,13',
            'alamat' => 'required',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'terms_agree' => 'required',
        ]);

        $kode_user = ['kode_user' => $user->kode];
        $kode_rental = ['kode' => Random::generate(5)];
        $rental = $request->only('nama', 'username', 'no_telp', 'alamat', 'kode_lokasi') + $kode_rental;

        $create = Rental::create($rental);
        $create->Pemilik()->create($kode_user);

        return redirect()->route('rental.profile')->with(['success' => 'Selamat, Rental anda berhasil dibuat. Silahkan lengkapi data dibawah ini.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $auth = \Auth::user();
        $data = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('user.rental.profile', compact('data', 'wlyh'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $auth = \Auth::user();
        $check = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Rental::where('kode', $check->kode_rental)->firstOrFail();
        $this->validate($request, [
            'nama' => 'required|string',
            'slogan' => 'nullable|max:255',
            'username' => ['required', 'max:100', Rule::unique('tb_rental')->ignore($data->username, 'username')],
            'email' => 'nullable|email',
            'no_telp' => 'required|digits_between:8,13',
            'alamat' => 'required',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode',
            'deskripsi' => 'nullable'
        ]);
        $rental = $request->only('nama', 'slogan', 'username', 'email', 'no_telp', 'alamat', 'kode_lokasi', 'deskripsi');

        $data->update($rental);

        return redirect()->route('rental.profile')->with(['success' => 'Data berhasil diubah...']);
    }
}