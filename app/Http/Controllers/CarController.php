<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Merk;
use App\Models\Wilayah;
use App\Models\Rental;
use App\Models\User;
use App\Models\RentalAdminUtama;
use Auth;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $car = Kendaraan::orderBy('harga', 'asc');
        if(Auth::check()) {
            $auth = \Auth::user();
            $data = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            $car = Kendaraan::whereNotIn('kode_rental', [$data->kode_rental]);
        }
        $tipe = Kendaraan::transmisiList();
        $merk = Merk::pluck('nama', 'kode');
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        if($request->has('lokasi') && !empty($request->lokasi))
        {
            $car->where('kode_lokasi', $request->lokasi);
        }
        if($request->has('merk') && !empty($request->merk))
        {
            $car->where('kode_merk', $request->merk);
        }
        if($request->has('transmisi') && !empty($request->transmisi))
        {
            $car->where('transmisi', $request->transmisi);
        }
        if($request->has('price_range') && !empty($request->price_range))
        {
            $harga = explode(',', urldecode($request->price_range));
            $harga_start = $harga[0];
            $harga_end = $harga[1];
            $car->whereBetween('harga', [$harga_start, $harga_end]);
        }
        $car = $car->where('status', 'tersedia')->paginate(5);
        return view('user.car.index', compact('car', 'tipe', 'merk', 'wlyh', 'request'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kode)
    {
        $data = Kendaraan::where('kode', $kode)->where('status', 'tersedia')->firstOrFail();
        $similar = Kendaraan::where('jenis', 'like', '%' . $data->jenis . '%')->whereNotIn('kode', [$data->kode])->get();
        return view('user.car.detail', compact('data', 'similar'));
    }
}
