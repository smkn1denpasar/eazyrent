<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Transaksi;
use App\Models\RentalAdminUtama;

class RentalStatusPenyewaan extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            $rental = RentalAdminUtama::where('kode_user', $auth->kode)->first();
            if(\Request::route()->getName() != 'rental.register' && !$rental) {
                return redirect()->route('rental.register');
            }
            return $next($request);
        });
    }
    public function index()
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::whereIn('status', ['pending', 'digunakan'])->where('kode_rental', $rau->kode_rental)->paginate(3);
        return view('user.rental.status-penyewaan.index', compact('data'));
    }
    public function edit($kode)
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::whereIn('status', ['pending', 'digunakan'])->where('kode_rental', $rau->kode_rental)->where('kode', $kode)->firstOrFail();
        $kendaraan = Kendaraan::whereIn('status', ['transaksi_pending', 'digunakan'])->where('kode', $data->kode_kendaraan)->firstOrFail();
        $status = Transaksi::statusList();
        if($data->status == 'pending')
        {
            unset($status['selesai']);
        } else {
            unset($status['pending'], $status['batal']);
        }
        $view = view('user.rental.status-penyewaan.edit', compact('data', 'status'));
        if($data->status == 'digunakan') $view = redirect()->route('rental.check-kondisi', $data->kode);

        return $view;
    }
    public function update(Request $request, $kode)
    {
        $auth = Auth::user();
        $rau = RentalAdminUtama::where('kode_user', $auth->kode)->firstOrFail();
        $data = Transaksi::whereIn('status', ['pending', 'digunakan'])->where('kode_rental', $rau->kode_rental)->where('kode', $kode)->firstOrFail();
        $kendaraan = Kendaraan::whereIn('status', ['transaksi_pending', 'digunakan'])->where('kode', $data->kode_kendaraan)->firstOrFail();
        $this->validate($request, [
            'status' => 'required|in:' . Transaksi::allowedStatus(),
        ]);
        $data->update($request->only('status'));
        if($request->status == 'digunakan') $kendaraan->update($request->only('status'));
        else if($request->status == 'selesai' || $request->status == 'batal') $kendaraan->update(['status' => 'tersedia']);

        return redirect()->route('rental.status-penyewaan')->with(['success' => 'Status berhasil diubah...']);
    }
}