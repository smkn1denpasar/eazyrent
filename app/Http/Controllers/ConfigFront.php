<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Merk;
use App\Models\Wilayah;

class ConfigFront extends Controller
{
    public function index() {
        $tipe = Kendaraan::transmisiList();
        $merk = Merk::pluck('nama', 'kode');
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('user.index', compact('tipe', 'merk', 'wlyh'));
    }
}
