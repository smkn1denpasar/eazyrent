<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Wilayah;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gender = User::genderList();
        $wilayah = Wilayah::whereRaw('LENGTH(kode) = 2')->orderBy('nama', 'asc')->get();
        foreach($wilayah as $val) {
            $data_wlyh = Wilayah::whereRaw('LEFT(kode, 2) = ' . $val->kode . ' AND LENGTH(kode) = 5')->orderBy('nama', 'asc')->get();
            foreach($data_wlyh as $values) {
                $wlyh[$val->nama][$values->kode] = $values->nama;
            }
        }
        return view('user.settings.profile', compact('gender', 'wlyh'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email', Rule::unique('tb_user')->ignore(Auth::user()->email, 'email')],
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'nullable|in:' . implode(",", User::allowedGender()),
            'alamat' => 'nullable|max:150',
            'no_telp' => 'required|digits_between:8,13',
            'kode_lokasi' => 'required|exists:tb_wilayah,kode'
        ], [], [
            'no_telp' => 'Nomor Telepon/Hp',
            'kode_lokasi' => 'Wilayah'
        ]);
        $req = $request->only('email', 'nama', 'jenis_kelamin', 'alamat', 'no_telp');

        if(empty(Auth::user()->no_ktp))
        {
            $this->validate($request, [
                'no_ktp' => ['nullable', 'numeric', 'digits:16', Rule::unique('tb_user')->ignore(Auth::user()->no_ktp, 'no_ktp')],
            ], [], [
                'no_ktp' => 'No KTP',
            ]);
            $req['ktp'] = $request->no_ktp;
        }

        $resend_email = false;

        if($request->email != Auth::user()->email)
        {
            $req['email_verified_at'] = null;
            $resend_email = true;
        }

        if(!empty($request->password))
        {
            $this->validate($request, [
                'password' => 'min:6|confirmed'
            ]);
            $req['password'] = $request->password;
        }

        Auth::user()->update($req);

        if($resend_email)
        {
            $request->user()->sendEmailVerificationNotification();
        }

        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }
    public function invoice(){
        return view('user.settings.invoice');
    }

    public function detailInvoice(){
        return view('user.settings.detail');
    }
}
