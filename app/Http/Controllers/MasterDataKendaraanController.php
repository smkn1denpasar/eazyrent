<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kendaraan;
use App\Models\Merk;
use App\Models\Pemilik;

class MasterDataKendaraanController extends Controller
{
    public function index() {
        $data = Kendaraan::all();
        return view('admin.master-data.kendaraan.index', compact('data'));
    }
    public function view_add() {
        $merk = Merk::pluck('nama', 'id')->all();
        return view('admin.master-data.kendaraan.add', compact('merk'));
    }
    public function view_edit($id) {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        return view('admin.master-data.kendaraan.edit', compact('data'));
    }
    public function add(Request $request) {
        $this->validate($request, [
            'type' => 'required|in:0,1',
            'no_ktp' => 'required',
            'nama' => 'required_if:type,0|max:100',
            'alamat' => 'required_if:type,0|max:100',
            'no_telp' => 'required_if:type,0|digits_between:8,13|regex:/(0)[0-9]/',
            'jenis' => 'required|max:50',
            'plat' => 'required|between:5,12',
            'tahun' => 'required|date_format:"Y"|after:1980',
            'tarif' => 'required|numeric',
            'transmisi' => 'required|in:1,2',
            'status' => 'required|in:1,2,3'
        ]);
        $req['data_kendaraan'] = $request->only('jenis', 'plat', 'tahun', 'tarif', 'transmisi', 'status');
        if($request->type == 0) {
            $req['data_pemilik'] = $request->only('no_ktp', 'nama', 'alamat', 'no_telp');
            $data = Pemilik::create($req['data_pemilik']);
            $data->kendaraan()->create($req['data_kendaraan']);
        } else {
            $data = Pemilik::where('no_ktp', $request->no_ktp)->firstOrFail();
            $data->kendaraan()->create($req['data_kendaraan']);
        }
        return redirect()->route('master-data.kendaraan')->with(['success' => 'Data berhasil dtambahkan...']);
    }
    public function edit(Request $request, $id) {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'type' => 'required|in:0,1',
            'no_ktp' => 'required',
            'nama' => 'required_if:type,0|max:100',
            'alamat' => 'required_if:type,0|max:100',
            'no_telp' => 'required_if:type,0|digits_between:8,13|regex:/(0)[0-9]/',
            'tarif' => 'required|numeric',
            'status' => 'required|in:1,2,3'
        ]);
        $req['data_kendaraan'] = $request->only('tarif', 'status');
        $data_pemilik = Pemilik::where('no_ktp', $request->no_ktp)->first();
        if($data_pemilik->count() >= 1) {
            $data->update($req['data_kendaraan']);
        } else {
            $req['data_pemilik'] = $request->only('no_ktp', 'nama', 'alamat', 'no_telp');
            $create = Pemilik::update($req);
            $create->Kendaraan->update($req['data_kendaraan']);
        }
        return redirect()->back()->with(['success' => 'Data berhasil diubah...']);
    }
    public function delete($id) {
        $data = Kendaraan::where('id', $id)->firstOrFail();
        $check_ = Kendaraan::where('pemilik_id', $data->pemilik_id)->count();
        $data->delete();
        if($check_ <= 1) {
            $data->Pemilik->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus...']);
    }
    public function api_pemilik() {
        $data = Pemilik::select('no_ktp AS value', 'nama', 'alamat', 'no_telp')->get();
        return $data;
    }
    public function api_kendaraan() {
        $data = Kendaraan::select(DB::RAW('DISTINCT jenis AS value'))->get();
        return $data;
    }
}
