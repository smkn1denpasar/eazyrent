<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaksi;
use App\Models\Kendaraan;
use App\Models\Pelanggan;
use App\Models\Sopir;

class TransaksiPenyewaanController extends Controller
{
    public function index() {
        $data = Transaksi::whereIn('status', [1, 2, 3])->get();
        return view('transaksi.penyewaan.index', compact('data'));
    }
    public function view_add() {
        $jenis_ = Kendaraan::select('jenis')->groupBy('jenis')->orderBy('jenis', 'asc')->get();
        // $kendaraan = [];
        foreach($jenis_ as $data_ => $value_) {
            // $id = Kendaraan::select('jenis', 'id')->where('status', 1)->where('jenis', $value_->jenis)->orderBy('tarif', 'asc')->first();
            $kendaraan[$value_->jenis] = Kendaraan::select(DB::raw('CONCAT(jenis, " (", tahun, ") Rp. ", REPLACE(FORMAT(tarif, "id_ID"), ",", ".")) AS jenis'), 'id')->where('status', 1)->where('jenis', $value_->jenis)->orderBy('tahun', 'desc')->orderBy('tarif', 'asc')->pluck('jenis', 'id');
            // $kendaraan[$id['id']] = $id['jenis'];
        }
        return view('transaksi.penyewaan.add', compact('kendaraan'));
    }
    public function view_edit($id) {
        $data = Transaksi::where('id', $id)->whereIn('status', [1, 2, 3])->firstOrFail();
        return view('transaksi.penyewaan.edit', compact('data'));
    }
    public function add(Request $request) {
        if($request->tanggal_kembali != null) {
            $format_date = date("Y-m-d", strtotime("+3 days", strtotime($request->tanggal_pinjam)));
        }
        $this->validate($request, [
            'no_id' => 'required|numeric|digits_between:12,16',
            'nama' => 'required|alpha_cds|max:150',
            'no_tlp' => 'required|numeric|digits_between:8,13|regex:/(0)[0-9]/',
            'email' => 'required|email',
            'id_kendaraan' => 'required|exists:db_kendaraan,id',
            'jenis_pengambilan' => 'required|in:1,2',
            'alamat' => 'required_if:jenis_pengambilan,1',
            'jenis_pembayaran' => 'required|in:1,2',
            'fasilitas' => 'required|in:1,2',
            'tanggal_sewa' => 'required|date|date_format:"Y-m-d"',
            'tanggal_kembali' => 'required|date|date_format:"Y-m-d"|after_or_equal:'.$format_date
        ]);

        //data pelanggan
        $req['data_pelanggan'] = $request->only('no_id', 'nama', 'alamat', 'no_tlp', 'email');
        $create_ = Pelanggan::create($req['data_pelanggan']);
        
        //data transaksi
        $id_karyawan = Auth::user()->id;
        // $durasi = date("d", strtotime($request->tanggal_kembali)-strtotime($request->tanggal_sewa));
        $durasi = date_diff(date_create($request->tanggal_sewa), date_create($request->tanggal_kembali))->format("%a");
        $no_transaksi = rand(10000,99999);
        $dataK_ = Kendaraan::where('id', $request->id_kendaraan)->first();
        $sopir = Sopir::where('status', 1)->inRandomOrder()->limit(1)->first();
        $total = $dataK_->tarif*$durasi;
        if($request->fasilitas == 1) {
            $total = $total + 150000;
        }
        // return $total;
        $req['data_transaksi'] = $request->only('id_kendaraan', 'jenis_pengambilan', 'jenis_pembayaran', 'fasilitas', 'tanggal_sewa', 'tanggal_kembali') + [
            'no_transaksi' => $no_transaksi,
            'id_karyawan' => $id_karyawan,
            'total' => $total,
        ];
        if($request->fasilitas == 1) {
            $req['data_transaksi']['id_sopir'] = $sopir->id;
        }
        $create_->Transaksi()->create($req['data_transaksi']);
        return redirect()->route('transaksi.penyewaan');
    }

    public function edit(Request $request, $id) {
        $data_ = Transaksi::where('id', $id)->whereIn('status', [1,2])->firstOrFail();
        $this->validate($request, [
            'alamat' => 'required_if:jenis_pengambilan,"Diantar"|max:200',
            'status' => 'required|in:1,2,3',
            'pembayaran' => 'nullable|required_unless:status,1,2|numeric',
        ]);
        $req = $request->only('status');
        $data_->update($req);

        if($request->jenis_pengambilan === "Diantar") {
            $data_->Pelanggan()->update(['alamat' => $request->alamat]);
        }

        if($data_->status != 3) {
            return redirect()->back()->with(['info' => "Data berhasil diubah..."]);
        } else {
            return redirect()->route('transaksi.penyewaan');
        }
    }

    public function delete($id) {
        $data = Transaksi::where('id', $id)->firstOrFail();
        $data->delete();
        $data->Pelanggan()->delete();
        return redirect()->back();
    }

    //api
    public function api_price_kendaraan(Request $request) {
        $data = Kendaraan::where('id', $request->id)->select('tarif')->first();
        if($data) {
            return $data;
        } else {
            return 400;
        }
    }
}
