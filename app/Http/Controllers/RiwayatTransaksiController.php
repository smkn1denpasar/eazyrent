<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Transaksi;

class RiwayatTransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::user();
        $data = Transaksi::where('kode_user', $auth->kode)->paginate(3);
        return view('user.settings.riwayat-transaksi.index', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kode)
    {
        $auth = Auth::user();
        $data = Transaksi::where('kode_user', $auth->kode)->where('kode', $kode)->firstOrFail();
        return view('user.settings.riwayat-transaksi.detail', compact('data'));
    }
}
