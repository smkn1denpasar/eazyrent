<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\RentalAdminUtama;
use Auth;

class CheckRental
{
    public function compose(View $view)
    {
        $user = \Auth::user();
        if($user) {
            $rental = RentalAdminUtama::where('kode_user', $user->kode)->first();
            $view->with('rental', $rental);
        }
    }
}