<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\SocialMedia;

class SocMed
{
    public function compose(View $view)
    {
        $social = SocialMedia::whereNotIn('type', ['email', 'no_telp', 'location'])->get();
        $view->with('social', $social);
    }
}