<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class Upload
{
    public function fileName(UploadedFile $photo, $title) {
        $time = date('dmyhis');
        $fileName = str_slug($title) . '-' . $time . '-' . uniqid() . '.' . $photo->guessClientExtension();
        return $fileName;
    }
    public function saveFile(UploadedFile $photo, $fileName, $dir)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'front'. DIRECTORY_SEPARATOR . $dir;
        return $photo->move($path, $fileName);
    }

    public function deleteFile($filename, $dir)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'front' . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }
}