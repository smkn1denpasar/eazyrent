<?php
namespace App\Service;

class random {
    public static function generate($length, $type = null) {

        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if($type === 'num') {
            $characters = '0123456789';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}