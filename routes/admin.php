<?php

Route::group(['prefix' => 'login'], function () {
    Route::get('/', 'Auth\LoginControllerAdmin@view_login')->name('admin.login');
    Route::post('/', 'Auth\LoginControllerAdmin@login');
});
Route::get('/logout', 'Auth\LoginControllerAdmin@logout')->middleware('auth:admin')->name('admin.logout');

Route::group(['middleware' => ['auth:admin'], 'namespace' => 'Admin'], function () {

    Route::get('/', 'DashboardAdmin@index')->name('admin.home');
    Route::get('/pemilik', 'MasterDataKendaraan@show_pemilik');
    Route::get('/pemilik/{id}', 'MasterDataKendaraan@show_pemilik');
    Route::get('/kendaraan', 'MasterDataKendaraan@show_kendaraan');

    Route::group(['prefix' => 'master-data', 'middleware' => 'role:master_admin|administrator'], function() {
        Route::group(['prefix' => 'karyawan', 'middleware' => 'role:master_admin'], function () {
            Route::get('/', 'MasterDataKaryawan@index')->name('master-data.karyawan');
            Route::get('/add', 'MasterDataKaryawan@create')->name('master-data.karyawan.add');
            Route::post('/add', 'MasterDataKaryawan@store');
            Route::get('/{id}/edit', 'MasterDataKaryawan@edit')->name('master-data.karyawan.edit');
            Route::patch('/{id}/edit', 'MasterDataKaryawan@update');
            Route::delete('/{id}/delete', 'MasterDataKaryawan@destroy')->name('master-data.karyawan.delete');
        });
        Route::group(['prefix' => 'rental', 'middleware' => 'role:master_admin|administrator'], function () {
            Route::get('/', 'MasterDataRental@index')->name('master-data.rental');
            Route::get('/add', 'MasterDataRental@create')->name('master-data.rental.add');
            Route::post('/add', 'MasterDataRental@store');
            Route::get('/{id}/edit', 'MasterDataRental@edit')->name('master-data.rental.edit');
            Route::patch('/{id}/edit', 'MasterDataRental@update');
            Route::delete('/{id}/delete', 'MasterDataRental@destroy')->name('master-data.rental.delete');

            //admin
            Route::group(['prefix' => '/{kode}/admin'], function() {
                Route::get('/', 'MasterDataRentalAdmin@index')->name('master-data.rental.admin');
                Route::get('/add', 'MasterDataRentalAdmin@create')->name('master-data.rental.admin.add');
                Route::post('/add', 'MasterDataRentalAdmin@store');
                Route::delete('/{id}/delete', 'MasterDataRentalAdmin@destroy')->name('master-data.rental.admin.delete');
            });

            //cabang
            Route::group(['prefix' => '/{kode}/cabang'], function() {
                Route::get('/', 'MasterDataCabangRental@index')->name('master-data.cabang-rental');
                Route::get('/add', 'MasterDataCabangRental@create')->name('master-data.cabang-rental.add');
                Route::post('add', 'MasterDataCabangRental@store');
                Route::get('/{id}/edit', 'MasterDataCabangRental@edit')->name('master-data.cabang-rental.edit');
                Route::patch('/{id}/edit', 'MasterDataCabangRental@update');
                Route::delete('/{id}/delete', 'MasterDataCabangRental@destroy')->name('master-data.cabang-rental.delete');
            });

            //kendaraan
            Route::group(['prefix' => '/{kode}/kendaraan'], function () {
                Route::get('/', 'MasterDataKendaraan@index')->name('master-data.rental.kendaraan');
                Route::get('/add', 'MasterDataKendaraan@create')->name('master-data.rental.kendaraan.add');
                Route::post('/add', 'MasterDataKendaraan@store');
                Route::get('/{id}/edit', 'MasterDataKendaraan@edit')->name('master-data.rental.kendaraan.edit');
                Route::patch('/{id}/edit', 'MasterDataKendaraan@update');
                Route::delete('/{id}/delete', 'MasterDataKendaraan@destroy')->name('master-data.rental.kendaraan.delete');
            });
        });
        Route::group(['prefix' => 'merk', 'middleware' => 'role:master_admin|administrator'], function () {
            Route::get('/', 'MasterDataMerk@index')->name('master-data.merk');
            Route::get('/add', 'MasterDataMerk@create')->name('master-data.merk.add');
            Route::post('/add', 'MasterDataMerk@store');
            Route::get('/{id}/edit', 'MasterDataMerk@edit')->name('master-data.merk.edit');
            Route::patch('/{id}/edit', 'MasterDataMerk@update');
            Route::delete('/{id}/delete', 'MasterDataMerk@destroy')->name('master-data.merk.delete');
        });
        // Route::group(['prefix' => 'sopir', 'middleware' => 'role:master_admin|administrator'], function() {
        //     Route::get('/', 'MasterDataSopir@index')->name('master-data.sopir');
        //     Route::get('/add', 'MasterDataSopir@create')->name('master-data.sopir.add');
        //     Route::post('/add', 'MasterDataSopir@store');
        //     Route::get('/{id}/edit', 'MasterDataSopir@edit')->name('master-data.sopir.edit');
        //     Route::patch('/{id}/edit', 'MasterDataSopir@update');
        //     Route::delete('/{id}/delete', 'MasterDataSopir@destroy')->name('master-data.sopir.delete');
        // });
    });

    Route::group(['prefix' => 'search'], function() {
        Route::get('/user-ktp', 'SearchController@user_ktp')->name('search.user-ktp');
        Route::get('/user-data/{kode}', 'SearchController@user_data')->name('search.user-data');
        
        Route::get('/jenis-kendaraan', 'SearchController@jenis_kendaraan')->name('search.jenis-kendaraan');
    });

    Route::group(['prefix' => 'transaksi', 'middleware' => 'role:master_admin|customer_service'], function() {
        Route::get('/', 'AllTransaksi@index')->name('transaksi');

        Route::group(['prefix' => 'bulan'], function() {
            Route::get('/', 'TransaksiPerBulan@index')->name('transaksi-per-bulan');
        });
    });
    
});