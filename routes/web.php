<?php

Route::get('/', 'ConfigFront@index')->name('index');
Route::group(['middleware' => ['guest']], function () {
    Route::post('/login', 'Auth\LoginController@login')->name('login');
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['prefix' => 'api'], function () {
    Route::post('/price-kendaraan', 'TransaksiPenyewaanController@api_price_kendaraan')->name('api.price-kendaraan');
    Route::get('/kendaraan', 'MasterDataKendaraanController@api_kendaraan')->name('api.kendaraan');
    Route::get('/pemilik', 'MasterDataKendaraanController@api_pemilik')->name('api.pemilik');
    Route::get('/transaksi', 'TransaksiPengembalianController@api_transaksi')->name('api.transaksi');
});